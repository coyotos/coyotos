/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/capability.h>
#include <kerninc/InvParam.h>
#include <kerninc/Process.h>
#include <kerninc/Endpoint.h>
#include <kerninc/assert.h>
#include <kerninc/ObjectHash.h>
#include <kerninc/ObStore.h>
#include <kerninc/DevRange.h>
#include <kerninc/Cache.h>
#include <kerninc/mutex.h>
#include <hal/syscall.h>
#include <coyotos/syscall.h>
#include <idl/coyotos/DevRangeCtl.h>

extern void cap_Cap(InvParam_t* iParam);

/** @brief Implementation of the device range management capability.
 *
 * This procedure implements all of the kerel range capability
 * methods. Most of the operations performed are trivial, with the
 * notable exception of the rescind operation.
 */
void
cap_DevRangeCtl(InvParam_t *iParam)
{
  uintptr_t opCode = iParam->opCode;

  switch(opCode) {
  case OC_coyotos_Cap_getType:	/* Must override. */
    {
      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();
      InvTypeMessage(iParam, IKT_coyotos_DevRangeCtl);
      break;
    }

  case OC_coyotos_DevRangeCtl_getNumRanges:
    {
      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();
      put_oparam32(iParam, DevRangeMgr.nDevRange);
      iParam->opw[0] = InvResult(iParam, 0);

      break;
    }

  case OC_coyotos_DevRangeCtl_getPagesPerRange:
    {
      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();
      put_oparam32(iParam, DevRangeMgr.nPagePerRange);
      iParam->opw[0] = InvResult(iParam, 0);

      break;
    }

  case OC_coyotos_DevRangeCtl_setup:
    {
      uint32_t ndx = get_iparam32(iParam);
      uint64_t base = get_iparam64(iParam);
      uint64_t limit = get_iparam64(iParam);
      uint32_t l2sz = get_iparam32(iParam);
      INV_REQUIRE_ARGS(iParam, 0);

      // Calls sched_commit_point internally:
      uint64_t result = devrange_setup(ndx, base, limit, l2sz);

      if (result != RC_coyotos_Cap_OK) {
	InvErrorMessage(iParam, result);
	return;
      }

      iParam->opw[0] = InvResult(iParam, 0);
      break;
    }

  case OC_coyotos_DevRangeCtl_destroy:
    {
      uint32_t ndx = get_iparam32(iParam);
      INV_REQUIRE_ARGS(iParam, 0);

      // Calls sched_commit_point internally:
      uint64_t result = devrange_destroy(ndx);

      if (result != RC_coyotos_Cap_OK) {
	InvErrorMessage(iParam, result);
	return;
      }

      iParam->opw[0] = InvResult(iParam, 0);
      break;
    }
  default:
    cap_Cap(iParam);
    break;
  }
}
