/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/capability.h>
#include <kerninc/InvParam.h>
#include <kerninc/Process.h>
#include <kerninc/Endpoint.h>
#include <kerninc/assert.h>
#include <kerninc/ObjectHash.h>
#include <kerninc/ObStore.h>
#include <kerninc/Cache.h>
#include <kerninc/mutex.h>
#include <hal/syscall.h>
#include <coyotos/syscall.h>
#include <idl/coyotos/Range.h>

extern void cap_Cap(InvParam_t* iParam);

/** @brief Implementation of the range capability.
 *
 * This procedure implements all of the kernel range capability
 * methods. Most of the operations performed are trivial, with the
 * notable exception of the rescind operation.
 */
void
cap_Range(InvParam_t *iParam)
{
  uintptr_t opCode = iParam->opCode;

  switch(opCode) {
  case OC_coyotos_Cap_getType:	/* Must override. */
    {
      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();
      InvTypeMessage(iParam, IKT_coyotos_Range);
      break;
    }

  case OC_coyotos_Range_nextBackedSubrange:
    {
      uint64_t startOffset __attribute__((unused)) = get_iparam64(iParam);
      uint32_t obType = get_iparam32(iParam);
      ObType oty;

      INV_REQUIRE_ARGS(iParam, 0);

      switch(obType) {
      case coyotos_Range_obType_otPage:
	oty = ot_Page;
	break;
      case coyotos_Range_obType_otCapPage:
	oty = ot_CapPage;
	break;
      case coyotos_Range_obType_otGPT:
	oty = ot_GPT;
	break;
      case coyotos_Range_obType_otProcess:
	oty = ot_Process;
	break;
      case coyotos_Range_obType_otEndpoint:
	oty = ot_Endpoint;
	break;

      default:
	{
	  sched_commit_point();
	  InvErrorMessage(iParam, RC_coyotos_Cap_RequestError);
	  return;
	}
      }
      
      sched_commit_point();
      put_oparam64(iParam, 0);
      put_oparam64(iParam, Cache.max_oid[oty]);
      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }
  case OC_coyotos_Range_identify:
    {
      INV_REQUIRE_ARGS(iParam, 1);

      cap_prepare(iParam->srcCap[1].cap);
      sched_commit_point();

      uint32_t ot = coyotos_Range_obType_otInvalid;
      oid_t oid = 0;

      switch(iParam->srcCap[1].cap->type) {
      case ct_Page:
	ot = coyotos_Range_obType_otPage;
	break;
      case ct_CapPage:
	ot = coyotos_Range_obType_otCapPage;
	break;
      case ct_GPT:
	ot = coyotos_Range_obType_otGPT;
	break;
      case ct_Process:
	ot = coyotos_Range_obType_otProcess;
	break;
      case ct_Endpoint:
	ot = coyotos_Range_obType_otEndpoint;
	break;
      default:
	break;
      }

      switch(iParam->srcCap[1].cap->type) {
      case ct_Page:
      case ct_CapPage:
      case ct_GPT:
      case ct_Process:
      case ct_Endpoint:
	oid = iParam->srcCap[1].cap->u2.prepObj.target->oid;
      default:
	break;
      }

      put_oparam32(iParam, ot);
      put_oparam64(iParam, oid);
      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Range_rescind:
    {
      /**
       * @section RescindCases Sub-Cases of Rescind
       *
       * There are several sub-cases of rescind to consider:
       *
       * - <b>Non-persistent objects</b>. For these objects, the
       *   effect of rescind is to 
       *   free the frame for immediate re-use. In this case there
       *   cannot be on-disk capabilities, because persistent objects
       *   are not permitted to contain such capabilities.
       * - <b>Persistent objects that have on-disk capabilities</b>.
       *   This may or may not be in-memory, and may or may not be dirty.
       *   We must clear the object and increment the allocation
       *   count. The allocation count must be incremented because in
       *   this case there must necessarily be on-disk capabilities.
       * - <b>Persistent objects that do not have on-disk capabilities</b>.
       *   This has two sub-cases:
       *     -# If it is clean, then we have a kernel inconsistency,
       *        because this object @em must be the result of a
       *        rescind that has not yet propagated to disk, and
       *        therefore must be dirty.
       *     -# If it is dirty, then this object may or may not be the
       *        result of a rescind. In either case, we need to clear
       *        it, but the object must not be freed before it is
       *        written back.
       *
       * It is tempting to imagine that we might usefully augment the
       * on-disk "object known to be zero" with an "object is known
       * not to have on-disk capabilities" bit. This would permit us
       * to reload such an object @em without setting the @p
       * hasDiskCaps field, which would often allow us to revoke it
       * without writing the object back at all -- even if it was
       * dirty.
       *
       * This does not work because of the following sequence of
       * events:
       * -# Object without on-disk caps is allocated and comes
       *    in. Object has not been modified and is therefore clean.
       * -# Some capability to this object is written into a
       *    capability slot somewhere.
       * -# The containing object goes to disk, so there are now
       *    on-disk capabilities.
       * -# The original object is still clean, does not go to
       *    disk, and so the "object has no on-disk caps" bit does
       *    not get cleared.
       * So the problem is that the "has no on-disk caps" bit is not
       * reliable.
       *
       * A variant that @em would work would be to declare that any
       * object having no on-disk caps must immediately be marked
       * dirty <em>when the first capability to that object is
       * prepared</em>. This would ensure that the object would get
       * written back if any cap to it manages (conservatively) to go
       * to disk. The reason to mark the object dirty aggressively is
       * to ensure that log space is reserved for this object. Note
       * that newly allocated objects get dirtied with very high
       * likelihood, so this doesn't really add any load to the
       * checkpoint mechanism.
       *
       * But we then have two cases of dirty=true,
       * hasDiskCaps=false. The first arises immediately following
       * rescind. This version MUST be written back. The second arises
       * because the object never had disk caps in the first
       * place. The proposal is to add a third tracking bit
       * cleanOnRescind that is used to tell us which case is
       * which. The @p cleanOnRescind field is governed by:
       *
       * - set if the object is an in-memory object.
       * - set at load it no disk caps existed at the time of load.
       * - cleared whenever @p hasDiskCaps is set.
       *
       * So far as I can tell, this scheme would work reliably, but it
       * is not yet implemented here.
       */

      INV_REQUIRE_ARGS(iParam, 1);

      /* If cap is unprepared, we need to bring the object in so
       * that we can bump the allocation count. If cap is prepared,
       * this will do no harm. Recall that if the target object is not
       * persistent, no unprepared cap to that object will exist
       * anywhere.
       *
       * Once we have a persistant system, we *really* don't want to do
       * the prepare if the object is on disk.  Here's the case analysis:
       *
       * If the cap is in the "prepared" state:
       *   If the preperation is valid, do cap_prepare() path
       *   If not, deprepare the capability and start over
       * If the cap is not in the "prepared" state:
       *   If the object is in the Object Hash and is current, do 
       *    cap_prepare() path.
       *   Otherwise, if the object is in the Log, check the cap's allocCount
       *    against the most recent version in the log.
       *     If it matches, allocate a new log frame, set it up as 
       *      allocCount + 1 and "zeroed", and we're done.
       *     If it doesn't match, fail as if passed a Null cap.
       *   Otherwise, bring in the allocation pot to get the allocCount 
       *    for the object.  Create a log entry with "allocCount + 1" and
       *    "zeroed" for the object.
       *
       *  cap_prepare() path:
       *    Prepare the cap, if it's not an object cap, bail out.
       *    Dirty the object, invalidate any cached state
       *    commit_point()
       *    Invalidate all outstanding caps, zero state, bump allocation
       *    count if necessary.
       *
       * For the moment, we always take the cap_prepare() path.
       */
      cap_prepare(iParam->srcCap[1].cap);

      ObjectHeader *obHdr = iParam->srcCap[1].cap->u2.prepObj.target;
      bool bumpAllocCount = obHdr->hasDiskCaps ? true : false;

      // Sanity check: A non-persistent object should not have on-disk
      // capabilities.
      assert (obHdr->isPersistent || !obHdr->hasDiskCaps);

      /**
       * @bug range_rescind() should fail in an orderly way when
       * applied to an immutable object.
       */

      /* Grab the frame cache lock in case we need to move the
       * rescinded object to the clean list. This needs to be done
       * before the commit point. Don't bother to retain the HoldInfo
       * structure, since we can't release until we complete in any
       * case.
       */
      ObFrameCache *ofc = Cache.obCache[obHdr->ty];
      (void) mutex_grab(&ofc->lock);

      /* Grab the object hash table locks for the FROM hash, in case
	 we need to clean the object. */
      (void) obhash_grabMutex(obHdr->ty, obHdr->oid);


      /**
       * If we are required to bump the allocation count, then we must
       * ensure that the object is marked dirty so that it will get
       * written out. This is harmless if the object is already
       * dirty.
       */
      if (bumpAllocCount) {
	assert(obHdr->isPersistent);
	/* obhdr_mark_dirty() fails exactly if the object is immutable, in
	 * which case we don't want to dirty it in any case, so ignore
	 * the result.
	 */
	(void) obhdr_mark_dirty(obHdr);
      }

      /**
       * Here is an enumeration of cases at this point:
       * - Object has on-disk caps and is therefore dirty.
       * - Object is non-persistent.
       * - Object has @em no on-disk caps and may or may not be
       *   dirty.
       */
      assert(obHdr->dirty || !obHdr->isPersistent);

      /* Whack any cached state that references this object. */
      obhdr_invalidate(obHdr);

      sched_commit_point();

      obHdr->pinned = 0;

      cache_clear_object(obHdr);

      if (bumpAllocCount) {
	/** @bug Watch out for alloc Count rollover -- object must
	    become broken in that case. */
	obHdr->allocCount++;
      }

      if (!obHdr->isPersistent) {
	/* This is a non-persistent object, so it has no on-disk
	 * caps. What we want to do here is move it directly to the
	 * unallocated agelist of its associated cache. */

	obhdr_free(obHdr);
      }

      /* Careful! We may have just destroyed either the invoker or the
       * invokee (or both). Unfortunately, if we destroyed the invoker
       * we still need to deliver results to the invokee (assuming one
       * exists).
       *
       * We may also have destroyed the reply endpoint.
       */

      if (iParam->invokeeEP == (Endpoint *) obHdr)
	iParam->invokee = 0;	/* suppress reply */

      if (iParam->invokee == (Process *) obHdr)
	iParam->invokee = 0;

      if (iParam->invoker == (Process *) obHdr) {
	HoldInfo hi = { &iParam->invoker->hdr.lock, 0 };
	mutex_release(hi);
	iParam->invoker = 0;
	MY_CPU(current) = 0;
      }

      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Range_getCap:
  case OC_coyotos_Range_waitCap:
  case OC_coyotos_Range_getProcess:
  case OC_coyotos_Range_waitProcess:
    {
      oid_t oid = get_iparam64(iParam);
      uint32_t obType = coyotos_Range_obType_otProcess;
      uint32_t nCapArg = 1;

      if ((opCode == OC_coyotos_Range_getCap) ||
	  (opCode == OC_coyotos_Range_waitCap)) {
	nCapArg = 0;
	obType = get_iparam32(iParam);
      }

      INV_REQUIRE_ARGS(iParam, nCapArg);

      bool waitForRange = 
	((opCode == OC_coyotos_Range_waitCap) || 
	 (opCode == OC_coyotos_Range_waitProcess));

      uint32_t capType = 0;
      capability *brand = NULL;

#if 0
      /* handle physical ranges as a special case */
      if (oid >= coyotos_Range_physOidStart) {
	kpa_t kpa = (oid - coyotos_Range_physOidStart) * COYOTOS_PAGE_SIZE;
	/* check for bad type or overflow */
	if (obType != coyotos_Range_obType_otPage ||
	    (kpa / COYOTOS_PAGE_SIZE + coyotos_Range_physOidStart) != oid) {
	  sched_commit_point();
	  InvErrorMessage(iParam, RC_coyotos_Cap_RequestError);
	  return;
	}

#if 0
	/* This is incorrect -- we should not be attempting to bounce
	 * the frame here.  That should not happen until we go to
	 * prepare this capability. Fabrication of the capability
	 * should succeed whether or not the physical frame exists.
	 */
	Page *retVal = cache_get_physPage(kpa);
	if (retVal == 0) {
	  sched_commit_point();
	  InvErrorMessage(iParam, RC_coyotos_Range_RangeErr);
	  return;
	}
#endif

	cap_init(&iParam->srcCap[0].theCap);

	/* Set up a deprepared cap, then prepare it. */
	iParam->srcCap[0].theCap.type = ct_Page;
	iParam->srcCap[0].theCap.swizzled = 0;
	iParam->srcCap[0].theCap.restr = 0;
	
	/* Physical page capabilities are not durable. They can always
	 * be given an allocation count of zero. */
	iParam->srcCap[0].theCap.allocCount = 0;
	
	/* Memory caps have a min GPT requirement. */
	iParam->srcCap[0].theCap.u1.mem.l2g = COYOTOS_PAGE_ADDR_BITS;

	iParam->srcCap[0].theCap.u2.oid = oid;

	cap_prepare(&iParam->srcCap[0].theCap);
	
	sched_commit_point();

#if 0
	retVal->mhdr.hdr.pinned = 1;
#endif

	iParam->opw[0] = InvResult(iParam, 1);
	return;
      }
#endif

      /** We need to hunt down the target object so that we can
       * determine the allocation count.  If the object is not already
       * in core we need to bring it in.
       *
       * It is tempting to think that we could avoid this complication
       * in the embedded implementation by pre-assigning object IDs to
       * the elements of the various object vectors. Regrettably this
       * is not so. If we do that we will lose the ability to allocate
       * objects at known physical addresses.
       *
       * Given that we cannot simplify this much for the embedded
       * case, we decided to go ahead and keep the code as close to
       * the general implementation as possible to lower code
       * maintainence complexity. I @em did take the expedient of
       * allocating the objects here directly rather than invoking the
       * ObStore logic to do it.
       */

      switch(obType) {
      case coyotos_Range_obType_otPage:
	capType = ct_Page;
	break;
      case coyotos_Range_obType_otCapPage:
	capType = ct_CapPage;
	break;
      case coyotos_Range_obType_otGPT:
	capType = ct_GPT;
	break;
      case coyotos_Range_obType_otProcess:
	capType = ct_Process;
	break;
      case coyotos_Range_obType_otEndpoint:
	capType = ct_Endpoint;
	break;

      default:
	{
	  sched_commit_point();
	  InvErrorMessage(iParam, RC_coyotos_Cap_RequestError);
	  return;
	}
      }

      ObType ot = captype_to_obtype(capType);

      ObjectHeader *obHdr = 
	obstore_require_object(ot, oid, waitForRange, NULL);

      if (obHdr == 0) {
	  sched_commit_point();
	  InvErrorMessage(iParam, RC_coyotos_Range_RangeErr);
	  return;
      }
      cap_init(&iParam->srcCap[0].theCap);

      /* Set up a deprepared cap, then prepare it. We do it this way
       * because preparing the capability has other useful side
       * effects like driving the ager, though I'm not convinced that
       * this hasn't already been done by the cache frame
       * allocator.
       *
       * Note that there is no way that the unprepared capability
       * survives to the end of this operation. If this prepare fails,
       * or if the attempt to mark the object dirty fails, the current
       * unit of operation will yield, and it will be as if this
       * unprepared copy had never existed. This is important, because
       * (e.g.) device page capabilities must never exist in
       * unprepared form.
       */
      iParam->srcCap[0].theCap.type = capType;
      iParam->srcCap[0].theCap.swizzled = 0;
      iParam->srcCap[0].theCap.restr = 0;

      iParam->srcCap[0].theCap.allocCount = obHdr->allocCount;

      /* Memory caps have a min GPT requirement. */
      if (capType == ct_Page || capType == ct_CapPage)
	iParam->srcCap[0].theCap.u1.mem.l2g = ((Page*)obHdr)->l2g;
      else if (capType == ct_GPT)
	iParam->srcCap[0].theCap.u1.mem.l2g = COYOTOS_PAGE_ADDR_BITS;

      iParam->srcCap[0].theCap.u2.oid = obHdr->oid;

      cap_prepare(&iParam->srcCap[0].theCap);

      if (brand)
	obhdr_mark_dirty(obHdr);

      sched_commit_point();

      if (brand) {
	assert(obType == coyotos_Range_obType_otProcess);
	Process *p = (Process *) obHdr;
	cap_set(&p->state.brand, brand);
      }

      iParam->opw[0] = InvResult(iParam, 1);
      return;
    }

  default:
    cap_Cap(iParam);
    break;
  }
}
