/* -*- mode: c -*- */

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package IA32;

/* This isn't really a unit; we do that as a namespace hack.
 *
 * CR3 normal coyotos state:
 *   VME=0 PVI=0 TSD=0 DE=1 PSE=1 PAE=0/1 MCE=0 PGE=1 PCE=0
 *   OSFXSR=0 PSMMEXCPT=0
 *
 * Notes on some of these:
 *
 * DE=0 will break compatibility with some things. Tough.
 *
 * MCE=0 is temporary - we need to handle this.
 *
 * PCE=0 Enabling app-level access to performance monitoring raises a
 * boat load of security issues. I'ld actually like to turn off RDTSC
 * as well, but that is not a pragmatically sustainable design choice.
 *
 * OSFXSR=0, OSXMMEXCPT=0 is a bug. I simply have not had a chance to
 * implement SIMD support yet.
 */
reg CR4 32 {
  bit VME         0;		/* virtual 8086 extensions */
  bit PVI         1;	        /* protected mode virtual interrupts */
  bit TSD         2;		/* time stamp disable */
  bit DE          3;		/* debugging extensions */
  bit PSE         4;		/* page size extensions */
  bit PAE         5;		/* physical address extension */
  bit MCE         6;		/* machine check enable */
  bit PGE         7;		/* page global enable */
  bit PCE         8;	        /* performance monitor counter enable. */
  bit OSFXSR      9;		/* OS supports FXSAVE/FSTSTOR */
  bit OSXMMEXCPT 10;		/* OS supports SIMD FP exceptions */
}
