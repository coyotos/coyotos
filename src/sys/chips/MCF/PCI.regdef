/* -*- mode: c -*- */

/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package MCF;

unit PCI {
  /******************************************************************
   * On-Chip PCI Controller
   *
   * There is a single instance of the on-chip PCI controller, which
   * occupies two device windows in the MBAR region:
   *
   *    0x0Bxx  The controller itself
   *    0x84xx  The FIFO Transmit/Receive Interface Registers
   *
   * Because all of these are strictly associated with the PCI
   * controller, and are named that way, I didn't see any compelling
   * reason to have multiple regdef files for them. For this reason, I
   * have used an instance offset of 0x0 (which is wrong) and used the
   * full MBAR register offsets.
   *
   ******************************************************************/

  instance 0 at 0x0b00 from BSP.defs.MBAR;

  reg IDR         32  0x0b00 {
    field DevID     16 31;
    field VendorID  0  15;
  }
  reg SCR         32  0x0b04 {
    bit   PE      31;		/* read-write-clear */
    bit   SE      30;		/* read-write-clear */
    bit   MA      29;		/* read-write-clear */
    bit   TR      28;		/* read-write-clear */
    bit   TS      27;		/* read-write-clear */
    field DT      25  26;
    bit   DP      24;		/* read-write-clear */
    bit   FC      23;
    bit   R       22;
    bit   Bus66M  21;
    bit   C       20;
    bit   F       9;		/* read/write */
    bit   S       8;		/* read/write */
    bit   ST      7;
    bit   PER     6;		/* read/write */
    bit   V       5;
    bit   MW      4;		/* read/write */
    bit   SP      3;		/* read/write */
    bit   B       2;		/* read/write */
    bit   M       1;		/* read/write */
    bit   IO      0;
  }
  reg CCRIR       32  0x0b08 {
    field ClassCode  8 31;
    field RevisionID 0 7;
  }

  reg CR1           32  0x0b0c {
    field BIST          24 31;
    field HdrType       16 23;
    field LatencyTimer  8 15;	/* bits 8:10 not writable */
    field CacheLnSz     0 7;	/* bits 4:7 not writable */
  }
  reg BAR0        32  0x0b10 {
    field BAR0  18 31;
    bit   PREF  3;
    field RANGE 1 2;
    bit IO_M    0 {
      Memory = 0;
      IO = 1;
    }
  }

  reg BAR1        32  0x0b14 {
    field BAR1  30 31;
    bit PREF    3;
    field RANGE 1 2;
    bit IO_M    0 {
      Memory = 0;
      IO = 1;
    }
  }
  /* 0x0b18 .. 0x0b24 reserved */
  reg CCPR        32  0x0b28;
  reg SID         32  0x0b2c;
  reg ERBAR       32  0x0b30;
  reg CPR         32  0x0b34;
  /* 0x0b38 reserved */
  reg CR2         32  0x0b3c {
    field MaxLat        24 31;
    field MinGnt        16 23;
    field InterruptPin  8 15;	/* fixed to zero (no interrupt pin)  */
    field InterruptLine 0 7;	/* fixed to zero (no interrupt line) */
  }
  /* 0x0b30-0x0b5c reserved */

  reg GSCR        32  0x0b60 {
    bit   PE        29;
    bit   SE        28;
    field XLB2CLKIN 24 26;
    bit   PEE       12;
    bit   SEE       11;
    bit   PR        0;
  }
  reg TBATR0      32  0x0b64 {
    field BaseAddressTranslation0  18 31;
    bit   EN                       0;
  }
  reg TBATR1      32  0x0b68 {
    field BaseAddressTranslation1  30 31;
    bit   EN                       0;
  }
  reg TCR         32  0x0b6c {
    bit   LD  24;
    bit   P   16;
  }
  reg IW0BTAR     32  0x0b70 {
    field Win0BaseAddr  24 31;
    field Win0AddrMask  16 23;
    field Win0XlateAddr 8  15;
  }
  reg IW1BTAR     32  0x0b74 {
    field Win1BaseAddr  24 31;
    field Win1AddrMask  16 23;
    field Win1XlateAddr 8  15;
  }
  reg IW2BTAR     32  0x0b78 {
    field Win2BaseAddr  24 31;
    field Win2AddrMask  16 23;
    field Win2XlateAddr 8  15;
  }
  /* 0x0b7c reserved */
  reg IWCR        32  0x0b80 {
    field Win0Ctrl 24 27;
    field Win1Ctrl 16 19;
    field Win2Ctrl 8  11;
  }
  reg ICR         32  0x0b84 {
    bit   REE        26;
    bit   IAE        25;
    bit   TAE        24;
    field MaxRetries 0 7;
  }
  reg ISR         32  0x0b88 {
    bit   RE         26;	/* read-write-clear */
    bit   IA         25;	/* read-write-clear */
    bit   TA         24;	/* read-write-clear */
  }

  /* 0x0b8c-0x0bf4 reserved */
  reg CAR         32  0x0bf8 {
    bit   E         31;
    field BusNo 16 23;
    field DevNo 11 15;
    field FuncNo 8 10;
    field DWORD  2 7;		/* dword addr in target config space */
  }
  /* 0x0bfc reserved */


  /******************************************************************
   *  CommBus FIFO Transmit Interface Registers
   *
   *  Strictly speaking this isn't part of the PCI device register
   *  space at all, but these registers are all named with PCI in the
   *  manual, and I didn't see a compelling reason to have a separate
   *  regdef file for them.
   ******************************************************************/

  reg TPSR        32  0x8400 {
    field PktSz 16 31;
  }
  reg TSAR        32  0x8404;
  reg TTCR        32  0x8408 {
    field PCIcmd     24 27;
    field MaxRetries 16 23;
    field MaxBeats   8  10;
    bit   W          4;
    bit   DI         0;
  }
  reg TER         32  0x840c {
    bit   RC  31;
    bit   RF  30;
    bit   CM  28;
    bit   BE  27;
    bit   ME  24;
    bit   FEE 21;
    bit   SE  20;
    bit   RE  19;
    bit   TAE 18;
    bit   IAE 17;
    bit   NE  16;
  }
  reg TNAR        32  0x8410;
  reg TLWR        32  0x8414;
  reg TDCR        32  0x8418 {
    field BytesDone 16 31;
    field PktsDone  0  15;
  }
  reg TSR         32  0x841c {
    bit NT  24;			/* read-write-clear */
    bit BE3 23;			/* read-write-clear */
    bit BE2 22;			/* read-write-clear */
    bit BE1 21;			/* read-write-clear */
    bit FE  20;			/* read-write-clear */
    bit SE  19;			/* read-write-clear */
    bit RE  18;			/* read-write-clear */
    bit TA  17;			/* read-write-clear */
    bit IA  16;			/* read-write-clear */
  }

  /* 0x8420 - 0x843c reserved */
  reg TFDR        32  0x8440;
  reg TFSR        32  0x8444 {
    bit FAE   23;			/* read-write-clear */
    bit RXW   22;			/* read-write-clear */
    bit UF    21;			/* read-write-clear */
    bit OF    20;			/* read-write-clear */
    bit FR    19;
    bit Full  18;
    bit Alarm 17;
    bit Empty 16;
  }
  reg TFCR        32  0x8448 {
    bit   WFR      29;
    field GR       24 26;
    bit   IP_MASK  23;
    bit   FAE_MASK 22;
    bit   RXW_MASK 21;
    bit   UF_MASK  20;
    bit   OF_MASK  19;
    bit   TXW_MASK 18;
  }
  reg TFAR        32  0x844c {
    field Alarm 0 11;
  }
  reg FRPR        32  0x8450 {
    field ReadPtr 0 6;
  }
  reg FWPR        32  0x8454 {
    field WritePtr 0 6;
  }
  /* 0x8457..0x847c reserved */

  /******************************************************************
   *  CommBus FIFO Receive Interface Registers
   *
   *  Strictly speaking this isn't part of the PCI device register
   *  space at all, but these registers are all named with PCI in the
   *  manual, and I didn't see a compelling reason to have a separate
   *  regdef file for them.
   ******************************************************************/

  reg RPSR        32  0x8480 {
    field PktSize 16 31;
  }
  reg RSAR        32  0x8484;
  reg RTCR        32  0x8488 {
    field PCIcmd     24 27;
    field MaxRetries 16 23;
    bit   FB         12;
    field MaxBeats   8  10;
    bit   W          4;
    bit   DI         0;
  }
  reg RER         32  0x848c {
    bit RC  31;
    bit RF  30;
    bit FE  29;
    bit CM  28;
    bit BE  27;
    bit ME  24;
    bit FEE 1;
    bit SE  20;
    bit RE  19;
    bit TAR 18;
    bit IAE 17;
    bit NE  16;
  }
  reg RNAR        32  0x8490;
  /* 0x8494 reserved */
  reg RDCR        32  0x8498 {
    field BytesDone 16 31;
    field PktsDone  0  15;
  }
  reg RSR         32  0x849c {
    bit NT  24;			/* read-write-clear */
    bit BE3 23;			/* read-write-clear */
    bit BE2 22;			/* read-write-clear */
    bit BE1 21;			/* read-write-clear */
    bit FE  20;			/* read-write-clear */
    bit SE  19;			/* read-write-clear */
    bit RE  18;			/* read-write-clear */
    bit TA  17;			/* read-write-clear */
    bit IA  16;			/* read-write-clear */
  }
  /* 0x84a0-0x84bc reserved */
  reg RFDR        32  0x84c0;
  reg RFSR        32  0x84c4 {
    bit IP    31;			/* read-write-clear */
    bit TXW   30;			/* read-write-clear */
    bit FAE   23;			/* read-write-clear */
    bit RXW   22;			/* read-write-clear */
    bit UF    21;			/* read-write-clear */
    bit OF    20;			/* read-write-clear */
    bit FR    19;
    bit Full  18;
    bit Alarm 17;
    bit Empty 16;
  }
  reg RFCR        32  0x84c8 {
    bit   WFR      29;
    field GR       24 26;
    bit   IP_MASK  23;
    bit   FAE_MASK 22;
    bit   RXW_MASK 21;
    bit   UF_MASK  20;
    bit   OF_MASK  19;
    bit   TXW_MASK 18;
  }
  reg RFAR        32  0x84cc {
    field Alarm 0 11;
  }
  reg RFRPR       32  0x84d0 {
    field ReadPtr 0 6;
  }
  reg RFWPR       32  0x84d4 {
    field WritePtr 0 6;
  }
  /* 0x84d8-0x84fc reserved */
}
