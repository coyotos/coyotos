/* -*- mode: c -*- */

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package MCF;

unit PSC {
  /* Syntax: 

     for absolute entries: instance N at ADDR
     for relative entreis: instance N at OFFSET from REGNAME
  */

  instance 0 at 0x8600 from BSP.defs.MBAR;
  instance 1 at 0x8700 from BSP.defs.MBAR;
  instance 2 at 0x8800 from BSP.defs.MBAR;
  instance 3 at 0x8800 from BSP.defs.MBAR;

  /* Syntax:  reg NAME BITSIZE DEFAULT-OFFSET */
  reg MR1 8 0x0 {
    /* Syntax:  bit NAME BITPOSITION */
    bit   RXRTS 7;
    bit   RXIRQ 6;
    bit   ERR   5;

    /* Syntax:  field NAME LOWBIT HIGHBIT (inclusive)
     */
    // Combines PM[4:3]:PT[2] fields:
    field MODE 2 4 {
      /* Value Syntax:  NAME = value */
      PARITY_EVEN = 0;
      PARITY_ODD  = 1;
      PARITY_FORCE_LOW = 2;
      PARITY_FORCE_HIGH = 3;
      PARITY_NONE = 4;	/* also 5 */
      MULTIDROP_DATA = 6;
      MULTIDROP_ADDR = 7;
    }
    field BITS 0 1 {
      B5 = 0;
      B6 = 1;
      B7 = 2;
      B8 = 3;
    }
  }

  reg MR2 8 0x00 {
    field CM 6 7 {
      NORMAL = 0;
      ECHO = 1;
      LOCAL_LOOPBACK = 2;
      REMOTE_LOOPBACK = 3;
    }

    bit   TXRTS 5;
    bit   TXCTS 4;
    field SB 0 3 {
      /* Other values are not of interest. Don't bother with them. */
      ONE = 7;
    }
  }

  reg SR 16 0x04 {		/* read only */
    bit RB_NEOF    15;
    bit FE_PHYERR  14;
    bit PE_CRCERR  13;
    bit OE         12;
    bit TXEMP_RERR 11;
    bit TXRDY      10;
    bit FU         9;
    bit RXRDY      8;
    bit CDE_DEOF   7;
    bit ERR        6;
  }

  reg CSR 8 0x04 {		/* write only */
    field RCSEL   4 7;
    field TCSEL   0 3;
  }

  reg CR 8 0x08 {
    field MISC 4 7 {
      RESET_MODE = 1;
      RESET_RX = 2;
      RESET_TX = 3;
      RESET_ERROR = 4;
      RESET_BREAK_CHANGE = 5;
      START_BREAK = 6;
      STOP_BREAK = 7;
    }
    field TXC 2 3 {
      ENABLE = 1;
      DISABLE = 2;
    }
    field RXC 0 1 {
      ENABLE = 1;
      DISABLE = 2;
    }
  }

  /* Interpretation of RB, TB depends on current mode.
   *
   * For UART, Modem8, SIR, MIR, and FIR mode it is simply 4 bytes of
   * receive (respectively transmit) data.
   *
   * For Modem16, it is 32-bit receive (respectively transmit) data.
   *
   * For AC97, bits [12:31] have the receive (respectively transmit)
   * data, and bit 11 is the SOF marker.
   *
   * I am only expanding the AC97 case here (RB only).
   */
  reg RB      32 0x0c {		/* read only */
    bit AC97_SOF 11;
    field AC97_RxData 12 31;
  }
  reg TB      32 0x0c {		/* write only */
    field AC97_TxData 12 31;
  }

  reg IPCR     8 0x10 {		/* read only */
    bit SYNC  7;		/* modem only */
    bit D_CTS 4;
    bit CTS   0;
  }

  reg ACR      8 0x10 {		/* write only */
    /* Note that the UART initialization example later in the document
     * references an undocumented bit IEC1. This bit is not
     * implemented on MCF5485. A documentation errata will be
     * published. */
    bit IEC0  0;		/* modem only */
  }

  reg ISR     16 0x14 {		/* read only */
    bit IPC      15;
    bit DB       10;
    bit RXRDY_FU  9;
    bit TXRDY     8;
    bit DEOF      7;
    bit ERR       6;
  }

  reg IMR     16 0x14 {		/* write only */
    bit IPC      15;
    bit DB       10;
    bit RXRDY_FU  9;
    bit TXRDY     8;
    bit DEOF      7;
    bit ERR       6;
  }

  reg CTUR     8 0x18;
  reg CTLR     8 0x1c;

  reg IP       8 0x34 {
    bit LPWR_B 7;
    bit TGL    6;
    bit CTS    0;
  }
  reg OPSET    8 0x38 {
    bit RTS    0;
  }
  reg OPRESET  8 0x3c {
    bit RTS    0;
  }
  reg SICR     8 0x40 {
    bit   ACRB   7;
    bit   AWR    6;
    bit   DTS1   5;
    bit   SHDIR  4;
    field SIM    0 2 {
      UART = 0;
      MODEM8 = 1;
      MODEM16 = 2;
      AC97 = 3;
      SIR = 4;
      MIR = 5;
      FIR = 6;
      /* value 7 is illegal */
    }
  }
  reg IRCR1    8 0x44 {
    bit FD    2;
    bit SIPEN 1;
    bit SPUL  0;
  }
  reg IRCR2    8 0x48 {
    bit SIPREQ 2;
    bit ABORT  1;
    bit NXTEOF 0;
  }
  reg IRSDR    8 0x4c {
    field IRSTIM 0 7;
  }
  reg IRMDR    8 0x50 {
    bit FREQ     7;
    field M_FDIV 0 6;
  }
  reg IRFDR    8 0x54 {
    field F_FDIV 0 3;
  }

  reg RFCNT   16 0x58;		/* read only */
  reg TFCNT   16 0x5c;		/* read only */

  reg RFDR    32 0x60;
  reg RFSR    16 0x64 {		/* read only */
    bit   IP      15;
    bit   TXW     14;
    field TAG     12 13;
    field FRM      8 11;
    bit   FAE      7;
    bit   RXW      6;
    bit   UF       5;
    bit   OF       4;
    bit   FRM_RDY  3;
    bit   FU       2;
    bit   ALARM    1;
    bit   EMT      0;
  }
  reg RFCR     32 0x68 {
    bit   WFR     29;
    bit   TIMER   28;
    bit   FRMEN   27;
    field GR      24 26;
    bit   IP_MSK  23;
    bit   FAE_MSK 22;
    bit   RXW_MSK 21;
    bit   UF_MSK  20;
    bit   OF_MSK  19;
    bit   TXW_MSK 18;

    field CNTR    0 15;
  }
  reg RFAR    16 0x6e;
  reg RFRP    16 0x72;
  reg RFWP    16 0x76;
  reg RLRFP   16 0x7a;
  reg RLWFP   16 0x7e;
  reg TFDR    32 0x80;
  reg TFSR    16 0x84 {		/* read only */
    bit   IP      15;
    bit   TXW     14;
    field TAG     12 13;
    field FRM      8 11;
    bit   FAE      7;
    bit   RXW      6;
    bit   UF       5;
    bit   OF       4;
    bit   FRM_RDY  3;
    bit   FU       2;
    bit   ALARM    1;
    bit   EMT      0;
  }
  reg TFCR    32 0x88 {
    bit   WFR     29;
    bit   TIMER   28;
    bit   FRMEN   27;
    field GR      24 26;
    bit   IP_MSK  23;
    bit   FAE_MSK 22;
    bit   RXW_MSK 21;
    bit   UF_MSK  20;
    bit   OF_MSK  19;
    bit   TXW_MSK 18;

    field CNTR    0 15;
  }

  reg TFAR    16 0x8e;
  reg TFRP    16 0x92;
  reg TFWP    16 0x96;
  reg TLRFP   16 0x9a;
  reg TLWFP   16 0x9e;
}
