#ifndef __COLDFIRE_SYSCALL_H__
#define __COLDFIRE_SYSCALL_H__
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if defined(__ASSEMBLER__) && !defined(__KERNEL__)

/** @brief Do a raw syscall using the invocation structure located at
 * @p pws.
 *
 * This is designed to be used by read-only assembly routines, like those
 * in ProtoSpace and the small-space initialization code.  Most registers
 * are overwritten by these calls.
 */
#define DO_RO_SYSCALL(pws)		\
        movel  pws,%a0                ; \
	moveml %a0@,%d0-%d7           ; \
        movel  %a0@(IPW_INVCAP*4),%a1 ; \
        movel  %a0@(IPW_SNDLEN*4),%a2 ; \
        movel  %a0@(IPW_EPID*4),%a4   ; \
        movel  %a0@(IPW_EPID*4+4),%a4 ; \
        trap   #0

/** @brief Do a raw system call, but overwrite the third and fourth 
 * parameters. */
#define DO_RO_SYSCALL_LD64(pws, low, high) \
        movel  pws,%a0	              ; \
	moveml %a0@,%d0-%d1           ; \
        movel  low,%d2                ;	\
        movel  high,%d3               ;	\
	moveml %a0@(4*8),%d4-%d7      ; \
        movel  %a0@(IPW_INVCAP*4),%a1 ; \
        movel  %a0@(IPW_SNDLEN*4),%a2 ; \
        movel  %a0@(IPW_EPID*4),%a4   ; \
        movel  %a0@(IPW_EPID*4+4),%a4 ; \
        trap   #0

#endif

#if !defined(__ASSEMBLER__) && !defined(__KERNEL__)

/* It is our considerable misfortune that the m86k ASM constraints do
 * not permit us to specify specific register names. In consequence,
 * cap_copy() and yeild() cannot be inlined on this architecture.
 */
extern bool invoke_capability(InvParameterBlock_t *ipb);

extern void cap_copy(caploc_t dest, caploc_t source);

extern void yield(void);

#endif /* !defined(__ASSEMBLER__) && !defined(__KERNEL__) */

#endif  /* __COLDFIRE_SYSCALL_H__ */
