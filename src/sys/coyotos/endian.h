#ifndef __ENDIAN_H__
#define __ENDIAN_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Support for byte-order definition and detection.
 *
 * While the Coyotos kernel proper should not have any byte order
 * dependencies in a given implementation, the definition of byte
 * order is architecture dependent and therefore properly supplied by
 * the kernel header.
 */

/* Some of the cross-compiled code uses the C++ library, which in turn
 * pulls in endian.h from /usr/include.  The values below are
 * compatible with the generally used UNIX values, but not textually
 * identical. One result of this is that the preprocessor complains. A
 * lot.
 *
 * In order to avoid massive complaints, what we do here is define the
 * values only if they are not already defined, and then check that
 * the definitions we end up with match the values we want.
 */
#define __COYOTOS_LITTLE_ENDIAN  1234
#define __COYOTOS_BIG_ENDIAN     4321
#define __COYOTOS_PDP_ENDIAN     3412	/* fond ancient history */

#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN __COYOTOS_LITTLE_ENDIAN
#elif LITTLE_ENDIAN != __COYOTOS_LITTLE_ENDIAN
#error "Inconsistency between /usr/include/endian.h and coyotos/endian.h"
#endif

#ifndef BIG_ENDIAN
#define BIG_ENDIAN __COYOTOS_BIG_ENDIAN
#elif BIG_ENDIAN != __COYOTOS_BIG_ENDIAN
#error "Inconsistency between /usr/include/endian.h and coyotos/endian.h"
#endif

#ifndef PDP_ENDIAN
#define PDP_ENDIAN __COYOTOS_PDP_ENDIAN
#elif PDP_ENDIAN != __COYOTOS_PDP_ENDIAN
#error "Inconsistency between /usr/include/endian.h and coyotos/endian.h"
#endif

#include <coyotos/machine/endian.h>

#endif  /* __ENDIAN_H__ */
