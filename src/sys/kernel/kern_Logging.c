/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Logging Buffer.
 *
 * This implements the kernel console logging interface. While the
 * kernel internally uses printf() for diagnostic purposes, the
 * messages generated from printf need to be gathered into a
 * record-structured data structure so that they can be retrieved
 * coherently at the capability layer.
 *
 * The log consists of two parts:
 * -# An ordered sequence of LogEntry structures having (logically)
 *    increasing sequence numbers.
 * -# A character ring buffer containing the associated message texts.
 * when the ring buffer becomes full, entries in the
 * log are killed off until sufficient space is available.
 */

#include <inttypes.h>
#include <kerninc/assert.h>
#include <kerninc/mutex.h>
#include <kerninc/string.h>

typedef struct LogEntry LogEntry;
struct LogEntry {
  LogEntry *next;
  uint64_t seqNo;
  int32_t  start;		/* position within log buffer */
  uint16_t len;			/* size of associated message,
				   including terminating NUL. */
};

#define NLOG     256
#define LogBufferSize 8192

static LogEntry log[NLOG];
static char LogBuffer[LogBufferSize];
static size_t LogBufferNext = 0;
static size_t LogAvail = LogBufferSize;

/** @brief Position from which we will fetch the next log message when
 * requested to do so at the capability interface, or -1 if no more
 * messages. */
LogEntry *fetch = NULL;

/** @brief Pointer to first (oldest) log entry. */
LogEntry *first = NULL;
/** @brief Pointer to most recently inserted log entry. */
LogEntry *last = NULL;

/** @brief Log Entry free list. */
LogEntry *freeList = NULL;

static bool log_initialized = false;


static irqlock_t log_lock;

static void
log_init()
{
  for (size_t i = 0; i < NLOG-1; i++)
    log[i].next = &log[i+1];
  log[NLOG-1].next = NULL;

  freeList = log;

  log_initialized = true;
}

static void 
log_clear_first()
{
  assert(first);

  LogEntry *oldest = first;
  first = first->next;

  /* Note that first might now be NULL. This is okay. */

  LogAvail += oldest->len;

  oldest->next = freeList;
  freeList = oldest;

  if (fetch == oldest)
    fetch = first;
  if (last == oldest)
    last = first;
}

static inline void 
log_buffer_putc(char c)
{
  LogBuffer[LogBufferNext++] = c;
  LogBufferNext %= LogBufferSize;
}

void 
log_append(const char *msg)
{
  static uint64_t seqNo = 0;

  /* Message length must include terminating NUL: */
  size_t msgBytes = strlen(msg) + 1;

  /* Message size exceeds available buffer space. Drop it entirely. */
  if (msgBytes > LogBufferSize)
    return;

  IrqHoldInfo ihi = irqlock_grab(&log_lock);

  if (!log_initialized)
    log_init();

  while ((freeList == NULL) || (LogAvail < msgBytes))
    log_clear_first();

  LogEntry *le = freeList;
  freeList = freeList->next;

  le->seqNo = seqNo++;
  le->start = LogBufferNext;
  le->len = msgBytes;
  le->next = 0;
  if (last)
    last->next = le;
  last = le;

  if (!first)
    first = le;
  if (!fetch)
    fetch = le;
 
  for (; *msg; msg++) {
    LogBuffer[LogBufferNext++] = *msg;
    LogBufferNext %= LogBufferSize;
  }

  irqlock_release(ihi);
}

/** @brief Copy out the text of the next log message to @p msgDest. */
size_t
log_copyout_next(char *msgDest, size_t bound)
{
  size_t out = 0;

  IrqHoldInfo ihi = irqlock_grab(&log_lock);

  if (fetch) {
    LogEntry *le = fetch;
    fetch = le->next;

    if (le->len < bound)
      bound = le->len;

    out = bound;

    size_t pos = le->start;
    
    while (bound--) {
      *msgDest++ = LogBuffer[pos++];
      pos %= LogBufferSize;
    }
  }

  irqlock_release(ihi);

  return out;
}

