/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Load module management.
 */

#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/string.h>
#include <kerninc/pstring.h>
#include <kerninc/ModInfo.h>
#include <kerninc/PhysMem.h>

ModInfo modules[MAX_MODULES];
uint32_t nModule = 0;

void
module_register(kpa_t start, kpa_t end, const char *nm)
{
  if (nModule == MAX_MODULES)
    fatal("Too many modules\n");

  modules[nModule].base = start;
  modules[nModule].end = end;
  strncpy(modules[nModule].imgName, nm, sizeof(modules[nModule].imgName));
  modules[nModule].needsRelease = true;
  modules[nModule].needsReloc = true;

  pmem_AllocRegion(modules[nModule].base, modules[nModule].end, 
		   pmc_RAM, pmu_ISLIMG, "module");

  printf("Module: [0x%0pP,0x%0pP] %s\n", modules[nModule].base, 
	 modules[nModule].end, nm);

  nModule++;
}

void
module_relocate(void)
{
  /* Multiboot helpfully puts the module files right above the
     kernel, which is exactly where we want our heap to go. Relocate
     them to highest available memory address. */
  for (size_t i = 0; i < MAX_MODULES; i++) {
    if (modules[i].base == modules[i].end)
      continue;

    if (modules[i].needsReloc == false)
      continue;

    kpa_t pa = 
      pmem_AllocBytes(&pmem_need_bytes, modules[i].end - modules[i].base,
		      pmu_ISLIMG, "module");

    assert(pa > modules[i].base);

    memcpy_ptop(pa, modules[i].base, modules[i].end - modules[i].base);

    /* Release the original location */
    pmem_AllocRegion(modules[i].base, modules[i].end, 
		     pmc_RAM, pmu_AVAIL, pmc_descrip(pmc_RAM));

    printf("Module: [0x%0pP, 0x%0pP]\n"
	   "    to: [0x%0pP, 0x%0pP].\n", 
	   modules[i].base,
	   modules[i].end,
	   pa,
	   modules[i].end - modules[i].base + pa);

    modules[i].end -= modules[i].base;
    modules[i].base = pa;
    modules[i].end += pa;
  }
}

