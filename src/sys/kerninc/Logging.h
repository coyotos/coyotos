#ifndef __KERNINC_LOGGING_H__
#define __KERNINC_LOGGING_H__
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Kernel logging interface.
 *
 * While the kernel internally uses printf and friends for
 * diagnostics, the capability interface to the log requires that we
 * know where the record boundaries are. The logging subsystem handles
 * that.
 */

#include <kerninc/ccs.h>
#include <hal/machine.h>
#include <hal/config.h>

/** @brief Atomically append a complete message to the log. */
void log_append(const char *msg, size_t len);

/** @brief Copy out the text of the next log message to @p msgDest,
 * returning number of bytes copied. */
size_t log_copyout_next(char *msgDest, size_t bound);

/** @brief Call this from a debugger, as needed. */
void log_dump();
#endif /* __KERNINC_LOGGING_H__ */
