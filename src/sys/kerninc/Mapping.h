#ifndef __KERNINC_MAPPING_H__
#define __KERNINC_MAPPING_H__
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Address mapping management structure.
 */

#include <stdint.h>
#include <coyotos/coytypes.h>
#include <kerninc/AgeList.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/mutex.h>
#include <hal/kerntypes.h>

/** @brief Allocate page table frames and management structures. */
__hal void pagetable_init(void);

/** @brief Determine how many page frames will be occupied by page
    tables. */
__hal size_t pagetable_reserve(size_t);

/** @brief Abstraction of a virtual address mapping context.
 *
 * Every architecture supported by Coyotos has some notion of a
 * Mapping abstraction. In hierarchical schemes, there are Mappings
 * that exist at multiple levels.
 *
 * Mapping structures define the units of potential address subspace
 * sharing. The @em uppermost mapping associated with a given process
 * (named by Process.curMap) defines the entire address space of that
 * process, and the asid value of that uppermost mapping gives an
 * architecture-dependent, globally unique name to that address space
 * for TLB purposes. On architectures providing hardware support for
 * ASIDs, the total number of top-level Mapping instances is currently
 * bounded by the number of hardware ASIDs, less one ASID value that
 * is reserved for kernel use. On architectures that do not have
 * ASIDs, a synthetic ASID value is used, and no such limit applies.
 *
 * There is a globally unique Mapping, KernMapping, that defines an
 * address space having a complete kernel map and no valid user-mode
 * mappings. This Mapping may be loaded onto any CPU, and should be
 * used when no current process is defined or when the top-level
 * mapping associated with the current process is destroyed.
 *
 * Much of the @em content of the Mapping structure is HAL-dependent.
 * On architectures that use hardware-defined page table structures,
 * the Mapping structure serves as the management structure for the
 * actual page tables.
 *
 * Note that the actual content of the Mapping structure is elsewhere;
 * the Mapping structure itself is mostly immutable "identity" information.
 *
 * The __immutable information can only be modified when the mappingsLock
 * is held @b and the mapping is not on any producer chain.
 */
typedef struct Mapping {
  /** @brief Links in object history list
   *
   * MUST BE FIRST!! 
   */
  __mappingsLock Ageable  age;

  /** @brief Next mapping structure on same product chain. 
   *
   * If chains get long, we may need to consider an alternative
   * linkage structure to facilitate removal. */
  __mappingsLock struct Mapping *nextProduct;

  /** @brief Pointer to the object that produced this page table. */
  __immutable MemHeader *producer;

  /** @brief Virtual address bits that must match in order for this
   * table to be appropriate for use.
   */
  __immutable coyaddr_t    match;

  /** @brief Virtual address bits that are significant for matching
   * purposes in order for this table to be appropriate for use.
   */
  __immutable coyaddr_t    mask;

  /** @brief Permission restrictions that must be applied to all
      entries in this table.
   */
  __immutable uint8_t restr;

  /** @brief Unique address space ID.
   *
   * On platforms that do not have an actual ASID, a synthetic ASID
   * should be used to reduce TLB flush requirements.
   */
  __immutable asid_t asid;

#if MAPPING_INDEX_BITS > 0
  /** @brief Level of page table */
  __immutable uint8_t level;

  __immutable size_t  userSlots;  /**< @brief @# of leading user-mode slots */

  /** @brief Physical address of this page table */
  __immutable kpa_t     pa;
#endif /* MAPPING_INDEX_BITS */
} Mapping;

__hal extern Mapping KernMapping;

#endif /* __KERNINC_MAPPING_H__ */
