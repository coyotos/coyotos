#ifndef __KERNINC_ANNOTATIONS_H__
#define __KERNINC_ANNOTATIONS_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Macro definitions to "remove" various qualifier-style
 * annotations that are present for documentation purposes.
 *
 * Entries in this file should consist of
 *
 *   @#define IDENT
 *
 * with no value defined. These may be ifdef'd so that other tools,
 * e.g. cqual, can see the qualifiers.
 *
 * This file is not explicitly included anywhere. It is included at
 * the compiler line by a "pre-include" directive.
 */

/** @brief Indicates that a declared procedure or variable is supplied
   by architecture-dependent code. */
#define __hal

/** @brief Indicates that a field must be altered under the
 * corresponding hash bucket mutex.
 *
 * If a field is marked __bucketmutex, it is necessary to hold the
 * hash bucked mutex for the buccket corresponding to the oid and type
 * of the object in order to manipulate the object. The issue is that
 * these fields must not be altered while an object is listed within
 * the (type,oid) hash table.
 *
 * When you have allocated a new object, you can set these fields, but
 * you must (?) grab the bucket mutex before calling the insert
 * routine. The insert routine does not grab the lock because this
 * would permit an insert/lookup race.
 *
 * When unloading an object, you must hold the bucket lock until the
 * I/O operation for the unload is interned. This is necessary to
 * avoid load/unload races. Similarly, when destroying an object, you
 * must hold the bucket lock until a racing load will correctly
 * analyze the fact of destruction.
 *
 * Coyotos is a second-chance cache, so barring startup oddities there
 * are no "free" object slots in the cache. An uninitialized
 * page/cappage cache slot will have it's OID set to a physical frame
 * OID value of the form (0xff00000000000000+pa), where pa is the
 * physical page frame address of the object. Non-page objects are set
 * to (0xff00000000000000+ndx), where ndx is the corresponding
 * per-type object vector index.
 *
 * Note that when converting to/from these quasi-physical frames, it
 * is necessary to hold the bucket mutex's for @em both the old and
 * new OID, to avoid modify/reload races.
 */
#define __bucket

/** @brief Indicates that the field can only be modified/examined
 * under the per-type agelist mutex.
 */
#define __agelist

/** @brief Indicates that the field can only be modified/examined
 * under the Cache.mappingsLock spinlock.
 */
#define __mappingsLock

/** @brief Indicates that a field is not altered following
 * initialization.
 *
 * Because initialization is nearly always a single CPU activity, any
 * field with this annocation can be acccessed, but not mutated, by
 * multiple CPUs without fear of concurrency-related complications.
 *
 * The notable exception to the "initialization happens on the
 * fabricating CPU" rule is global data structures that are
 * initialized by pre-SMP code, typically in arch_init(). Ironically,
 * the vector of per-CPU structures is one of these exceptions. */
#define __immutable

/** @brief Indicates that a field is private to the given CPU, and
 * must not be accessed from any other CPU.
 *
 * In most cases, such fields should be accessed using the MY_CPU(id)
 * form.
 *
 * Since all per-CPU fields are gathered into the CPU structure, this
 * macro should really only appear in that file. */
#define __mycpu


#endif /* __KERNINC_ANNOTATIONS_H__ */
