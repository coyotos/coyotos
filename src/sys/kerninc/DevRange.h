#ifndef __KERNINC_DEVRANGE_H__
#define __KERNINC_DEVRANGE_H__
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Interface to the kernel object cache. */

#include <stdbool.h>
#include <kerninc/annotations.h>
#include <kerninc/mutex.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/FreeList.h>

typedef struct DevRange {
  kpa_t base;
  kpa_t bound;

  uint32_t l2g;

  /** @brief Vector of object headers for this DevRange */
  Page *page;
} DevRange;

typedef struct DevRangeMgr_s {
  DevRange *range;

  size_t nDevRange;
  size_t nPagePerRange;

  AgeList freePages;
  AgeList activePages;
} DevRangeMgr_s;

extern DevRangeMgr_s DevRangeMgr;

/** @brief Compute the amount of heap space required for device
 * range data structures. */
extern size_t devrange_compute_required_heap();

/** @brief Initialize the device ranges. */
extern void devrange_init(void);

/** @brief Configure a given device range.
 *
 * Returns result code.
 */
extern uint64_t devrange_setup(size_t ndx, kpa_t base, kpa_t bound, 
			       uint32_t l2g);
 
/** @brief Deallocate an existing device range. */
extern uint64_t devrange_destroy(size_t ndx);

/** @brief gets a device memory page whose OID corresponds to known
 * address @p pa, creating it if @b andCreate is true.
 *
 * The returned object header is already locked.
 *
 * Device memory pages are not considered for ageing.
 *
 * @pre obhdr_invalidate has already been called.
 * @pre obhash bucket lock is held.
 */
struct Page *devpage_lookup(oid_t oid, bool andCreate, HoldInfo *hi);

void devpage_destroy(Page* pg);


#endif /* __KERNINC_DEVRANGE_H__ */
