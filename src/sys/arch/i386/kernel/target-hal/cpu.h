#ifndef I386_HAL_CPU_H
#define I386_HAL_CPU_H
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <coyotos/i386/pagesize.h>

struct CPU;

// static inline struct CPU *current_cpu() __attribute__((always_inline));

/** @brief Return pointer to CPU structure corresponding to currently
 * executing CPU.
 */
static inline struct CPU *current_cpu()
{
  register uint32_t sp asm ("esp");
  sp &= ~((KSTACK_NPAGES*COYOTOS_PAGE_SIZE)-1);
  return *((struct CPU **) sp);
}

#endif /* I386_HAL_CPU_H */
