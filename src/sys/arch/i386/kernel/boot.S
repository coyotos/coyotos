/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
	
	/** @file
	 * @brief Bootstrap and startup code.
	 */
#include <coyotos/i386/asm.h>
#include <coyotos/i386/pagesize.h>
#include <target-hal/config.h>
#include "IA32/CR0.h"
#include "IA32/CR4.h"
#include "IA32/PTE.h"
#include "IA32/PAE.h"
#include "IA32/EFLAGS.h"
#include "IA32/CPUID.h"

	/* This is a multiboot OS. We currently make no provision
	 * for other forms of native boot. If there comes to be a
	 * need for that, we should probably implement it using a
	 * trampoline bootstrap loader.
	 *
	 * On entry to the kernel, multiboot standard provides the
	 * following environment:
	 *
	 *  EAX:    magic value 0x2BADB002
	 *
	 *  EBX:    32-bit physical address of multiboot info
	 *          structure
	 *
	 *  CS:     32-bit RX flat model (0-4G) segment, selector 
	 *          unspecified
	 *
	 *  DS..SS: 32-bit RW flat model (0-4G) segment, selector 
	 *
	 *  A20 Gate: enabled
	 *
	 *  CR0:    PG cleared, PE set, others undefined
	 *  EFLAGS: VM cleared, IF cleared, others undefined
	 *
	 * Other registers undefined, specifically including ESP,
	 * GDTR, IDTR, and so forth. OS must establish a stack.
	 *
	 * Note that since IF is clear, interrupts are disabled 
	 * on entry.
	 *
	 * The multiboot specification can be found online at
	 *
	 * http://www.gnu.org/software/grub/manual/multiboot/
	 */

	.text
ENTRY(common_start)	
	/* *****************************************************
	 *
	 * RUNTIME CONDITIONS (each will be resolved in turn):
	 *
	 * BSS has been zeroed by BSP startup logic.
	 *
	 * No valid stack
	 *
	 * Interrupts are disabled.
	 *
	 * We are relocated at base=KVA, but running at base=0.
	 *
	 *******************************************************/
#if HAVE_CONSOLE
	movl	$0x076f0743,0x000b8000	 /* "Co" */
#endif
	/* *****************************************************
	 *
	 * CHECK FOR CPUID SUPPORT, THEN FOR PAE
	 *
	 *******************************************************/
	
	/* To check for CPUID support, we need a temporary stack.
	 * Set up %esp to temporarily point to where the stack *will* 
	 * be after relocation -- all state going on the stack here is 
	 * garbage, and can be thrown away after the CPUID process is 
	 * done.
	 *
	 * This is not intended to be a complete CPUID feature check.
	 * We will do that later. We are only trying to determine here
	 * whether the processor supports PAE.
	 */
	
	/* If AC bit cannot be modified, this is 486 or earlier, and *
	 * CPUID is not available. To check that, we need a stack so that
	 * we can use PUSHF and POPF. We will later overwrite %esp with
	 * a more appropriate value.
	 */
	movl	$EXT(cpu0_kstack_hi)-KVA, %esp
	
	pushf				/* push flags onto stack */
	xorl	$IA32_EFLAGS_AC,(%esp)
	movl	(%esp),%eax
	popf				/* pop and re-push flags to */
	pushf				/* check for AC bit change */
	cmpl	(%esp),%eax
	je	1f
	
	pushl	$EXT(message_prehistoric)-KVA
	call	fail_message
	
1:	/* Restore original flags */
	pushl	%eax
	popf
	
	/* CPU *might* support CPUID. See if CPUID bit is modifiable. */
	pushf				/* push flags onto stack */
	xorl	$IA32_EFLAGS_ID,(%esp)
	movl	(%esp),%eax
	popf				/* pop and re-push flags to */
	pushf				/* check for ID bit change */
	cmpl	(%esp),%eax
	je	1f
	
	pushl	$EXT(message_no_cpuid)-KVA
	call	fail_message
	
1:	/* Restore original flags */
	pushl	%eax
	popf

	/* We have CPUID insruction. Check that operation 1 (feature info)
	   is present. */
	xorl	%eax,%eax
	cpuid
	cmp	$0,%eax			/* Check if supports feature info */
	jg	1f
	pushl	$EXT(message_no_cpuid_op1)-KVA
	call	fail_message
		
1:	/* Determine what features are present. */
	movl	$1,%eax
	cpuid

	/* Selectively enable some features in CR4: */
	movl	%cr4,%ebx
	
	/* Enable page global extensions if present */
	movl	%edx,%eax
	andl	$IA32_CPUID_EDX_PGE,%eax
	jz	1f
	orl	$IA32_CR4_PGE,%ebx

1:	/* Enable large pages if present */
	movl	%edx,%eax
	andl	$IA32_CPUID_EDX_PSE,%eax
	jz	1f
	movb	$1,EXT(IA32_HavePSE)
	orl	$IA32_CR4_PSE,%ebx

1:	/* Enable debugging extensions if present */
	movl	%edx,%eax
	andl	$IA32_CPUID_EDX_DE,%eax
	jz	1f
	orl	$IA32_CR4_DE,%ebx

1:	movl	%ebx,%cr4

	/* Check for PAE mode and decide which way to build the map */
	movl	%edx,%eax
	andl	$IA32_CPUID_EDX_PAE,%eax
#if 1
	jnz	setup_pae_map
#endif
	
	/* Get us a valid mapping without delaying for anything else
	 * first. Note we have been loaded at 0x1000000, but we are
	 * relocated for 0xC1000000, so initializing the page table
	 * requires an adjustment to the KernPageDir directory
	 * address.
	 *
	 * The following arrangement is a temporary expedient, which
	 * is why we do NOT mark any of these entries global.  We will
	 * be re-initializing the kernel master mapping table more
	 * completely during early initialization.
	 *
	 * At this point, we are simply trying to establish a quick
	 * and dirty flat mapping that will make Physical [0g,2M)
	 * appear at virtual [0g,2M) and also at virtual [3g,2M). In
	 * legacy mode we are actually going to get the first 4M this
	 * way.
	 */
setup_legacy_map:
	/* Set up a legacy-style page table for the first 4Mbytes */
	movl	$EXT(KernPageTable)-KVA,%esi
	xorl	%edx,%edx
	orl	$(IA32_PTE_V|IA32_PTE_W|IA32_PTE_ACC|IA32_PTE_DRTY),%edx

	movl	$1024,%ecx
1:	movl	%edx,(%esi)
	addl	$0x1000,%edx
	addl	$4,%esi
	loop	1b
	
	movl	$EXT(KernPageDir)-KVA,%esi
	movl	$EXT(KernPageTable)-KVA,%edx
	orl	$(IA32_PTE_V|IA32_PTE_W|IA32_PTE_ACC|IA32_PTE_DRTY),%edx
	movl	%edx,(%esi)	/* Map it at VA=0 */
	movl	%edx,3072(%esi)	/* And again at VA=3G */
	
	/* Load KernPageDir into the master mapping register: */
	movl	$ EXT(KernPageDir)-KVA,%edx
	mov	%edx,%cr3
	
	
#if HAVE_CONSOLE
	movl	$0x076f0779,0x000b8004  /* "yo" */
#endif
	
	jmp	enable_paging
	
setup_pae_map:
	movl	$EXT(KernPageTable)-KVA,%esi
	xorl	%edx,%edx
	orl	$(IA32_PAE_V|IA32_PAE_W|IA32_PAE_ACC|IA32_PAE_DRTY),%edx
	
	movl	$512,%ecx
1:	movl	%edx,(%esi)
	movl	$0,4(%esi)
	addl	$8,%esi
	addl	$0x1000,%edx
	loop	1b

	movl	$EXT(KernPageDir)-KVA,%esi
	movl	$EXT(KernPageTable)-KVA,%edx
	orl	$(IA32_PAE_V|IA32_PAE_W|IA32_PAE_ACC|IA32_PAE_DRTY),%edx
	movl	%edx,(%esi)	/* Map it at VA=0 */
	movl	$0,4(%esi)
	
	/* Now set up the PDBR */
	movl	$EXT(KernPageDir)-KVA,%edx
	orl	$(IA32_PAE_V),%edx	/* On legacy PAE, *not* W, USER */
	
	/* Load KernPageDir into the PDBR: */
	movl	$EXT(KernPDPT)-KVA,%esi
	movl	%edx,(%esi)	/* lower bits, entry 0 */
	movl	$0,4(%esi)	/* upper bits, entry 0 */
	movl	$0,8(%esi)	/* lower bits, entry 1 */
	movl	$0,12(%esi)	/* upper bits, entry 1 */
	movl	$0,16(%esi)	/* lower bits, entry 2 */
	movl	$0,20(%esi)	/* upper bits, entry 2 */
	movl	%edx,24(%esi)	/* lower bits, entry 3 */
	movl	$0,28(%esi)	/* upper bits, entry 3 */
	
	movl	$EXT(KernPDPT)-KVA,%edx
	mov	%edx,%cr3
	
#if HAVE_CONSOLE
	movl	$0x076f0779,0x000b8004  /* "yo" */
#endif
	
	/* Set up the desired configuration in CR4. While we are here,
	 * initialize the debugging extensions and enable the TLB
	 * Global bit. Note that IA32_CR4_PSE is ignored in PAE mode, but 
	 * is safe to set.
	 *
	 * FIX:	we should also set MCE, OSFXSR, OSXMMEXCPT here when
	 * we have implemented support for those.
	 */
	mov	%cr4,%edx
	orl	$IA32_CR4_PAE,%edx
	mov	%edx,%cr4
	
enable_paging:	
#if HAVE_CONSOLE
	movl	$0x076f0774,0x000b8008 /* "to" */
	movl	$0x07200773,0x000b800c /* "s " */
#endif

	/* Set up the desired configuration in CR0. Note that
	 * we continue to rely on the GRUB-provided segment register
	 * values, because we haven't loaded GDTR yet.
	 *
	 * While we are here, set up alignment checking, kernel write
	 * protect, and the numerics unit.
	 *
	 * FIX:	Might want to delay setup of the numerics unit for 
	 * embedded processors. Anything else here that we should delay?
	 */
	mov	%cr0,%edx
	orl	$(IA32_CR0_PG|IA32_CR0_AM|IA32_CR0_WP|IA32_CR0_ET\
		|IA32_CR0_TS|IA32_CR0_MP|IA32_CR0_PE),%edx
	
#if HAVE_CONSOLE
	/* Preserve write-through mode until just before we go live. */
	orl	$(IA32_CR0_NW),%edx
#endif
	mov	%edx,%cr0
	
#if HAVE_CONSOLE
	movl	$0x07750752,0xc00b8010 /* "Ru" */
#endif

	/*
	 * WE ARE NOW RUNNING MAPPED, however, EIP is still a low-memory
	 * address. Fix that by doing a jump register to the next useful 
	 * address.
	 */
	movl	$drop_low_map,%edx
	jmp	*%edx
	
drop_low_map:	
	/* Drop the mapping alias at VA=0x0. How to do this depends on 
	 * whether we are running in PAE mode. 
	 */
	mov	%cr4,%edx
	andl	$IA32_CR4_PAE,%edx
	jz	drop_low_legacy_map
	
	/* Record the fact that we are using PAE for later use:	 */
	movb	$1,EXT(IA32_UsingPAE)
	
	/* Whack the low-memory mapping: */
	movl	$EXT(KernPDPT),%esi
	movl	$0,(%esi)	/* Kill mapping at VA=0 */
	movl	$0,4(%esi)
	jmp	reload_map

drop_low_legacy_map:
	movl	$EXT(KernPageDir),%esi
	movl	$0,(%esi)	/* Kill mapping at VA=0 */


reload_map:
	/* Reload the master mapping table pointer to flush the TLB
	 * of any dangling low mappings: 
	 */
	mov	%cr3,%edx
	mov	%edx,%cr3
	
setup_kstack:
	/* Now set up the stack for the first CPU (CPU0). Stacks for other 
	 * CPUs will be set up by the C code.
	 */
	movl	$EXT(cpu0_kstack_hi), %esp
	
	/* Set the curCPU pointer for CPU 0: */
	movl	$EXT(cpu_vec),EXT(cpu0_kstack_lo)
	
#if HAVE_CONSOLE
	movl	$0x0765076c,0xc00b8014 /* "le" */
#endif

	/* Clear flags register */
	pushl	$0
	popf

#if HAVE_CONSOLE
	movl	$0x07210773,0xc00b8018 /* "s!" */
#endif
	
start_kernel:
	call	EXT(kernel_main)
	
	/* This CPU is not supported. Say so "by hand":	 */
fail_message:
#if HAVE_CONSOLE
	popl	%edx
	/* Clear the screen: */
	movb	$0x07,%ah	/* White on black */
	movb	$0x20,%al	/* ASCII space */
	movl	$0x000b8000,%edi
	movl	$(25*80),%ecx
	rep
	stosw
	
	movl	$0x000b8000,%edi
	movl	%edx,%esi
	
1:	movb	(%esi),%al
	cmpb	$0,%al
	je	halt
	movw	%ax,(%edi)
	inc	%esi
	addl	$2,%edi
	jmp	1b
	
#endif
	
ENTRY(halt)
ENTRY(sysctl_halt)
1:	cli
	hlt
	jmp 1b

#if HAVE_CONSOLE
	.data
message_no_pae:
	.asciz "Coyotos does not support this CPU (need Pentium-II or later)"
message_prehistoric:
	.asciz "Coyotos does not support prehistoric processors"
message_no_cpuid:
	.asciz "Coyotos does not support this CPU (need CPUID)"
message_no_cpuid_op1:
	.asciz "Coyotos does not support this CPU (need CPUID op 0x1)"
#endif	
