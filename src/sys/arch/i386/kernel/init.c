/*
 * Copyright (C) 2005, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Low level, architecture-specific initialization.
 */

#include <stdbool.h>

#include <hal/kerntypes.h>
#include <hal/console.h>
#include <hal/machine.h>
#include <hal/config.h>
#include <hal/vm.h>
#include <hal/transmap.h>
#include <kerninc/assert.h>
#include <kerninc/ctype.h>
#include <kerninc/printf.h>
#include <kerninc/PhysMem.h>
#include <kerninc/string.h>
#include <kerninc/pstring.h>
#include <kerninc/ccs.h>
#include <kerninc/malloc.h>
#include <kerninc/util.h>
#include <kerninc/Cache.h>
#include <kerninc/DevRange.h>
#include <kerninc/CPU.h>
#include <kerninc/CommandLine.h>
#include <kerninc/util.h>

#include "IA32/CR4.h"

#include "kva.h"
#include "GDT.h"
#include "TSS.h"
#include "IRQ.h"
#include "PIC.h"
// #include "acpi.h"
#include "cpu.h"
#include "hwmap.h"

extern uint32_t _start[];
extern uint32_t _etext[];
extern uint32_t __syscallpg[];
extern uint32_t __esyscallpg[];
extern uint32_t __begin_maps[];
extern uint32_t __end_maps[];
extern uint32_t cpu0_kstack_lo[];
extern uint32_t cpu0_kstack_hi[];
extern uint32_t _data[];
extern uint32_t _end[];
extern uint32_t _pagedata[];
extern uint32_t _epagedata[];
extern uint32_t _rodata[];
extern uint32_t _erodata[];

extern void bsp_init();
extern void bsp_mem_config();
extern void bsp_protect_modules();
extern void hardclock_init();

static void 
setup_heap()
{
  /** @bug This needs to be modified to use large page mappings. */

  size_t heap_size = cache_compute_required_heap();
  heap_size += devrange_compute_required_heap();

  kva_t heap_base = align_up((kva_t)&_end, alignof(uintptr_t));
  kva_t heap_bound = heap_base + heap_size;
  heap_bound = align_up(heap_bound, COYOTOS_PAGE_SIZE);

  /* Heap is already backed (and mapped) to end of last page
     containing BSS. */
  kpa_t heap_backed_va = align_up(heap_base, COYOTOS_PAGE_SIZE);
  if (heap_backed_va > heap_base)
    pmem_AllocRegion(KVTOP(_end), KVTOP(heap_backed_va), pmc_RAM, 
		     pmu_KHEAP, "Kernel heap");

  assert(((heap_bound - heap_backed_va) % COYOTOS_PAGE_SIZE) == 0);

  /* For the moment, simply grab the heap as a single, contiguous
   * region. Since the heap works out to about 25% of physical
   * memory, this ought to succeed for all machine configurations
   * observed in the wild.
   *
   * @bug This really ought to be grabbing 4M units so that we can
   * use the large page mapping logic.
   */
  kpa_t pa = 
    pmem_AllocBytes(&pmem_need_pages, heap_bound - heap_backed_va,
		    pmu_KHEAP, "Kernel heap");

  /* We are overwriting some of the pre-initialized low mappings, so
   * set remap to true here.
   */
     
  kmap_map(heap_backed_va, pa, heap_bound - heap_backed_va, KMAP_W, true);
  heap_init(heap_base, heap_bound, heap_bound);
}


static void 
protect_kernel_memory()
{
  pmem_AllocRegion(KVTOP(_start), KVTOP(_etext), pmc_RAM, pmu_KERNEL,
		   "Kernel code");
  pmem_AllocRegion(KVTOP(_rodata), KVTOP(_erodata), pmc_RAM, pmu_KERNEL,
		   "Kernel rodata");
  pmem_AllocRegion(KVTOP(__syscallpg), KVTOP(__esyscallpg),
		   pmc_RAM, pmu_KERNEL,
		   "Syscall page");
  /* Following includes the transmap page(s) */
  pmem_AllocRegion(KVTOP(__begin_maps), KVTOP(__end_maps), pmc_RAM, pmu_KERNEL,
  		   "Kernel mapping pages");
  pmem_AllocRegion(KVTOP(cpu0_kstack_lo), KVTOP(cpu0_kstack_hi), 
		   pmc_RAM, pmu_KERNEL, "CPU0 stack");
  if (_pagedata != _epagedata)
    pmem_AllocRegion(KVTOP(_pagedata), KVTOP(_epagedata), pmc_RAM, pmu_KERNEL,
		     "Kernel page data");
  pmem_AllocRegion(KVTOP(_data), KVTOP(_end), pmc_RAM, pmu_KERNEL,
		   "Kernel data/bss");
}


/**
 * @brief Perform architecture-specific initialization.
 *
 * arch_init() is the first thing called from the Coyotos kern_main()
 * procedure. On the Pentium, the situation at the time kern_main() is
 * entered is that we have a valid virtual map, and we are running in
 * 32-bit protected mode (courtesy of Grub), but we haven't got our
 * own GDT or LDT or IDT loaded. Fixing this is the first order of
 * business, because we will start missing clock interrupts if we
 * don't get this done quickly.
 *
 * @section init_theory Initialization: Theory of Operation
 *
 * The order of initialization is critical and delicate. Here is a
 * quick explanation.
 *
 * On startup, boot.S has either left us in PAE or legacy translation
 * mode (according to <tt>UsingPAE</tt>) and has constructed a 1:1 map
 * of the V[0:2M-1] => P[0:2M-1] and also V[3G,3G+2M-1] => P[0:2M-1].
 * More precisely, it built one leaf page table worth of mappings, and
 * has recorded this in two places in the page directory. In "legacy
 * PTE" mode this is actually a [0:4M-1] region, but we shouldn't make
 * assumptions about this. One of our goals is going to be to abandon
 * this mapping fairly quickly, but note that we do need to be careful
 * about references into memory above 2M until we can deal with this.
 *
 * <b>Order of Bringup</b>
 *
 * <ul>
 * <li>
 * We will want to do diagnostics as early as possible, so bring up
 * the console first.  The console uses only statically preallocated
 * storage, and we are taking advantage of the fact that the BIOS has
 * already initialized the video subsystem for us (on other boards we
 * might need to init the display and deal with font issues here).
 *
 * The kernel will not own the console forever. Just before
 * transferring control to the first non-kernel code we will disable
 * further use of the display.
 * </li>
 * <li>
 * Call <tt>pmem_init()</tt>, establishing the total addressable
 * physical range. This creates the initial "hole" from which all
 * physical ranges are created.
 *
 * As with the console, the number of physical memory ranges is
 * statically fixed at compile time. No memory allocation is done
 * here.
 * </li>
 * <li>
 * Configure physical memory. That is: we scan the
 * multiboot-supplied list of memory regions and tell the PhysMem
 * allocator about them. Note that PhysMem is still using statically
 * allocated entries, so we aren't stepping on anything here.
 *
 * At the end of this phase we pre-allocate the regions containing the
 * BIOS area, the Extended BIOS Data Area, the kernel code/data, and
 * the CPU0 stack. The purpose of this preallocation is to ensure that
 * nothing steps on these regions in later allocations.
 *
 * At the end of this stage we compute an initial estimate of the
 * total number of physical pages available to us.
 * </li>
 * <li>
 * Grab a snapshot of the total number of available physical pages.
 * This will guide us later when we allocate various caches.
 * </li>
 * <li>
 * Protect the modules from interference until we can deal with them
 * properly. We do this by marking them allocated in the physical
 * range map.
 * </li>
 * <li>
 * Process command line arguments, and protect the command line itself
 * from interference by pre-allocating its physical memory location.
 * We need to do this because GRUB does not tell us about the regions
 * that it has dynamically allocated for its own structures.
 * </li>
 * <li>
 * Allocate the kernel heap. Once this is done we can do dynamic
 * allocations for various things, notably to save the command line in
 * a safe place.
 * </li>
 * <li>
 * Reserve memory space for page tables.
 * </li>
 * <li>
 * Scan CPUs to figure out how many we have and allocate the necessary
 * per-CPU structures and per-CPU private mappings.
 * </li>
 * <li>
 * Set up GDT, LDT, TSS, and IRQ tables.
 * </li>
 * </ul>
 *
 * We now return to the main-line code so that it can initialize the
 * object cache. It will call arch_cache_init() to let us populate the
 * object cache and release module storage.
 */
size_t heap_size;
void 
arch_init(void)
{
  bsp_init();

  /* Initialize the console output first, so that we can get
   * diagnostics while the rest is running.
   */
  console_init();

  /* Initialize the CPU structures, since we need to call things that
   * want to grab mutexes.
   */
  for (size_t i = 0; i < MAX_NCPU; i++)
    cpu_construct(i);

  init_gdt();
  init_tss();

  /* Initialize the transient map, so that later code can access
     arbitrary parts of memory. */
  transmap_init();

#if 0
  {
    uint32_t cr4;
    GNU_INLINE_ASM("mov %%cr4,%0\n"
		   : "=q" (cr4));
    if (cr4 & IA32_CR4_PAE)
      printf("We appear to be running in PAE mode.\n");
    else
      printf("Running with legacy mapping.\n");
  }
#endif

  /* Initialize the BSP. By the time this returns, information about
   * available physical memory will be populated and any precious
   * regions of physical memory will be protected from allocation.
   */

  bsp_mem_config();

  protect_kernel_memory();

  /* Need to get this estimate BEFORE we protect the multiboot
   * regions, because we are going to release those back into the pool
   * later, and if we don't count them now we will end up
   * under-supplied with header structures for them.
   */
  size_t totPage = 
    pmem_Available(&pmem_need_pages, COYOTOS_PAGE_SIZE, false);

  bsp_protect_modules();

  cmdline_process_options();

  if (cmdline_has_option("noioapic"))
    use_ioapic = false;
  else if (cmdline_has_option("ioapic"))
    use_ioapic = true;

  /* Find all of our CPUs. Also checks the ACPI tables, which we
     should probably do separately. Probing the ACPI tables may have
     the side effect of updating the execption vector table as we
     discover interrupt sources. */
  (void) cpu_probe_cpus();

  printf("%d pages initially available\n", totPage);

  size_t nReservedPage = (cpu_ncpu - 1) * KSTACK_NPAGES;
  nReservedPage += pagetable_reserve(totPage - nReservedPage);

  cache_estimate_sizes(PAGES_PER_PROCESS, totPage - nReservedPage);

  setup_heap();

  pagetable_init();

  //  pmem_showall();

  cpu_scan_features();

  /* Initialize the hardware exception vector table. */
  vector_init();
  
  printf("CPU0: GDT/LDT, ");
  load_gdtr_ldtr();		/* for CPU 0 */

  printf("TSS, ");
  load_tr();

  irq_init();			/* for CPU 0 */

  printf(" established\n");

  cpu_init_all_aps();

  //  pmem_showall();

  // printf("Test of breakpoint:\n");
  // GNU_INLINE_ASM ("int3");

  hardclock_init();

  printf("Enabling interrupts\n");
  GNU_INLINE_ASM ("sti");
}
