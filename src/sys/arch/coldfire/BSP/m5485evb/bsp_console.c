/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdbool.h>
#include <inttypes.h>
#include <hal/console.h>
#include <MCF/PSC.h>
#include "bsp.h"

static bool console_live = true;

static void 
console_putc(char c)
{
  if (!console_live)
    return;

  /* Wait until there is room in the outgoing FIFO */
  while (!(*MCF_PSC_SR(DEBUG_UART_PORT) & MCF_PSC_SR_TXRDY))
    ;

  *MCF_PSC_TB(DEBUG_UART_PORT) = (uint8_t) c;
}

void 
console_puts(char *msg, size_t len)
{
  while (*msg && len) {
    console_putc(*msg++);
    len--;
  }
}

static bool haveUserConsole = false;

void 
console_haveUserConsole()
{
  haveUserConsole = true;
}

void
console_handoff_to_user()
{
  if (!haveUserConsole)
    return;

  console_live = false;
}

void
console_init()
{
  /* All done from bsp_init() now. */
}
