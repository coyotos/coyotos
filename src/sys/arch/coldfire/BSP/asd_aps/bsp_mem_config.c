/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief BSP memory configuration for m5485evb platform.
 */

#include <kerninc/printf.h>
#include <kerninc/PhysMem.h>
#include <kerninc/string.h>
#include <kerninc/pstring.h>
#include <kerninc/CommandLine.h>
#include <kerninc/ModInfo.h>
#include <kerninc/Cache.h>
#include <kerninc/util.h>
#include "../../kernel/hwmap.h"
#include "../../kernel/kva.h"
#include <BSP/defs.h>
#include "bsp.h"

extern uint8_t _mkimage_start[];
extern uint8_t _mkimage_end[];

/** @brief Initialize physical memory.
 *
 * This routine is <em>extremely</em> board-specific.
 *
 * @todo This routine is *supposed* to get the physical memory layout,
 * but I have just determined that grub under QEMU isn't providing all
 * of the information that I want to gather, so I need to check what
 * it does on real hardware. The issue is that I want the ROM
 * locations as well. Drat!
 * 
 * @todo The case !MBI_MMAP and MBI_VALID should only appear on
 * ancient legacy BIOS's that are no longer shipping. It is not clear
 * whether we should still be supporting this.
 */
static void
config_physical_memory(void)
{
  /* This platform has a 16 Mbyte SDRAM mapped at 0xc0000000 */

  pmem_AllocRegion(BSP_defs_SDRAM_BASE, 
		   BSP_defs_SDRAM_BOUND, pmc_RAM, pmu_AVAIL,
		   pmc_descrip(pmc_RAM));

#if 0
  pmem_AllocRegion(BSP_defs_FlexBus_CS0_BASE,
		   BSP_defs_FlexBus_CS0_BOUND, pmc_ROM, pmu_AVAIL,
		   pmc_descrip(pmc_ROM));
#endif
}

void
bsp_mem_config()
{
  pmem_init(0, ~(uintptr_t)0u);

  /* Need to get the physical memory map before we do anything else. */
  config_physical_memory();
}

void
bsp_protect_modules()
{
  /* Protect embedded mkimage (if any) from being overwritten. */
  if (_mkimage_start != _mkimage_end)
    module_register(KVTOP(_mkimage_start), KVTOP(_mkimage_end),
		    "Emb. Image");
  module_relocate();
}
