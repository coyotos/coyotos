/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief BSP initialization for m5485evb platform.
 */

#include "bsp.h"
#include <inttypes.h>
#include <MCF/GPIO.h>
#include <MCF/PSC.h>
#include <BSP/defs.h>

static void
gpio_init()
{
  /* Do not let ethernet signals float. */
  *MCF_GPIO_PAR_FECI2CIRQ = 0
    | MCF_GPIO_PAR_FECI2CIRQ_E1MDC_E1MDC
    | MCF_GPIO_PAR_FECI2CIRQ_E1MDIO_E1MDIO
    | MCF_GPIO_PAR_FECI2CIRQ_E1MII
    | MCF_GPIO_PAR_FECI2CIRQ_E1_7
    | MCF_GPIO_PAR_FECI2CIRQ_E0MDC
    | MCF_GPIO_PAR_FECI2CIRQ_E0MDIO
    | MCF_GPIO_PAR_FECI2CIRQ_E0MII
    | MCF_GPIO_PAR_FECI2CIRQ_E0_7
    ;
}

static void 
uart_init(uint32_t port, uint32_t baud)
{
  /* Set up UART for debug console use. The original sequence came
     from the dBUG monitor and the M5485RM. */

  /* Example in manual shows 33.33Mhz, but on MCF5485evb, sys_clk
     comes up at 100Mhz, which is 10ns cycle time. */
  const uint32_t sys_clk = BSP_defs_SYSCLK;
  const uint32_t divider = sys_clk / (baud * 16 * 2);

  *MCF_PSC_CR(port) = MCF_PSC_CR_MISC_RESET_TX;
  *MCF_PSC_CR(port) = MCF_PSC_CR_MISC_RESET_RX;
  *MCF_PSC_CR(port) = MCF_PSC_CR_MISC_RESET_MODE;


  /* We want "no parity", 8bit, error mode "character": */
  *MCF_PSC_MR1(port) = MCF_PSC_MR1_MODE_PARITY_NONE|MCF_PSC_MR1_BITS_B8;

  /* We want CM=normal, 1 stop bit nothing else (per dBUG) */
  *MCF_PSC_MR2(port) = MCF_PSC_MR2_CM_NORMAL | MCF_PSC_MR2_SB_ONE;

  /* Use PSC timer for Rx/Tx baud rate */
  *MCF_PSC_CSR(port) = MCF_PSC_CSR_RCSEL_val(0xd) | MCF_PSC_CSR_TCSEL_val(0xd);

  /* For kernel console use, we will use polled mode, so we don't
     want to enable any interrupts at all. */
  *MCF_PSC_IMR(port) = 0;

  /* Divide sys_clk by 108 to get baud rate. If sys_clk is 33.33 Mhz,
   * baud rate is 9600 bps. */
  *MCF_PSC_CTUR(port) = (divider >> 8) & 0xffu;
  *MCF_PSC_CTLR(port) = (divider & 0xffu);

  /* Enable transmitter, receiver. */
  *MCF_PSC_CR(port) = MCF_PSC_CR_TXC_ENABLE|MCF_PSC_CR_RXC_ENABLE;
}

static void 
psc_init()
{
#if DEBUG_UART_PORT > 3
#error "DEBUG_UART_PORT must be 0 <= port <= 3"
#endif

  /* Before we can actually configure the UART, we need to do some
     GPIO configuration. */
  if (DEBUG_UART_PORT == 0) 
    *MCF_GPIO_PAR_PSC0 = 0
#ifdef HARDWARE_FLOW_CONTROL
      /* CTS/RTS for hardware flow control */
      | MCF_GPIO_PAR_PSC0_CTS0_CTS_bar | MCF_GPIO_PAR_PSC0_RTS0_RTS_bar
#endif
      | MCF_GPIO_PAR_PSC0_RXD0|MCF_GPIO_PAR_PSC0_TXD0
      ;
  else if (DEBUG_UART_PORT == 1)
    *MCF_GPIO_PAR_PSC1 = 0
      /* CTS/RTS for hardware flow control */
#ifdef HARDWARE_FLOW_CONTROL
      | MCF_GPIO_PAR_PSC1_CTS1_CTS_bar | MCF_GPIO_PAR_PSC1_RTS1_RTS_bar
#endif
      | MCF_GPIO_PAR_PSC1_RXD1|MCF_GPIO_PAR_PSC1_TXD1
    ;
  else if (DEBUG_UART_PORT == 2)
    *MCF_GPIO_PAR_PSC2 = 0
      /* CTS/RTS for hardware flow control */
#ifdef HARDWARE_FLOW_CONTROL
      | MCF_GPIO_PAR_PSC2_CTS2_CTS_bar | MCF_GPIO_PAR_PSC2_RTS2_RTS_bar
#endif
      | MCF_GPIO_PAR_PSC2_RXD2|MCF_GPIO_PAR_PSC2_TXD2
    ;
  else if (DEBUG_UART_PORT == 3)
    *MCF_GPIO_PAR_PSC3 = 0
      /* CTS/RTS for hardware flow control */
#ifdef HARDWARE_FLOW_CONTROL
      | MCF_GPIO_PAR_PSC3_CTS3_CTS_bar | MCF_GPIO_PAR_PSC3_RTS3_RTS_bar
#endif
      | MCF_GPIO_PAR_PSC3_RXD3|MCF_GPIO_PAR_PSC3_TXD3
    ;

  /* Put PSC in UART mode */
  *MCF_PSC_SICR(DEBUG_UART_PORT) = MCF_PSC_SICR_SIM_UART;

  uart_init(DEBUG_UART_PORT, 115200);
}

void
bsp_init()
{
  gpio_init();
  psc_init();
}
