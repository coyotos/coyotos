set confirm no
set arch m68k:cfv4e
target extended-remote localhost:1234

source util.gdb

monitor reset

load THE_IMAGE

echo Adding breakpoint on halt():\n
b halt

echo Coyotos Debugger Support loaded.\n
