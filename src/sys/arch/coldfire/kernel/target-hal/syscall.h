#ifndef COLDFIRE_HAL_SYSCALL_H
#define COLDFIRE_HAL_SYSCALL_H
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System runtime library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330 Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Machine-dependent system call support.
 *
 * These are the procedures that know how to get/set the various
 * system call parameters.
 *
 * We mostly aren't going to be able to maintain live values in
 * registers at the system call boundary, so the main priority is to
 * ensure that any @em outgoing parameters on the receiver side are
 * passed in regsters. A second priority is to ensure that any
 * parameters consulted from the receive side fall in registers. In
 * order of priority:
 *
 * Outgoing Params:
 *   DW0-DW3
 *   PP,
 *   sndLen [OUT]
 *   epID (server receive case)
 *   DW4-DW7
 * Consulted Params:
 *   rcvBound
 *   rcvPtr
 *   cdest[0:3]
 *
 * For coldfire, I have done the following assignment:
 *
 *   DW0:DW3    %d0:%d3
 *   PP[OUT]    %a1
 *   invCap[IN] %a1
 *   epID       %a3:%a4
 *   sndLen     %a2
 *   DW4:DW7    %d4-%d7
 *
 * Recall that %a7 is SP and %a6 is FP; it is preferable not to
 * perturb these. The register assignment above leaves %a0 to point to
 * the parameter block and %a5 for scratch purposes on exit.  This is
 * enough for the system call trap stub to get things saved to their
 * proper message structure locations within the remaining available
 * address registers.
 */

#include <kerninc/Process.h>
#include <coyotos/syscall.h>
#include <target-hal/vm.h>

/** @brief Return TRUE if the non-registerized parameter block falls
 * within valid user-mode addresses according to the rules of the
 * current architecture.
 */
static inline bool valid_param_block(Process *p)
{
  /* Maximum stack-based parameter block is 87 bytes unless we are
     using the scatter/gather engine. */
  return (vm_valid_uva(p, p->state.regs.fix.a[0]) &&
	  vm_valid_uva(p, p->state.regs.fix.a[0] + sizeof(InvParameterBlock_t) - 1));
}

/* TEMPORARY: */
#include <kerninc/printf.h>
/** @brief Fetch input parameter word @p pw from the appropriate
 * architecture-dependent location. */
static inline uintptr_t 
get_paramblock_pw(Process *p, size_t pw)
{
  return ((InvParameterBlock_t *) p->state.regs.fix.a[0])->pw[pw];
}

/** @brief Fetch input parameter word @p pw from the appropriate
 * architecture-dependent location.
 *
 * Sender-side registers won't remain live by the time we get
 * here. The main reason to put things in registers has to do with
 * what happens at user level on the receiver side after restore. We
 * want to use registers to avoid write-back to memory, but we don't
 * want to overburden the register file.
 */
static inline uintptr_t 
get_pw(Process *p, size_t pw)
{
  switch (pw) {
  case IPW_DW(0):
    return p->state.regs.fix.d[0];
  case IPW_DW(1):
    return p->state.regs.fix.d[1];
  case IPW_DW(2):
    return p->state.regs.fix.d[2];
  case IPW_DW(3):
    return p->state.regs.fix.d[3];
  case IPW_DW(4):
    return p->state.regs.fix.d[4];
  case IPW_DW(5):
    return p->state.regs.fix.d[5];
  case IPW_DW(6):
    return p->state.regs.fix.d[6];
  case IPW_DW(7):
    return p->state.regs.fix.d[7];
  case IPW_RCVBOUND:
  case IPW_RCVPTR:
  case IPW_RCVCAP+0:
  case IPW_RCVCAP+1:
  case IPW_RCVCAP+2:
  case IPW_RCVCAP+3:
    bug("Attempt to fetch rcv parameter with get_pw()\n");
  default:
    return get_paramblock_pw(p, pw);
  }
}

/** @brief Fetch receive parameter word @p pw from the appropriate
 * architecture-dependent location. */
static inline uintptr_t 
get_rcv_pw(Process *p, size_t pw)
{
  switch (pw) {
  case 0:
    return p->state.regs.fix.d[0];
  case IPW_RCVBOUND:
    return p->state.regs.fix.a[1];
  case IPW_RCVPTR:
    return p->state.regs.fix.a[2];
  case IPW_RCVCAP+0:
  case IPW_RCVCAP+1:
  case IPW_RCVCAP+2:
  case IPW_RCVCAP+3:
    bug("Attempt to fetch rcv cap through get_rcv_pw()\n");
  default:
    bug("Bad fetch of receive paramater word\n");
  }
}

/** @brief Save the necessary architecture-dependent parameter words
 * that are not transferred across the system call boundary in
 * registers.
 */
static inline void
save_soft_parameters(Process *p)
{
  InvParameterBlock_t *pb = (InvParameterBlock_t *) p->state.regs.fix.a[0];

  p->state.regs.soft.rcvBound = pb->rcvBound;
  p->state.regs.soft.rcvPtr = (uint32_t) pb->rcvPtr;

  /* IPW_RCVCAP0..3 */
  p->state.regs.soft.cdest[0] = pb->rcvCap[0];
  p->state.regs.soft.cdest[1] = pb->rcvCap[1];
  p->state.regs.soft.cdest[2] = pb->rcvCap[2];
  p->state.regs.soft.cdest[3] = pb->rcvCap[3];
}

/** @brief Copy received soft params back to user.
 *
 * This is @em always executed in the current address space.
 */
static inline void
copyout_soft_parameters(Process *p)
{
}

/** @brief Store input parameter word @p i to the appropriate
 * architecture-dependent INPUT location. Used in transition to
 * receive phase. */
static inline void 
set_pw(Process *p, size_t pw, uintptr_t value)
{
  switch (pw) {
  case OPW_DW(0):
    p->state.regs.fix.d[0] = value;
    return;
  case OPW_DW(1):
    p->state.regs.fix.d[1] = value;
    return;
  case OPW_DW(2):
    p->state.regs.fix.d[2] = value;
    return;
  case OPW_DW(3):
    p->state.regs.fix.d[3] = value;
    return;
  case OPW_DW(4):
    p->state.regs.fix.d[4] = value;
    return;
  case OPW_DW(5):
    p->state.regs.fix.d[5] = value;
    return;
  case OPW_DW(6):
    p->state.regs.fix.d[6] = value;
    return;
  case OPW_DW(7):
    p->state.regs.fix.d[7] = value;
    return;
  case OPW_PP:
    p->state.regs.fix.a[1] = value;
    return;
  case OPW_SNDLEN:
    p->state.regs.fix.a[2] = value;
    return;
  default:
    bug("Do not yet know how to store parameter word %d\n", pw);
  }
}

/** @brief Set receiver epID to incoming epID value. */
static inline void 
set_epID(Process *p, uint64_t epID)
{
  p->state.regs.fix.a[3] = epID;
  p->state.regs.fix.a[4] = epID >> 32;
}

/** @brief Fetch receiver epID for matching. */
static inline uint64_t 
get_rcv_epID(Process *p)
{
  uint64_t epID = p->state.regs.fix.a[4];
  epID <<= 32;
  epID |=  p->state.regs.fix.a[3];
  return epID;
}

static inline uintptr_t
get_icw(Process *p)
{
  return (get_pw(p, 0));
}

static inline void
set_icw(Process *p, uintptr_t val)
{
  return (set_pw(p, 0, val));
}

static inline caploc_t
get_invoke_cap(Process *p)
{
  return (caploc_t) p->state.regs.fix.a[1];;
}

static inline caploc_t
get_snd_cap(Process *p, size_t cap)
{
  InvParameterBlock_t *pb = (InvParameterBlock_t *) p->state.regs.fix.a[0];

  return pb->sndCap[cap];
}

static inline caploc_t
get_rcv_cap(Process *p, size_t cap)
{
  return p->state.regs.soft.cdest[cap];
}
#endif /* COLDFIRE_HAL_SYSCALL_H */
