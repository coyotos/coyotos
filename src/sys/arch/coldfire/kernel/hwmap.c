/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Manipulation of the hardware mapping table.
 */

#include <stddef.h>
#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/Depend.h>
#include <kerninc/Mapping.h>
#include <kerninc/RevMap.h>
#include <kerninc/event.h>
#include <kerninc/AgeList.h>
#include <kerninc/Cache.h>
#include <hal/transmap.h>

#include "hwmap.h"

#define N_USER_MAPPINGS 255

Mapping KernMapping;
Mapping UserMapping[N_USER_MAPPINGS]; /* One per user ASID. ASID 0
					 reserved for kernel use. */

AgeList MappingAgeList = { { &MappingAgeList.list, &MappingAgeList.list, } } ;

/* Following are placeholder implementations */
void
global_tlb_flush(asid_t asid)
{
  local_tlb_flush(asid);
}

void
local_tlb_flush(asid_t asid)
{
  flags_t flags = locally_disable_interrupts();

  coldfire_set_asid(asid);
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_CAS;
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_CAS|MCF_MMU_MMUOR_ITLB;

  coldfire_set_asid(MY_CPU(curMap)->asid);

  locally_enable_interrupts(flags);
}

void
local_tlb_flushva(asid_t asid, kva_t va)
{
  flags_t flags = locally_disable_interrupts();

  coldfire_set_asid(asid);

  *MCF_MMU_MMUAR = va & 0xfffffffe;

  /* Search DTLB */
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_STLB|MCF_MMU_MMUOR_ADR;

  if (*MCF_MMU_MMUSR & MCF_MMU_MMUSR_HIT) {
    /* Read the entry */
    *MCF_MMU_MMUOR = (MCF_MMU_MMUOR_ACC|MCF_MMU_MMUOR_RW);
    /* Invalidate it */
    *MCF_MMU_MMUTR = 0;
    /* Rewrite it. */
    *MCF_MMU_MMUOR = MCF_MMU_MMUOR_ACC;
  }
    
  /* Repeat for ITLB */
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_STLB|MCF_MMU_MMUOR_ADR|MCF_MMU_MMUOR_ITLB;

  if (*MCF_MMU_MMUSR & MCF_MMU_MMUSR_HIT) {
    /* Read the entry */
    *MCF_MMU_MMUOR = (MCF_MMU_MMUOR_ACC|MCF_MMU_MMUOR_RW|MCF_MMU_MMUOR_ITLB);
    /* Invalidate it */
    *MCF_MMU_MMUTR = 0;
    /* Rewrite it. */
    *MCF_MMU_MMUOR = MCF_MMU_MMUOR_ACC|MCF_MMU_MMUOR_ITLB;
  }

  coldfire_set_asid(MY_CPU(curMap)->asid);

  locally_enable_interrupts(flags);

#if 0
  /* Temporarily: */
  local_tlb_flush(asid);
#endif
}

void
pagetable_init(void)
{
  printf("Initializing mappings\n");

  ageable_init(&KernMapping.age);
  KernMapping.asid = 0;
  MY_CPU(curMap) = &KernMapping;

  for (size_t i = 0; i < N_USER_MAPPINGS; i++) {
    ageable_init(&UserMapping[i].age);
    UserMapping[i].asid = i + 1;
    agelist_addBack(&MappingAgeList, &UserMapping[i]);
  }
}

/** @brief Make the mapping @p m undiscoverable by removing it from
 * its product chain. 
 *
 * @bug This should be generic code, but it isn't obvious what source
 * file to stick it in.
 */
static void
mapping_make_unreachable(Mapping *m)
{
  /* Producer chains are guarded by the Cache.mappingsLock. Okay to
     fiddle them here. */

  MemHeader *hdr = m->producer;
  Mapping **mPtr = &hdr->products;

  while (*mPtr != m)
    mPtr = &((*mPtr)->nextProduct);

  /* We really *should* actually be *on* the product list: */
  assert(*mPtr == m);

  *mPtr = m->nextProduct;
}

static Mapping *
mapping_alloc(void)
{
  assert(mutex_isheld(&Cache.mappingsLock));

  Mapping *map = agelist_oldest(&MappingAgeList);

  if (map->producer) {
    /* whack any outstanding pointers to this mapping */
    rm_whack_mapping(map);
    
    /* and remove it from the product chain. */
    mapping_make_unreachable(map);
  }

  /* Re-initialize mapping table to safe state */

  map->match = 0;
  map->mask = 0;
  map->restr = 0;

  /* There is no need to re-initialize the mapping table, since we
     don't presently HAVE an explicit mapping table, but we do need to
     flush the corresponding ASID from the TLB */

  global_tlb_flush(map->asid);

  /* Newly allocated, so move to front. */
  agelist_remove(&MappingAgeList, map);
  agelist_addFront(&MappingAgeList, map);

  return map;
}

Mapping *
mapping_get(MemHeader *hdr,
	    coyaddr_t guard, coyaddr_t mask, size_t restr)
{
  HoldInfo hi = mutex_grab(&Cache.mappingsLock);
  Mapping *cur;

  for (cur = hdr->products;
       cur != NULL;
       cur = cur->nextProduct) {
    if (cur->match == guard &&
	cur->mask == mask &&
	cur->restr == restr) {

      agelist_remove(&MappingAgeList, cur);
      agelist_addFront(&MappingAgeList, cur);

      goto done;
    }
  }
  
  /* If not found, allocate a new one: */
  cur = mapping_alloc();

  cur->match = guard;
  cur->mask = mask;
  cur->restr = restr;

  cur->producer = hdr;
  cur->nextProduct = hdr->products;
  hdr->products = cur;

 done:
  mutex_release(hi);
  return cur;
}

void
memhdr_destroy_products(MemHeader *hdr)
{
  HoldInfo hi = mutex_grab(&Cache.mappingsLock);

  Mapping *map;

  while ((map = hdr->products) != NULL) {
    /* Make this page table undiscoverable. */
    mapping_make_unreachable(map);

    rm_whack_mapping(map);

    agelist_remove(&MappingAgeList, map);
    agelist_addBack(&MappingAgeList, map);
  }

  mutex_release(hi);
}

void
depend_entry_invalidate(const DependEntry *entry, int slot)
{
  global_tlb_flush(entry->map->asid);
}

void
rm_whack_pte(struct Mapping *map,  size_t slot)
{
  global_tlb_flush(map->asid);
}

#if 0
void
hwmap_enable_low_map()
{
  if (UsingPAE)
    KernPDPT.entry[0] = KernPDPT.entry[3];
  else
    KernPageDir[0] = KernPageDir[768];
}

void
hwmap_disable_low_map()
{
  if (UsingPAE)
    PTE_CLEAR(KernPDPT.entry[0]);
  else
    PTE_CLEAR(KernPageDir[0]);
}
#endif

