/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief LAPIC interrupt controller support.
 */

#include <kerninc/printf.h>
#include <kerninc/vector.h>
#include "IRQ.h"
#include "chips/MCF/ICU.h"

/** @brief Initial assignment for hardware interrupt priorities.
 *
 * Assigning a priori priorities for this part is difficult, because a
 * bunch of qualitatively different things are mixed together on the
 * chip: units, bus arbitrators, DMA subsystems, and so
 * forth. Further, there is significant cross-connect flexibility on
 * the part permitting drivers to (e.g.) wire subsystems to DMA
 * channels directly, and the associated configuration decisions can
 * motivate different answers for interrupt priorities.  We need to
 * choose @em something as an initial configuration, thus the
 * following table.
 *
 * The "theory of operation" here is as follows:
 *
 * -# Timers need the highest priority, and (ignoring NMI) nothing
 *    should have higher priority than the scheduling slice timer.
 * -# Interrupts related to bus arbitration and management failure
 *    should have higher priority than interrupts related to content
 *    and/or units on the bus. If things are properly configured,
 *    these should be rare, but urgent when they occur.
 * -# CAN busses support hard real-time, and therefore get high
 *    priority.
 * -# Ethernet comes next.
 * -# Security engine comes next (somewhat arbitrarily).
 * -# Serial and I2C at the bottom.
 *
 * This set of assignments does not consider USB endpoints, comm
 * timers, or DMA interrupts, because I don't yet have any decent
 * intuition for those. On M5485, the USB interrupts aren't really an
 * issue, because the USB subsystem doesn't work in any case. On later
 * CFV4E parts the USB subsystem *does* work. USB presents a problem
 * for any static assignment, because USB is a bus and there are a
 * wide range of possible endpoint latency requirements. Similarly for
 * PCI interrupts.
 *
 * As this list matures and gets rearranged, my best guess is that
 * some of the answers will simply have no good solution, and will
 * tend to receive inflationary assignments to ensure that the
 * worst-case requirement is met. These assignments will be followed
 * by schedule downgrades at user level for lower-priority devices.
 *
 * In the following table, the value -1 is used to indicate slots that
 * are either unused or not programmable. This works because bit 7 is
 * reserved and must be zero.
 *
 * The main residual confusions below concern PCI vs. ethernet. Level
 * 3 is presently unused.
 */

/* Local Variables: */
/* comment-column:24 */
/* End: */
#define LP(lvl, prio) ((((uint8_t)(lvl)) << 3) | ((uint8_t)(prio)))

static const uint8_t IntPrio[64] = { 
   -1,			/*  0: Spurious interrupt vector */
   -1,			/*  1: Edge port 1 (NOT PROGRAMMABLE) */
   -1,			/*  2: Edge port 2 (NOT PROGRAMMABLE) */
   -1,			/*  3: Edge port 3 (NOT PROGRAMMABLE) */
   -1,			/*  4: Edge port 4 (NOT PROGRAMMABLE) */
   -1,			/*  5: Edge port 5 (NOT PROGRAMMABLE) */
   -1,			/*  6: Edge port 6 (NOT PROGRAMMABLE) */
   -1,			/*  7: Edge port 7 (NOT PROGRAMMABLE) */

   -1,			/*  8: NOT USED ON M5485 */
   -1,			/*  9: NOT USED ON M5485 */
   -1,			/* 10: NOT USED ON M5485 */
   -1,			/* 11: NOT USED ON M5485 */
   -1,			/* 12: NOT USED ON M5485 */
   -1,			/* 13: NOT USED ON M5485 */
   -1,			/* 14: NOT USED ON M5485 */

   /* USB SUBSYSTEM. This subsystem is not functional on M5485 and is
      therefore assigned lowers level. This assignment will need
      review for later parts. */
   LP(0, 0),		/* 15: USB Endpoint 0 */
   LP(0, 1),		/* 16: USB Endpoint 1 */
   LP(0, 2),		/* 17: USB Endpoint 2 */
   LP(0, 3),		/* 18: USB Endpoint 3 */
   LP(0, 4),		/* 19: USB Endpoint 4 */
   LP(0, 5),		/* 20: USB Endpoint 5 */
   LP(0, 6),		/* 21: USB Endpoint 6 */
   LP(0, 7),		/* 22: USB 2.0 General Interrupt */
   LP(1, 0),		/* 23: USB 2.0 Core Interrupt */
   LP(1, 1),		/* 24: OR of all USB Interrupts */

   LP(1, 2),		/* 25: DSPI overflow or underflow */
   LP(1, 3),		/* 26: Receive FIFO overflow interrupt */
   LP(1, 4),		/* 27: Receive FIFO drain interrupt */
   LP(1, 5),		/* 28: Transmit FIFO underflow interrupt */
   LP(1, 6),		/* 29: Transfer complete interrupt */
   LP(1, 7),		/* 30: Transfer FIFO fill interrupt */
   LP(2, 0),		/* 31: End of queue interrupt */

   LP(2, 1),		/* 32: Peripheral Serial Controller 3 */
   LP(1, 2),		/* 33: Peripheral Serial Controller 2 */
   LP(2, 3),		/* 34: Peripheral Serial Controller 1 */
   LP(2, 4),		/* 35: Peripheral Serial Controller 0 */
   LP(2, 5),		/* 36: Combined interrupts from COMM timer */

   LP(4,7),		/* 37: SEC interrupt */
   LP(5,0),		/* 38: Ethernet 1 interrupt */
   LP(5,1),		/* 39: Ethernet 0 interrupt */
   LP(2,6),		/* 40: I2C interrupt */
   LP(4,6),		/* 41: PCI arbiter interrupt */
   LP(4,5),		/* 42: Comm bus PCI interrupt */
   LP(4,4),		/* 43: XLB PCI Interrupt */

   -1,			/* 44: NOT USED ON M5485 */
   -1,			/* 45: NOT USED ON M5485 */
   -1,			/* 46: NOT USED ON M5485 */

   LP(4,3),		/* 47: XLBARB to CPU interrupt */
   LP(4,2),		/* 48: Multichannel DMA interrupt */
   LP(5,7),		/* 49: CAN0 FlexCan error interrupt */
   LP(5,5),		/* 50: CAN0 FlexCan bus off interrupt */
   LP(5,3),		/* 51: CAN0 Message buffer ORed interrupt */

   -1,			/* 52: NOT USED ON M5485 */

   LP(6,6),		/* 53: Slice Timer 1 */
   LP(6,7),		/* 54: Slice Timer 0 */

   LP(5,6),		/* 55: CAN1 FlexCan error interrupt */
   LP(5,4),		/* 56: CAN1 FlexCan bus off interrupt */
   LP(5,2),		/* 57: CAN1 Message buffer ORed interrupt */

   -1,			/* 58: NOT USED ON M5485 */

   LP(6,2),		/* 59: General Purpose Timer 3 */
   LP(6,3),		/* 60: General Purpose Timer 2 */
   LP(6,4),		/* 61: General Purpose Timer 1 */
   LP(6,5),		/* 62: General Purpose Timer 0 */

   -1			/* 63: NOT USED ON M5485 */
};

static void
IntCtrlr_setup(VectorInfo *vi)
{
  /* Already set up at initialization time. */
}

static void
IntCtrlr_unmask(VectorInfo *vi)
{
  irq_t irq = vi->irq;

  flags_t flags = locally_disable_interrupts();

  uint32_t bit = 1u << (irq % 32);

  if (irq >= 32)
    *MCF_ICU_IMRH &= ~bit;
  else 
    *MCF_ICU_IMRL &= ~bit;

  locally_enable_interrupts(flags);
}

static void
IntCtrlr_mask(VectorInfo *vi)
{
  irq_t irq = vi->irq;

  flags_t flags = locally_disable_interrupts();

  uint32_t bit = 1u << (irq % 32);

  if (irq >= 32)
    *MCF_ICU_IMRH |= bit;
  else 
    *MCF_ICU_IMRL |= bit;

  locally_enable_interrupts(flags);
}

static bool
IntCtrlr_isPending(VectorInfo *vi)
{
  irq_t irq = vi->irq;

  bool isPending = false;
  flags_t flags = locally_disable_interrupts();

  uint32_t bit = 0;

  if (irq >= 32) {
    bit = *MCF_ICU_IPRH;
    irq -= 32;
  }
  else 
    bit = *MCF_ICU_IPRL;

  if (bit & (1u << irq))
    isPending = true;

  locally_enable_interrupts(flags);

  return isPending;
}

static void
IntCtrlr_acknowledge(VectorInfo *vi)
{
  //  IntCtrlr_eoi();
}

static IrqController IntCtrlr = {
  .baseIRQ = 0,
  .nIRQ = 16,
  .va = 0,
  .setup = IntCtrlr_setup,
  .unmask = IntCtrlr_unmask,
  .mask = IntCtrlr_mask,
  .isPending = IntCtrlr_isPending,
  .ack = IntCtrlr_acknowledge,
};

#if 0
void
IntCtrlr_spurious_interrupt()
{
  fatal("Spurious lapic interrupt!\n");
}

void 
IntCtrlr_dump()
{
  printf("LAPIC TIMER 0x%08x   LAPIC ERROR 0x%08x\n"
	 "LAPIC LINT0 0x%08x   LAPIC LINT1 0x%08x\n"
	 "LAPIC PCINT 0x%08x   LAPIC LDR   0x%08x\n"
	 "LAPIC DFR   0x%08x   LAPIC ESR   0x%08x\n"
	 "LAPIC ICR0  0x%08x   LAPIC ICR1  0x%08x\n"
	 "LAPIC SVR   0x%08x\n",
	 IntCtrlr_read_register(INTCTRLR_LVT_Timer),
	 IntCtrlr_read_register(INTCTRLR_LVT_ERROR),
	 IntCtrlr_read_register(INTCTRLR_LVT_LINT0),
	 IntCtrlr_read_register(INTCTRLR_LVT_LINT1),
	 IntCtrlr_read_register(INTCTRLR_LVT_PCINT),
	 IntCtrlr_read_register(INTCTRLR_LDR),
	 IntCtrlr_read_register(INTCTRLR_DFR),
	 IntCtrlr_read_register(INTCTRLR_ESR),
	 IntCtrlr_read_register(INTCTRLR_ICR0),
	 IntCtrlr_read_register(INTCTRLR_ICR32),
	 IntCtrlr_read_register(INTCTRLR_SVR));

  printf("ISR=0x");
  for (size_t i = 0; i < 0x80; i+= 0x10)
    printf("%08x", IntCtrlr_read_register(INTCTRLR_ISR+i));
  printf("\n");
  printf("IRR=0x");
  for (size_t i = 0; i < 0x80; i+= 0x10)
    printf("%08x", IntCtrlr_read_register(INTCTRLR_IRR+i));
  printf("\n");
  printf("TMR=0x");
  for (size_t i = 0; i < 0x80; i+= 0x10)
    printf("%08x", IntCtrlr_read_register(INTCTRLR_TMR+i));
  printf("\n");

}
#endif

/** @brief Initialize the interrupt controller.
 */
void
IntCtrlr_init()
{
#if 0
  if (MY_CPU(id) == 0) {
    {
      VectorInfo *vector = irq_MapInterrupt(irq_INTCTRLR_Timer);
      vector->type = vt_Interrupt;
      vector->mode = VEC_MODE_FROMBUS;
      vector->level = VEC_LEVEL_FROMBUS;
      vector->irq = irq_INTCTRLR_Timer;
      vector->fn = vh_UnboundIRQ;
      vector->status = vec_st_uninit;
      vector->ctrlr = &lapic;
    }

    {
      VectorInfo *vector = irq_MapInterrupt(irq_INTCTRLR_IPI);
      vector->type = vt_Interrupt;
      vector->mode = VEC_MODE_EDGE;
      vector->level = VEC_LEVEL_ACTLOW; /* ?? */
      vector->irq = irq_INTCTRLR_IPI;
      vector->fn = vh_UnboundIRQ;
      vector->status = vec_st_uninit;
      vector->ctrlr = &lapic;
    }

    {
      VectorInfo *vector = irq_MapInterrupt(irq_INTCTRLR_SVR);
      vector->type = vt_Interrupt;
      vector->mode = VEC_MODE_FROMBUS;
      vector->level = VEC_LEVEL_FROMBUS; /* ??? */
      vector->irq = irq_INTCTRLR_SVR;
      vector->fn = vh_UnboundIRQ;
      vector->status = vec_st_uninit;
      vector->ctrlr = &lapic;
    }

    irq_Bind(irq_INTCTRLR_SVR, VEC_MODE_EDGE, VEC_LEVEL_ACTHIGH, 
	     IntCtrlr_spurious_interrupt);

    VectorInfo *vector = irq_MapInterrupt(irq_INTCTRLR_SVR);
    vector->status = vec_st_live;
    vector->ctrlr->unmask(vector);
  }
  else {
    VectorInfo *vector = irq_MapInterrupt(irq_INTCTRLR_SVR);
    vector->ctrlr->unmask(vector);
  }
#endif
  //   IntCtrlr_eoi();
}

void
irq_init()
{
  /* The M5485 supports 63 interrupt "lines" internally, which arrive
   * on vectors 65..128, where the rule is "vector=irq+64". It is not
   * (presently) clear to me how these entries interact with entries
   * 24..31.
   */

  /* Mask out everything at the controller. */
  *MCF_ICU_IMRL = 0x1u;	/* mask all bit */

  /* Initialize all hardware table entries. */
  for (uint32_t irq = 0; irq < 64; irq++) {
    VectorInfo *vector = &VectorMap[TARGET_HAL_NUM_TRAP + irq];
    vector->type = vt_Interrupt;
    vector->mode = VEC_MODE_FROMBUS;
    vector->level = VEC_LEVEL_FROMBUS;
    vector->irq = irq;
    vector->fn = vh_UnboundIRQ;
    vector->status = vec_st_uninit;
    vector->ctrlr = &IntCtrlr;

    /* Assign the appropriate hardware layer interrupt priority. */
    MCF_ICU_ICR0[irq] = IntPrio[irq];
  }

  /* It does not appear to be possible to reset the controller. */
}
