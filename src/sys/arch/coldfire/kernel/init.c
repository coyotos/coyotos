/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Startup initialization for Coldfire.
 */

#include <hal/config.h>
#include <hal/kerntypes.h>
#include <hal/console.h>
#include <hal/transmap.h>
#include <hal/vm.h>
#include <kerninc/CPU.h>
#include <kerninc/Cache.h>
#include <kerninc/DevRange.h>
#include <kerninc/util.h>
#include <kerninc/PhysMem.h>
#include <kerninc/malloc.h>
#include <kerninc/printf.h>
#include <kerninc/vector.h>

#include "kva.h"
#include "hwmap.h"
#include "IRQ.h"

extern uint32_t _start[];
extern uint32_t _etext[];
extern uint32_t __syscallpg[];
extern uint32_t __esyscallpg[];
extern uint32_t __begin_maps[];
extern uint32_t __end_maps[];
extern uint32_t cpu0_kstack_lo[];
extern uint32_t cpu0_kstack_hi[];
extern uint32_t _data[];
extern uint32_t _end[];
extern uint32_t _pagedata[];
extern uint32_t _epagedata[];
extern uint32_t _rodata[];
extern uint32_t _erodata[];
extern uint32_t bsp_ExceptionVector[];

extern void bsp_init();
extern void bsp_mem_config();
extern void bsp_protect_modules();
extern void hardclock_init();

static void 
setup_heap()
{
  /** @bug This needs to be modified to use large page mappings. */

  size_t heap_size = cache_compute_required_heap();
  heap_size += devrange_compute_required_heap();
  kva_t heap_base = align_up((kva_t)&_end, alignof(uintptr_t));
  kva_t heap_bound = heap_base + heap_size;
  heap_bound = align_up(heap_bound, COYOTOS_PAGE_SIZE);

  /* Heap is already backed (and mapped) to end of last page
     containing BSS. */
  kpa_t heap_backed_va = align_up(heap_base, COYOTOS_PAGE_SIZE);
  if (heap_backed_va > heap_base)
    pmem_AllocRegion(KVTOP(_end), KVTOP(heap_backed_va), pmc_RAM, 
		     pmu_KHEAP, "Kernel heap");

  assert(((heap_bound - heap_backed_va) % COYOTOS_PAGE_SIZE) == 0);

  /* For the moment, simply grab the heap as a single, contiguous
   * region. Since the heap works out to about 25% of physical
   * memory, this ought to succeed for all machine configurations
   * observed in the wild.
   *
   * @bug This really ought to be grabbing 4M units so that we can
   * use the large page mapping logic.
   */
  pmem_AllocRegion(KVTOP(heap_backed_va), KVTOP(heap_bound), pmc_RAM, 
		   pmu_KHEAP, "Kernel heap");

  /* We are overwriting some of the pre-initialized low mappings, so
   * set remap to true here.
   */
     
  kmap_map(heap_backed_va, KVTOP(heap_backed_va), 
	   heap_bound - heap_backed_va, KMAP_W, true);
  heap_init(heap_base, heap_bound, heap_bound);
}

static void 
protect_kernel_memory()
{
  pmem_AllocRegion(KVTOP(bsp_ExceptionVector), 
		   KVTOP(bsp_ExceptionVector)+1024, pmc_RAM, pmu_KERNEL,
		   "Exception Vector");

  pmem_AllocRegion(KVTOP(_start), KVTOP(_etext), pmc_RAM, pmu_KERNEL,
		   "Kernel code");
  pmem_AllocRegion(KVTOP(_rodata), KVTOP(_erodata), pmc_RAM, pmu_KERNEL,
		   "Kernel rodata");

  /* We do not need to protect the stack, because it isn't marked
     allocatable by the BSP. */

  if (_pagedata != _epagedata)
    pmem_AllocRegion(KVTOP(_pagedata), KVTOP(_epagedata), pmc_RAM, pmu_KERNEL,
		     "Kernel page data");

  pmem_AllocRegion(KVTOP(_data), KVTOP(_end), pmc_RAM, pmu_KERNEL,
		   "Kernel data/bss");
}

void
arch_init()
{
  /* Flush TLB completely. */
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_CA;
  *MCF_MMU_MMUOR = MCF_MMU_MMUOR_CA|MCF_MMU_MMUOR_ITLB;

  bsp_init();

  /* Initialize the console output first, so that we can get
   * diagnostics while the rest is running.
   */
  console_init();

  /* This is a soft-mapped architecture, so we really ought to set up
   * the interrupt vectors early -- or at least the one for the TLB
   * miss handler. We don't do this, because this is ALSO an
   * architecture where we have a complete V=P map of physical memory
   * (excluding device memory) available to us in kernel space. The
   * transmap implementation takes advantage of that, so we won't need
   * any temporary mappings during bring-up at all.
   */

  /* Initialize the CPU structures, since we need to call things that
   * want to grab mutexes.
   */
  assert(MAX_NCPU == 1);

  for (size_t i = 0; i < MAX_NCPU; i++)
    cpu_construct(i);
  cpu_vec[0].topOfStack = BSP_defs_SRAM1_BASE + BSP_defs_SRAM1_SIZE;

  /* Initialize the transient map, so that later code can access
     arbitrary parts of memory. */
  transmap_init();

  protect_kernel_memory();

  bsp_mem_config();

  size_t totPage = 
    pmem_Available(&pmem_need_pages, COYOTOS_PAGE_SIZE, false);

  bsp_protect_modules();

  cache_estimate_sizes(PAGES_PER_PROCESS, totPage);

  setup_heap();
  printf("Heap is alive.\n");

  pagetable_init();

  /* Initialize the hardware exception vector table. */
  vector_init();

  irq_init();

  hardclock_init();

  /* Enable interrupts on this processor. */
  printf("Enabling interrupts\n");
  locally_enable_interrupts(local_get_flags() & ~MCF_SR_I_MASK);
}
