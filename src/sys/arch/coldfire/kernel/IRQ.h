#ifndef __COLDFIRE_IRQ_H__
#define __COLDFIRE_IRQ_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Hardware vector declarations
 */

#include <stdbool.h>
#include <kerninc/vector.h>

/** @brief How we use the interrupt vectors. */
enum Vectors {
  /* IPL SP at slot 0 */
  /* IPL PC at slot 1 */
  vec_AccessError	  = 2,	/* page fault */
  vec_AddressError	  = 3,	/* misaligned reference */
  vec_IllegalInstr	  = 4,
  vec_DivZero		  = 5,
  /* Vectors 6,7 reserved */
  vec_PrivViolation	  = 8,
  vec_Trace		  = 9,	/* single step complete */
  vec_BadOpcodeLineA      = 10,
  vec_BadOpcodeLineF      = 11,
  vec_NonPCBreakpoint     = 12,
  vec_PCBreakpoint        = 13,
  vec_FormatError         = 14,
  vec_UninitializedIntr   = 15,
  /* Vectors 16-23 reserved */
  vec_SpuriousIntr        = 24,

  /* Level 1-7 autovectored interrupts */
  vec_IRQ0                = 25,

  /* Trap #0-#15 */
  vec_Trap0               = 32,

  vec_BptInstr            = 47,
  vec_FPBranchUnordered   = 48,
  vec_FPInexactResult     = 49,
  vec_FPDivideByZero      = 50,
  vec_FPUnderflow         = 51,
  vec_FPOperandError      = 52,
  vec_FPOverflow          = 53,
  vec_FPInputNotANumber   = 54,
  vec_FPInputDenormalized = 55,
  /* Vectors 56-60 reserved */
  vec_UnsupportedInstr    = 61,
  /* Vectors 62-63 reserved */
  /* Vectors [64,255] are user defined */
  vec_User0               = 64
};

enum Interrupts {
  irq_SLT0 = IRQ(IBUS_GLOBAL, 54),
};

#if 0
/* Hardware interrupt busses and pins known to the kernel. */
enum Interrupts {
  /* CMOS Interval Timer */
  irq_ISA_PIT        = IRQ(IBUS_ISA,0),
  /* Keyboard */
  irq_ISA_Keyboard   = IRQ(IBUS_ISA,1),
  /* master 8259 cascade from secondary */
  irq_ISA_Cascade    = IRQ(IBUS_ISA,2),

  irq_LAPIC_IPI      = IRQ(IBUS_LAPIC,0xf8),
  irq_LAPIC_Timer    = IRQ(IBUS_LAPIC,0xfe),
  /* Note that SVR interrupt vector is required to have the least four
   * bits set to 1, and therefore must end in 0xf. See Intel
   * documentation of LAPIC SVR register. */
  irq_LAPIC_SVR      = IRQ(IBUS_LAPIC,0xff),
};

/** @brief Number of global interrupt sources */
extern irq_t nGlobalIRQ;

#endif

/** @brief Enable interrupt handling on current CPU.
 *
 * When called for the first time, also initializes the interrupt
 * handling tables.
 */
void irq_init(void);

// #define IRQ_NO_VECTOR 0
// #define VECTOR_NO_IRQ 255

/** @brief Dispatcher for an interrupt or exception that diverted us
 * from userland.
 *
 * Note that this function will not always return, because in many cases we
 * will have yielded, and in that case we will be returning to another
 * process. The function will return if:
 *
 * - This was a kernel interrupt.
 * - This was a user interrupt that did NOT cause a preemption, yield,
 *   or context switch. Check the interrupt.S code that follows
 *   the user-mode call to OnTrapOrInterrupt in this case, because it
 *   is doing a fairly sleazy flow-through.
 *
 */
void irq_OnTrapOrInterrupt(Process *inProc, fixregs_t *saveArea);

void irq_DoTripleFault() NORETURN;

/* For debugging the interrupt handler bring-up: */
#ifdef BRING_UP
void irq_set_softled(VectorInfo *vi, bool on);
#endif

#endif /* __COLDFIRE_IRQ_H__ */
