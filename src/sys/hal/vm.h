#ifndef HAL_VM_H
#define HAL_VM_H
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stddef.h>
#include <hal/kerntypes.h>
#include <target-hal/vm.h>

/** @file
 * @brief Functions to manipulate the master kernel map.
 */

/* *****************************************************************
 *
 * INTERFACES FOR KERNEL MAP MANAGEMENT
 *
 * *****************************************************************/

#define KMAP_R  0x1u
#define KMAP_W  0x2u
#define KMAP_X  0x4u
#define KMAP_NC 0x8u		/* non-cacheable */

/** @brief Map a contiguous region of size @p sz starting at physical
 * address @p pa to a contiguous virtual region starting at virtual
 * address @p va, with permissions @p perms, remapping if @p remap is
 * true.
 *
 * The size argument @p sz must be some multiple of the fundamental
 * page size, and both parameters must be aliged at no worse than page
 * boundaries. If the underlying hardware implements large pages, and
 * the requested mapping is capable of using them, do so.
 *
 * If @p remap is true, this will replace any pre-existing mappings.
 *
 * If the supplied permissions @p perms is zero, the net effect will
 * be to ensure that all necessary mapping entries (PTEs) are present,
 * but all of the resulting leaf PTEs will have their valid bits
 * clear. This can also be used to unmap previous mappings. If the
 * goal is to allocate page tables, however, care must be taken to
 * ensure that the subsequent call passes the same va/pa pair. This is
 * necessary because the implementation exploits alignment properties
 * to use large page mappings where possible, so a subsequent call
 * that does not honor the conditions of the first call may generate
 * allocations of page tables.
 *
 * THIS FUNCTION IS NOT SMP-SAFE. The only code that should call this
 * after multiprocessing is enabled is code that is doing CPU-local
 * mapping window management, and therefore knows that the updates are
 * happening to CPU-local page directories and page tables.
 */
void kmap_map(kva_t va, kpa_t pa, size_t sz, uint32_t perms, bool remap);

/** @brief Opaque PTE type definition. 
 *
 * Target-specific HAL must define TARGET_HAL_PTE_T. Machine
 * independent code may declare and use pointers to this type, but the
 * type itself (including its size) is fully opaque.
 */
typedef TARGET_HAL_PTE_T pte_t;

void pte_invalidate(pte_t *);

struct Mapping;

/** @brief Switch address space register of the current CPU to the
 * provided PA. */
void vm_switch_curcpu_to_map(struct Mapping *map);

/** @brief Returns TRUE iff the specified VA is a valid user-mode VA
 * for the referencing process according to the address map of the current
 * architecture.
 */
struct Process;
static inline bool vm_valid_uva(struct Process *p, kva_t uva);

/** @brief Flush the TLB on all processors */
void global_tlb_flush(asid_t asid);

/** @brief Flush the TLB of all user-mode mappings matching @p asid.
 *
 * On some architectures, this will also (regrettably) flush kernel
 * mappings.
 */
void local_tlb_flush(asid_t asid);

/** @brief Ensure that a particular (@p asid, @p va) pair is no longer
 * in the TLB.
 *
 * On some targets this is implemented by flushing the entire TLB.
 *
 * Most architectures having an ASID register provide some means to
 * flush all entries associated with a given ASID from the TLB. On
 * such architectures, a distinguished ASID value is reserved for use
 * by the transmap, which installs supervisor-only shared-global
 * mappings using that ASID as a tag.
 *
 * On architectures where kernel mappings must be loaded into the TLB
 * explicitly, a second ASID @em may by reserved for non-transient
 * kernel mappings.
 */
void local_tlb_flushva(asid_t asid, kva_t va);

#endif /* HAL_VM_KMAP_H */
