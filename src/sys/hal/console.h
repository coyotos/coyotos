#ifndef HAL_CONSOLE_H
#define HAL_CONSOLE_H
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stddef.h>

/** @file
 * @brief Console output mechanism.
 *
 * These functions are generally implemented by BSP-specific code.
 */

/** @brief Initialize the BSP console logic. */
extern void console_init();

/** @brief Append a character to the system console, if one exists */
extern void console_puts(char *msg, size_t nBytes);

/** @brief Called in cache_load_module() if a loaded module claims to
 * provide a user-mode console driver.
 */
extern void console_haveUserConsole();

/** @brief Hand off console responsibility to user driver, if
 * present.
 *
 * Advises the console subsystem that we are about to start the
 * first user mode instruction, and that further attempts to put
 * output on the console should be ignored if a user-mode console
 * driver is present.
 */
extern void console_handoff_to_user();

#endif /* HAL_CONSOLE_H */
