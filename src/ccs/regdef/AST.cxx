
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <string>
#include "AST.hxx"


using namespace boost ;
unsigned long long AST::astCount = 0;


AST::~AST()
{
}

AST::AST(const AstType at)
{
  astType = at;

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_TOKEN_TYPE& tok)
{
  astType = at;
  loc = tok.loc;
  s = tok.str;

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc)
{
  astType = at;
  loc = _loc;

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc,
         AST_SMART_PTR<AST> child1)
{
  astType = at;
  loc = _loc;
  addChild(child1);

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc,
         AST_SMART_PTR<AST> child1,
         AST_SMART_PTR<AST> child2)
{
  astType = at;
  loc = _loc;
  addChild(child1);
  addChild(child2);

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc,
         AST_SMART_PTR<AST> child1,
         AST_SMART_PTR<AST> child2,
         AST_SMART_PTR<AST> child3)
{
  astType = at;
  loc = _loc;
  addChild(child1);
  addChild(child2);
  addChild(child3);

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc,
         AST_SMART_PTR<AST> child1,
         AST_SMART_PTR<AST> child2,
         AST_SMART_PTR<AST> child3,
         AST_SMART_PTR<AST> child4)
{
  astType = at;
  loc = _loc;
  addChild(child1);
  addChild(child2);
  addChild(child3);
  addChild(child4);

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

AST::AST(const AstType at, const AST_LOCATION_TYPE& _loc,
         AST_SMART_PTR<AST> child1,
         AST_SMART_PTR<AST> child2,
         AST_SMART_PTR<AST> child3,
         AST_SMART_PTR<AST> child4,
         AST_SMART_PTR<AST> child5)
{
  astType = at;
  loc = _loc;
  addChild(child1);
  addChild(child2);
  addChild(child3);
  addChild(child4);
  addChild(child5);

  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

::std::string
AST::getTokenString()
{
  return s;
}

void
AST::addChild(AST_SMART_PTR<AST> cld)
{
  children.push_back(cld);
}

const char *
AST::tagName(const AstType at)
{
  switch(at) {
  case at_Null:
    return "at_Null";
  case at_AnyGroup:
    return "at_AnyGroup";
  case at_ident:
    return "at_ident";
  case at_intLiteral:
    return "at_intLiteral";
  case at_uoc:
    return "at_uoc";
  case at_unit:
    return "at_unit";
  case at_instances:
    return "at_instances";
  case at_instance:
    return "at_instance";
  case at_oreg:
    return "at_oreg";
  case at_reg:
    return "at_reg";
  case at_describe:
    return "at_describe";
  case at_def:
    return "at_def";
  case at_constdef:
    return "at_constdef";
  case at_addrlist:
    return "at_addrlist";
  case at_bit:
    return "at_bit";
  case at_field:
    return "at_field";
  case at_value:
    return "at_value";
  case at_binop:
    return "at_binop";
  case at_unop:
    return "at_unop";
  case agt_topdef:
    return "agt_topdef";
  case agt_unitdef:
    return "agt_unitdef";
  case agt_anyfield:
    return "agt_anyfield";
  case agt_expr:
    return "agt_expr";
  default:
    return "<unknown>";
  }
}

const char *
AST::astTypeName() const
{
  return tagName(astType);
}

const char *
AST::astName() const
{
  switch(astType) {
  case at_Null:
    return "Null";
  case at_AnyGroup:
    return "AnyGroup";
  case at_ident:
    return "ident";
  case at_intLiteral:
    return "intLiteral";
  case at_uoc:
    return "uoc";
  case at_unit:
    return "unit";
  case at_instances:
    return "instances";
  case at_instance:
    return "instance";
  case at_oreg:
    return "oreg";
  case at_reg:
    return "reg";
  case at_describe:
    return "describe";
  case at_def:
    return "def";
  case at_constdef:
    return "constdef";
  case at_addrlist:
    return "addrlist";
  case at_bit:
    return "bit";
  case at_field:
    return "field";
  case at_value:
    return "value";
  case at_binop:
    return "binop";
  case at_unop:
    return "unop";
  case agt_topdef:
    return "{topdef}";
  case agt_unitdef:
    return "{unitdef}";
  case agt_anyfield:
    return "{anyfield}";
  case agt_expr:
    return "{expr}";
  default:
    return "<unknown>";
  }
}

#define ISSET(v,b) ((v)[((b)/8)] & (1u << ((b)%8)))

void
astChTypeError(const AST &myAst, const AstType exp_at,
               const AstType act_at, size_t child)
{
  ::std::cerr << myAst.loc.asString() << ": " << myAst.astTypeName();
  ::std::cerr << " has incompatible Child# " << child;
  ::std::cerr << ". Expected " << AST::tagName(exp_at) << ", "; 
  ::std::cerr << "Obtained " << AST::tagName(act_at) << "." << ::std::endl;
}

void
astChNumError(const AST &myAst, const size_t exp_ch,
               const size_t act_ch)
{
  ::std::cerr << myAst.loc.asString() << ": " << myAst.astTypeName();
  ::std::cerr << " has wrong number of children. ";
  ::std::cerr << "Expected " << exp_ch << ", ";
  ::std::cerr << "Obtained " << act_ch << "." << ::std::endl;
}

static const unsigned char *astMembers[] = {
  (unsigned char *)"\x01\x00\x00", // at_Null
  (unsigned char *)"\x02\x00\x00", // at_AnyGroup
  (unsigned char *)"\x04\x00\x00", // at_ident
  (unsigned char *)"\x08\x00\x00", // at_intLiteral
  (unsigned char *)"\x10\x00\x00", // at_uoc
  (unsigned char *)"\x20\x00\x00", // at_unit
  (unsigned char *)"\x40\x00\x00", // at_instances
  (unsigned char *)"\x80\x00\x00", // at_instance
  (unsigned char *)"\x00\x01\x00", // at_oreg
  (unsigned char *)"\x00\x02\x00", // at_reg
  (unsigned char *)"\x00\x04\x00", // at_describe
  (unsigned char *)"\x00\x08\x00", // at_def
  (unsigned char *)"\x00\x10\x00", // at_constdef
  (unsigned char *)"\x00\x20\x00", // at_addrlist
  (unsigned char *)"\x00\x40\x00", // at_bit
  (unsigned char *)"\x00\x80\x00", // at_field
  (unsigned char *)"\x00\x00\x01", // at_value
  (unsigned char *)"\x00\x00\x02", // at_binop
  (unsigned char *)"\x00\x00\x04", // at_unop
  (unsigned char *)"\x20\x1e\x08", // agt_topdef
  (unsigned char *)"\x00\x1f\x10", // agt_unitdef
  (unsigned char *)"\x00\xc0\x20", // agt_anyfield
  (unsigned char *)"\x08\x00\x46"  // agt_expr
};

bool
AST::isMemberOfType(AstType ty) const
{
  return ISSET(astMembers[ty], astType) ? true : false;}

bool
AST::isValid() const
{
  size_t c;
  size_t specNdx;
  bool errorsPresent = false;

  for (c = 0; c < children.size(); c++) {
    if (!child(c)->isValid())
      errorsPresent = true;
  }

  c = 0;
  specNdx = 0;

  switch(astType) {
  case at_Null: // leaf AST:
    if(children.size() != 0) {
      astChNumError(*this, 0, children.size());
      errorsPresent = true;
    }
    break;

  case at_AnyGroup: // leaf AST:
    if(children.size() != 0) {
      astChNumError(*this, 0, children.size());
      errorsPresent = true;
    }
    break;

  case at_ident: // leaf AST:
    if(children.size() != 0) {
      astChNumError(*this, 0, children.size());
      errorsPresent = true;
    }
    break;

  case at_intLiteral: // leaf AST:
    if(children.size() != 0) {
      astChNumError(*this, 0, children.size());
      errorsPresent = true;
    }
    break;

  case at_uoc: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_topdef
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_topdef], child(c)->astType)) {
      astChTypeError(*this, agt_topdef, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_unit: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_instances
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_instances], child(c)->astType)) {
      astChTypeError(*this, at_instances, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_unitdef+
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_unitdef], child(c)->astType)) {
      astChTypeError(*this, agt_unitdef, child(c)->astType, 2);
      errorsPresent = true;
    }
    while (c < children.size()) {
      if (!ISSET(astMembers[agt_unitdef], child(c)->astType))
        astChTypeError(*this, agt_unitdef, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_instances: // normal AST:
    // match at_instance*
    while (c < children.size()) {
      if (!ISSET(astMembers[at_instance], child(c)->astType))
        astChTypeError(*this, at_instance, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_instance: // normal AST:
    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_ident?
    if ((c < children.size()) && ISSET(astMembers[at_ident], child(c)->astType))
      c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_oreg: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_instances
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_instances], child(c)->astType)) {
      astChTypeError(*this, at_instances, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_anyfield*
    while (c < children.size()) {
      if (!ISSET(astMembers[agt_anyfield], child(c)->astType))
        astChTypeError(*this, agt_anyfield, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_reg: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_anyfield*
    while (c < children.size()) {
      if (!ISSET(astMembers[agt_anyfield], child(c)->astType))
        astChTypeError(*this, agt_anyfield, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_describe: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_anyfield*
    while (c < children.size()) {
      if (!ISSET(astMembers[agt_anyfield], child(c)->astType))
        astChTypeError(*this, agt_anyfield, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_def: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_constdef: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_addrlist: // normal AST:
    // match agt_expr+
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, 0);
      errorsPresent = true;
    }
    while (c < children.size()) {
      if (!ISSET(astMembers[agt_expr], child(c)->astType))
        astChTypeError(*this, agt_expr, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_bit: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_value*
    while (c < children.size()) {
      if (!ISSET(astMembers[at_value], child(c)->astType))
        astChTypeError(*this, at_value, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_field: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_intLiteral
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_intLiteral], child(c)->astType)) {
      astChTypeError(*this, at_intLiteral, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match at_value*
    while (c < children.size()) {
      if (!ISSET(astMembers[at_value], child(c)->astType))
        astChTypeError(*this, at_value, child(c)->astType, c);
      c++;
    }

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_value: // normal AST:
    // match at_ident
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[at_ident], child(c)->astType)) {
      astChTypeError(*this, at_ident, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_binop: // normal AST:
    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  case at_unop: // normal AST:
    // match agt_expr
    if(c >= children.size()) {
      astChNumError(*this, c+1, children.size());
      errorsPresent = true;
      break;
    }
    if (!ISSET(astMembers[agt_expr], child(c)->astType)) {
      astChTypeError(*this, agt_expr, child(c)->astType, c);
      errorsPresent = true;
    }
    c++;

    if(c != children.size()) {
      astChNumError(*this, c, children.size());
      errorsPresent = true;
    }
    break;

  // group ASTagt_topdef gets default
    break;

  // group ASTagt_unitdef gets default
    break;

  // group ASTagt_anyfield gets default
    break;

  // group ASTagt_expr gets default
    break;

  default:
    errorsPresent = true;
  }

  return !errorsPresent;
}


shared_ptr<AST>  
AST::makeIntLit(const sherpa::LToken &tok) 
{
  boost::shared_ptr<AST> ast = AST::make(at_intLiteral, tok);
  ast->intValue = (NumberType) strtoull(tok.str.c_str(), 0, 0);
  return ast;
}

shared_ptr<AST>
AST::makeOperator(const sherpa::LToken &tok, boost::shared_ptr<AST> arg) 
{
  boost::shared_ptr<AST> ast = AST::make(at_unop, tok.loc, arg);
  ast->opName = tok.str[0];
  return ast;
}

shared_ptr<AST>
AST::makeOperator(const sherpa::LToken &tok, boost::shared_ptr<AST> leftArg,
		  boost::shared_ptr<AST> rightArg) 
{
  boost::shared_ptr<AST> ast = AST::make(at_binop, tok.loc, leftArg, rightArg);
  ast->opName = tok.str[0];
  return ast;
}


