#ifndef INTERP_HXX
#define INTERP_HXX

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <boost/shared_ptr.hpp>

struct AST;

struct InterpState {
  std::ostream &errStream;
  boost::shared_ptr<AST> curAST;

  InterpState(std::ostream & _errStream, 
	      boost::shared_ptr<AST> _curAST);

  InterpState(const InterpState& is);
};

struct CheckState {
  std::ostream &errStream;
  boost::shared_ptr<AST> curAST;
  boost::shared_ptr<AST> instances;

  bool valid;

  uint32_t regWidth;		// of prevailing register
  uint32_t baseBit;		// of prevailing field

  CheckState(std::ostream & _errStream, 
	     boost::shared_ptr<AST> _curAST);

  CheckState(const CheckState& cs);
};

struct EmitState {
  std::ostream &ostream;
  boost::shared_ptr<AST> curAST;
  boost::shared_ptr<AST> instances;

  bool valid;

  std::string fromReg;
  std::string nm;
  std::string pkgName;
  uint32_t regWidth;		// of prevailing register
  uint32_t baseBit;		// of prevailing field

  EmitState(std::ostream & _errStream, 
	    boost::shared_ptr<AST> _curAST);

  EmitState(const EmitState& cs);
};

#endif /* INTERP_HXX */
