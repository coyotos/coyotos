/* -*- mode: c++ -*- */
%copyright {
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
}

%include {
#include <stdint.h>
#include <set>
#include <string>
#include <boost/shared_ptr.hpp>
#include <libsherpa/INOstream.hxx>
#include "Interp.hxx"

}

%members {
 private:  
  static unsigned long long astCount;
  
 public:
  typedef std::set< std::string > DepSet;

  typedef uint64_t NumberType;

  unsigned long long ID; // Unique ID of this AST

  unsigned long flags;

  NumberType   intValue;
  char         opName;
  
  unsigned printVariant;	// which syntax to use for pretty printing

  static boost::shared_ptr<AST> makeIntLit(const sherpa::LToken &tok);
  static boost::shared_ptr<AST> makeOperator(const sherpa::LToken &tok, 
				 boost::shared_ptr<AST> arg);
  static boost::shared_ptr<AST> makeOperator(const sherpa::LToken &tok, 
				 boost::shared_ptr<AST> leftArg, boost::shared_ptr<AST> rightArg);
  
  std::string asString() const;

  void PrettyPrint(sherpa::INOstream& out) const;
  void PrettyPrint(std::ostream& out) const;

  // For use in GDB:
  void PrettyPrint() const;

  NumberType interp(InterpState& is);
  void check(CheckState& cs);
  void depends(const std::string& curPkg, DepSet& deps);
  void emit(EmitState& cs);
} 

%sourcetop {
using namespace boost ;
unsigned long long AST::astCount = 0;
}

%source {
shared_ptr<AST>  
AST::makeIntLit(const sherpa::LToken &tok) 
{
  boost::shared_ptr<AST> ast = AST::make(at_intLiteral, tok);
  ast->intValue = (NumberType) strtoull(tok.str.c_str(), 0, 0);
  return ast;
}

shared_ptr<AST>
AST::makeOperator(const sherpa::LToken &tok, boost::shared_ptr<AST> arg) 
{
  boost::shared_ptr<AST> ast = AST::make(at_unop, tok.loc, arg);
  ast->opName = tok.str[0];
  return ast;
}

shared_ptr<AST>
AST::makeOperator(const sherpa::LToken &tok, boost::shared_ptr<AST> leftArg,
		  boost::shared_ptr<AST> rightArg) 
{
  boost::shared_ptr<AST> ast = AST::make(at_binop, tok.loc, leftArg, rightArg);
  ast->opName = tok.str[0];
  return ast;
}

}

%construct {
  ID = ++(AST::astCount);
  printVariant = 0;		// until otherwise stated
  flags = 0;
}

/* Use LEAF declarations for literals. */

// IDENTIFIERS
LEAF ident;

// INTEGER LITERALS
LEAF intLiteral;

// COMPILATION UNITS
// Note: UOC should match BLOCK.
AST uoc = ident topdef;

GROUP topdef = describe | reg | def | constdef | unit;

// GROUP topdef = unit;
GROUP unitdef =  describe | reg | def | constdef | oreg;

AST unit = ident instances unitdef+;
// Can be empty if it is a placeholder.
AST instances = instance*;
// INSTANCE N DELTA from BASE
// instance N at ADDR
AST instance = intLiteral expr ident?;

// STATEMENTS
AST oreg = ident intLiteral intLiteral instances anyfield*;

AST reg = ident intLiteral anyfield*;

AST describe = ident intLiteral anyfield*;

AST def = ident intLiteral expr;

AST constdef = ident intLiteral expr;

AST addrlist = expr+;

AST bit = ident expr value*;
AST field = ident intLiteral intLiteral value*;

GROUP anyfield = bit | field;

AST value = ident expr;

AST binop = expr expr;
AST unop = expr;

// EXPRESSIONS
GROUP expr = 
  intLiteral
  | binop
  | unop;
