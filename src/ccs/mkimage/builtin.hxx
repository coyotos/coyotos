#ifndef BUILTIN_HXX
#define BUILTIN_HXX

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <boost/shared_ptr.hpp>
#include "Value.hxx"

extern bool
isArgType(boost::shared_ptr<AST> arg, Value::ValKind);

extern void
requireArgType(InterpState& is, const std::string& nm,
	       std::vector<boost::shared_ptr<Value> >& args, size_t ndx, Value::ValKind vk);

extern boost::shared_ptr<CapValue>
needCapArgType(InterpState& is, const std::string& nm,
	       std::vector<boost::shared_ptr<Value> >& args, size_t ndx, CapType ct1, CapType ct2);

inline boost::shared_ptr<CapValue>
needCapArgType(InterpState& is, const std::string& nm,
	       std::vector<boost::shared_ptr<Value> >& args, size_t ndx, CapType ct)
{
  return needCapArgType(is, nm, args, ndx, ct, ct);
}

extern boost::shared_ptr<CapValue>
needAnyCapArg(InterpState& is, const std::string& nm,
	      std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<CapValue>
needBankArg(InterpState& is, const std::string& nm,
	    std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<CapValue>
needSpaceArg(InterpState& is, const std::string& nm,
	     std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<IntValue>
needIntArg(InterpState& is, const std::string& nm,
	   std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<BoolValue>
needBoolArg(InterpState& is, const std::string& nm,
	    std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<StringValue>
needStringArg(InterpState& is, const std::string& nm,
	      std::vector<boost::shared_ptr<Value> >& args, size_t ndx);

extern boost::shared_ptr<Environment<Value> > getBuiltinEnv(boost::shared_ptr<CoyImage>);

extern boost::shared_ptr<Value>
pf_print_one(InterpState& is, boost::shared_ptr<Value> arg);

extern boost::shared_ptr<Value>
pf_doprint(PrimFnValue& pfv,
	   InterpState& is, 
	   std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_doprint_tree(PrimFnValue& pfv,
		InterpState& is, 
		std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_doprint_space(PrimFnValue& pfv,
		 InterpState& is, 
		 std::vector<boost::shared_ptr<Value> >& args);

// This one is not really a legal built-in:
extern boost::shared_ptr<Value>
pf_doprint_star(InterpState& is, 
		std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_arith(PrimFnValue& pfv,
	 InterpState& is, 
	 std::vector<boost::shared_ptr<Value> >& args);

// Capability downgrade operations:
extern boost::shared_ptr<Value>
pf_downgrade(PrimFnValue& pfv,
	     InterpState& is, 
	     std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_insert_subspace(PrimFnValue& pfv,
		   InterpState& is, 
		   std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_readfile(PrimFnValue& pfv,
	    InterpState& is, 
	    std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_loadimage(PrimFnValue& pfv,
	     InterpState& is, 
	     std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_guard(PrimFnValue& pfv,
	 InterpState& is, 
	 std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_guarded_space(PrimFnValue& pfv,
		 InterpState& is, 
		 std::vector<boost::shared_ptr<Value> >& args);

extern boost::shared_ptr<Value>
pf_mk_misccap(PrimFnValue& pfv,
	      InterpState& is, 
	      std::vector<boost::shared_ptr<Value> >& args);

// Fabricate window capability
extern boost::shared_ptr<Value>
pf_mk_window(PrimFnValue& pfv,
	     InterpState& is, 
	     std::vector<boost::shared_ptr<Value> >& args);

// Fabricate process
extern boost::shared_ptr<Value>
pf_mk_process(PrimFnValue& pfv,
	      InterpState& is, 
	      std::vector<boost::shared_ptr<Value> >& args);

// Fabricate endpoint
extern boost::shared_ptr<Value>
pf_mk_endpoint(PrimFnValue& pfv,
	       InterpState& is, 
	       std::vector<boost::shared_ptr<Value> >& args);

// Fabricate gpt
extern boost::shared_ptr<Value>
pf_mk_gpt(PrimFnValue& pfv,
	       InterpState& is, 
	       std::vector<boost::shared_ptr<Value> >& args);

// Fabricate page
extern boost::shared_ptr<Value>
pf_mk_page(PrimFnValue& pfv,
	   InterpState& is, 
	   std::vector<boost::shared_ptr<Value> >& args);

// Fabricate cappage
extern boost::shared_ptr<Value>
pf_mk_cappage(PrimFnValue& pfv,
	      InterpState& is, 
	      std::vector<boost::shared_ptr<Value> >& args);

// Wrap an existing capability:
extern boost::shared_ptr<Value>
pf_wrap(PrimFnValue& pfv,
	InterpState& is, 
	std::vector<boost::shared_ptr<Value> >& args);

// Make a sender capability:
extern boost::shared_ptr<Value>
pf_sendTo(PrimFnValue& pfv,
	  InterpState& is, 
	  std::vector<boost::shared_ptr<Value> >& args);

// Make a sender capability:
extern boost::shared_ptr<Value>
pf_dup(PrimFnValue& pfv,
       InterpState& is, 
       std::vector<boost::shared_ptr<Value> >& args);

#endif /* BUILTIN_HXX */
