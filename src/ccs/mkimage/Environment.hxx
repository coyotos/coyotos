#ifndef ENVIRONMENT_HXX
#define ENVIRONMENT_HXX
/*
 * Copyright (C) 2005, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

class UocInfo;

// Type of (sub) environment, if any.
// Universal, In module scope, or in record scope
//enum envType {universal, mod, rec, none}; 
//enum BindType {bind_type, bind_value};


#define BF_EXPORTED   0x1u  /* Binding is exported */
#define BF_CONSTANT   0x2u  /* Binding is immutable */
#define BF_PURE       0x4u  /* Binding is pure */

template <class T>
struct Binding {
  std::string nm;
  boost::shared_ptr<T> val;
  unsigned flags;

  Binding(const std::string& _nm, boost::shared_ptr<T> _val)
  {
    nm = _nm;
    val = _val;
    flags = 0;
  }

  static inline boost::shared_ptr<Binding>
  make(const std::string& _nm, boost::shared_ptr<T> _val) {
    Binding *b = new Binding(_nm, _val);
    return boost::shared_ptr<Binding>(b);
  }
};

template <class T>
struct Environment :public boost::enable_shared_from_this<Environment<T> > {
  boost::shared_ptr<Environment<T> > parent; // in the chain of environments

  typedef std::vector<boost::shared_ptr<Binding<T> > > BindingVec;
  BindingVec bindings;

  boost::shared_ptr<Binding<T> > 
  getBinding(const std::string& nm) const;

  boost::shared_ptr<Binding<T> > 
  getLocalBinding(const std::string& nm) const;

  Environment(const boost::shared_ptr<Environment>& _parent =
	      boost::shared_ptr<Environment>())
  {
    parent = _parent;
  }

  ~Environment();

  static inline boost::shared_ptr<Environment>
  make(const boost::shared_ptr<Environment>& _parent =
       boost::shared_ptr<Environment>()) {
    Environment *env = new Environment(_parent);
    return boost::shared_ptr<Environment>(env);
  }

  void
  addBinding(const std::string& name, boost::shared_ptr<T> val, 
	     bool rebind = false);

  void
  removeBinding(const std::string& name);

  inline boost::shared_ptr<T>
  getValue(const std::string& nm) const
  {
    const boost::shared_ptr<Binding<T> > binding = getBinding(nm);
    return (binding ? binding->val : boost::shared_ptr<T>());
  }

  inline unsigned
  getFlags(const std::string& nm)
  {
    const boost::shared_ptr<Binding<T> > binding = getBinding(nm);
    return (binding ? binding->flags : 0);
  }

  inline void
  setFlags(const std::string& nm, unsigned long flags)
  {
    boost::shared_ptr<Binding<T> > binding = getBinding(nm);
    if (binding) binding->flags |= flags;
  }

  void
  addConstant(const std::string& name, boost::shared_ptr<T> val)
  {
    addBinding(name, val);
    setFlags(name, BF_CONSTANT);
  }

  void
  addPureConstant(const std::string& name, boost::shared_ptr<T> val)
  {
    addBinding(name, val);
    setFlags(name, BF_CONSTANT|BF_PURE);
  }

  boost::shared_ptr<Environment<T> >newScope();
};
#endif /* ENVIRONMENT_HXX */
