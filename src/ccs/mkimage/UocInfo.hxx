#ifndef UOCINFO_HXX
#define UOCINFO_HXX

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <map>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <libsherpa/LexLoc.hxx>
#include <libcoyimage/CoyImage.hxx>

#include "Environment.hxx"
#include "AST.hxx"
#include "DepVector.hxx"

// Passes consist of transforms on the AST. The parse pass is included in 
// the list for debugging annotation purposes, but is handled as a special
// case.

enum Pass {
#define PASS(variant, nm, short, descrip) pn_##nm,
#include "pass.def"
};

class UocInfo;

struct PassInfo {
  const char *name;
  bool (UocInfo::* fn)(std::ostream& errStream, 
		       DepSet& deps,
		       unsigned long flags);
  const char *descrip;
  bool printAfter;		// for debugging
  bool stopAfter;		// for debugging
  uint32_t variant;
};

// Unit Of Compilation Info. One of these is constructed for each
// unit of compilation (source or interface). In the source case, the
// /name/ field will be the file name. In the interface case, the
// /name/ will be the "ifname" reported in the import statement.
class UocInfo :public boost::enable_shared_from_this<UocInfo> {
  static boost::filesystem::path resolveModulePath(std::string modName);

public:
  std::string uocName;		// either ifName or simulated
  boost::filesystem::path origin;
  unsigned long flags;
  
  bool isCmdLine;		// processing requested from command line

  Pass lastCompletedPass;

  boost::shared_ptr<AST> ast;
  boost::shared_ptr< Environment<Value> > env;
  boost::shared_ptr< Environment<Value> > exportedEnv;

  boost::shared_ptr< CoyImage > ci;
  
  DepSet uocDeps;

  // If fileName argument is an empty string, search path resolution
  // will be attempted.
  UocInfo(const std::string& _uocName, const std::string& fileName,
	  const boost::shared_ptr<CoyImage>& ci);
  UocInfo(const boost::shared_ptr<UocInfo>& uoc);

  static inline boost::shared_ptr<UocInfo>
  make(const std::string& _uocName, const std::string& fileName,
	  const boost::shared_ptr<CoyImage>& ci) {
    UocInfo *tmp = new UocInfo(_uocName, fileName, ci);
    return boost::shared_ptr<UocInfo>(tmp);
  }

  static inline boost::shared_ptr<UocInfo>
  make(const boost::shared_ptr<UocInfo>& uoc) {
    UocInfo *tmp = new UocInfo(uoc);
    return boost::shared_ptr<UocInfo>(tmp);
  }

  static std::vector< boost::filesystem::path > searchPath;

  // Following is implemented in Parser.y
  static std::string extractModuleName(std::ostream&, 
				       const boost::filesystem::path& fName);

  struct ImportRecord {
    std::string nm;
    sherpa::LexLoc loc;

    inline ImportRecord(const std::string& _nm, const sherpa::LexLoc& _loc)
    {
      nm = _nm;
      loc = _loc;
    }
  };
  static std::vector<ImportRecord> importStack;

  struct UocListEntry {
    std::string fileName;
    bool started;
    boost::shared_ptr<UocInfo> uoc;

    UocListEntry()
    {
      started = false;
    }

    UocListEntry(std::string _fileName)
    {
      fileName = _fileName;
      started = false;
    }

    static inline boost::shared_ptr<UocListEntry>
    make() {
      UocListEntry *tmp = new UocListEntry();
      return boost::shared_ptr<UocListEntry>(tmp);
    }

    static inline boost::shared_ptr<UocListEntry>
    make(std::string _fileName) {
      UocListEntry *tmp = new UocListEntry(_fileName);
      return boost::shared_ptr<UocListEntry>(tmp);
    }
  };

  // The presence of a UocInfo record in the uocList indicates that
  // parsing of a module has at least started. If the /ast/ pointer is
  // non-null, the parse has been completed.
  typedef std::map<std::string, 
		   boost::shared_ptr<UocListEntry> > UocList;
  static UocList uocList;

  // If passed filename is empty, search path resolution will be attempted.
  static boost::shared_ptr<UocInfo> 
  importModule(std::ostream&, 
	       const sherpa::LexLoc& loc, 
	       boost::shared_ptr<CoyImage> ci,
	       std::string modName,
	       DepSet& deps,
	       uint32_t compileVariant);

  boost::shared_ptr< Environment<Value> > getExportedEnvironment();

#if 0
  static boost::shared_ptr<UocInfo> 
  compileSource(const std::string& srcName, boost::shared_ptr<CoyImage> ci);
#endif

  // Individual passes:
#define PASS(variant, nm,short,descrip)				  \
  bool pass_##nm(std::ostream& errStream, DepSet& deps, \
		 unsigned long flags=0);
#include "pass.def"

  static PassInfo passInfo[];  

  void Compile(DepSet& deps, uint32_t compileVariant);

  static boost::shared_ptr<AST>
  lookupByFqn(const std::string& s, boost::shared_ptr<UocInfo> &targetUoc);
  
  void PrettyPrint(std::ostream& out);

private:
};

#endif /* UOCINFO_HXX */
