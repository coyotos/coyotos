/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <string>

#include <boost/shared_ptr.hpp>
#include <libsherpa/utf8.hxx>

#include "Environment.hxx"
#include "Value.hxx"
#include "AST.hxx"

using namespace boost;
using namespace sherpa;

// Set field values:
static void 
CapPageSet(PrimFieldValue& pfv,
	   const shared_ptr<Value> & val)
{
  shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  shared_ptr<CoyImage> ci = thisv->ci;

  shared_ptr<CiCapPage> ob = ci->GetCapPage(thisv->cap);

  size_t nSlots = ob->pgSize/sizeof(capability);

  if (pfv.ndx >= nSlots) {
    // Cannot set() an un-dereferenced vector or exceed the bound
    throw excpt::BoundsError;
  }

  try {
    ob->data[pfv.ndx] = dynamic_pointer_cast<CapValue>(val)->cap;
  }
  catch(...) {
    throw excpt::BadValue;
  }

  return;
}

// Get field values:
static shared_ptr<Value> 
CapPageGet(PrimFieldValue& pfv)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  shared_ptr<CoyImage> ci = thisv->ci;

  shared_ptr<CiCapPage> ob = ci->GetCapPage(thisv->cap);

  size_t nSlots = ob->pgSize/sizeof(capability);

  if (pfv.ndx >= nSlots) {
    // Cannot set() an un-dereferenced vector or exceed the bound
    throw excpt::BadValue;
  }

  return CapValue::make(ci, ob->data[pfv.ndx]);
}


// Set field values:
static void 
PageSet(PrimFieldValue& pfv,
	const shared_ptr<Value> & val)
{
  shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  shared_ptr<CoyImage> ci = thisv->ci;

  shared_ptr<CiPage> ob = ci->GetPage(thisv->cap);

  size_t nSlots = ob->pgSize;

  if (pfv.ndx >= nSlots) {
    // Cannot set() an un-dereferenced vector or exceed the bound
    throw excpt::BadValue;
  }

  shared_ptr<IntValue> iv = dynamic_pointer_cast<IntValue>(val);
  if (iv->bn > 255)
    // Exceeds byte value bound.
    throw excpt::BadValue;

  ob->data[pfv.ndx] = iv->bn.as_uint32();
  return;
}

// Get field values:
static shared_ptr<Value> 
PageGet(PrimFieldValue& pfv)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  shared_ptr<CoyImage> ci = thisv->ci;

  shared_ptr<CiPage> ob = ci->GetPage(thisv->cap);

  size_t nSlots = ob->pgSize;

  if (pfv.ndx >= nSlots) {
    // Cannot set() an un-dereferenced vector or exceed the bound
    throw excpt::BadValue;
  }

  return IntValue::make(ob->data[pfv.ndx]);
}


shared_ptr<PrimFieldValue> 
CapValue::derefAsVector(size_t ndx)
{
  if (cap.type == ct_CapPage) {
    shared_ptr<PrimFieldValue> pfv =
      PrimFieldValue::make("<capPage>", false,
			   CapPageSet, 
			   CapPageGet,
			   true);
      
    shared_ptr<Value> sp_this = shared_from_this();
    pfv = dynamic_pointer_cast<PrimFieldValue>(pfv->closeWith(sp_this));
    pfv->ndx = ndx;
    pfv->cacheTheValue();
      
    return pfv;
  }
  else if (cap.type == ct_Page) {
    shared_ptr<PrimFieldValue> pfv =
      PrimFieldValue::make("<page>", false,
			   PageSet, 
			   PageGet,
			   true);
      
    shared_ptr<Value> sp_this = shared_from_this();
    pfv = dynamic_pointer_cast<PrimFieldValue>(pfv->closeWith(sp_this));
    pfv->ndx = ndx;
    pfv->cacheTheValue();
      
    return pfv;
  }
  else
    throw excpt::BadValue;
}

