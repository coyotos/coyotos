/*
 * Copyright (C) 2006, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <stdint.h>
#include <dirent.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include "UocInfo.hxx"
#include "Options.hxx"
#include "Lexer.hxx"

using namespace boost;
using namespace sherpa;

std::vector<filesystem::path > UocInfo::searchPath;
UocInfo::UocList UocInfo::uocList;
std::vector<UocInfo::ImportRecord> UocInfo::importStack;

PassInfo UocInfo::passInfo[] = {
#define PASS(variant, nm, short, descrip) { short, &UocInfo::pass_##nm, descrip, false, false, variant },
#include "pass.def"
};

UocInfo::UocInfo(const std::string& _uocName, const std::string& fileName, 
		 const shared_ptr<CoyImage>& _ci)
{
  lastCompletedPass = pn_none;
  // ast, env auto-init by construction
  ci = _ci;

  uocName = _uocName;

  if (fileName.empty())
    origin = resolveModulePath(uocName).string();
  else {
    origin = fileName;
  }

  if (origin.empty() || !filesystem::exists(origin))
    THROW(excpt::BadValue, 
	  format("File path for module \"%s\" not found", 
		 uocName.c_str()));

  if (!filesystem::is_regular(origin))
    THROW(excpt::BadValue, 
	  format("File path \"%s\" does not name a regular file.\n",
		 origin.string().c_str()));
}

UocInfo::UocInfo(const shared_ptr<UocInfo>& uoc)
{
  uocName = uoc->uocName;
  origin = uoc->origin;
  lastCompletedPass = uoc->lastCompletedPass;
  ast = uoc->ast;
  env = uoc->env;
  ci = uoc->ci;
}

/// @brief Given a module name, find the corresponding source file.
///
/// @bug This should be using the path concatenation code, not just
/// substituting '/'. The current implementation isn't compatible with
/// windows.
filesystem::path
UocInfo::resolveModulePath(std::string modName)
{
  if (modName == "coyotos.TargetInfo")
    modName = modName + "." + Options::targetArchName;

  std::string tmp = modName;
  for (size_t i = 0; i < tmp.size(); i++) {
    if (tmp[i] == '.')
      tmp[i] = '/';
  }

  // Must not use change_extension() here, because modName constains
  // dots 
  for (size_t i = 0; i < UocInfo::searchPath.size(); i++) {
    filesystem::path testPath =
      UocInfo::searchPath[i] / (tmp + ".mki");
    
    if (filesystem::exists(testPath) && 
	filesystem::is_regular(testPath))
      return testPath;
  }

  return filesystem::path();
}

shared_ptr< Environment<Value> > 
UocInfo::getExportedEnvironment()
{
  if (! exportedEnv) {
    exportedEnv = Environment<Value>::make();

    for (size_t i = 0; i < env->bindings.size(); i++) {
      if (env->bindings[i]->flags & BF_EXPORTED)
	exportedEnv->bindings.push_back(env->bindings[i]);
    }
  }

  return exportedEnv;
}

#if 0
shared_ptr<UocInfo> 
UocInfo::compileSource(const std::string& srcFileName,
		       shared_ptr<CoyImage> ci)
{
  shared_ptr<Path> path = new Path(srcFileName);

  shared_ptr<UocInfo> puoci = new UocInfo(srcFileName, path, ci);

  puoci->Compile();

  UocInfo::uocList.append(puoci);
  return puoci;
}
#endif

shared_ptr<UocInfo> 
UocInfo::importModule(std::ostream& errStream,
		      const LexLoc& loc, 
		      shared_ptr<CoyImage> ci,
		      std::string modName,
		      DepSet& deps,
		      uint32_t compileVariant)
{
  UocList::iterator it = uocList.find(modName);
 
  importStack.push_back(ImportRecord(modName, loc));

  if (it == uocList.end()) {
    // Not found
    shared_ptr<UocListEntry> ule = UocListEntry::make();
    uocList[modName] = ule;
    it = uocList.find(modName);
  }
  else {
    if (it->second->uoc)
      return it->second->uoc;

    if (it->second->started) {
      // An in-progress record exists without a unit of compilation
      // pointer. This means that we have a circular import
      // dependency:

      errStream << "Module \"" << modName << "\" has a circular import:\n";
      for (size_t i = 0; i < importStack.size() - 1; i++)
	errStream << "  Module " << importStack[i].nm 
		  << " imports " << importStack[i+1].nm
		  << " at " << importStack[i+1].loc <<	'\n';

      importStack.pop_back();

      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }

  it->second->started = true;
   
  try {
    shared_ptr<UocInfo> puoci = 
      UocInfo::make(modName, it->second->fileName, ci);
    if (!contains(puoci->uocDeps, puoci->origin))
      puoci->uocDeps.insert(puoci->origin);
    if (!contains(deps, puoci->origin))
      deps.insert(puoci->origin);

    puoci->Compile(deps, compileVariant);

    it->second->uoc = puoci;
  } catch ( const sherpa::UExceptionBase& ex ) {
    errStream
      << loc.asString() << ": "
      << "Error importing module \"" << modName << "\": "
      << ex.msg
      << endl;
    throw ex;
  }
  importStack.pop_back();

  return it->second->uoc;
}

bool
UocInfo::pass_none(std::ostream& errStream, 
		   DepSet& deps,
		   unsigned long flags)
{
  return true;
}

bool
UocInfo::pass_npass(std::ostream& errStream,
		    DepSet& deps,
		    unsigned long flags)
{
  return true;
}


bool
UocInfo::pass_parse(std::ostream& errStream,
		    DepSet& deps,
		    unsigned long flags)
{
  // Use binary mode so that newline conversion and character set
  // conversion is not done by the stdio library.
  std::ifstream fin(origin.string().c_str(), std::ios_base::binary);

  if (!fin.is_open()) {
    errStream << "Couldn't open input file \""
	      << origin
	      << "\"\n";
  }

  Lexer lexer(std::cerr, fin, origin.string());

  // This is no longer necessary, because the parser now handles it
  // for all interfaces whose name starts with "bitc.xxx"
  //
  // if (this->flags & UOC_IS_PRELUDE)
  //   lexer.isRuntimeUoc = true;

  lexer.setDebug(Options::showLex);

  extern int mkiparse(Lexer *lexer, shared_ptr<AST> &topAst);
  mkiparse(&lexer, ast);  
  // On exit, ast is a pointer to the AST tree root.
  
  fin.close();

  if (lexer.num_errors != 0u) {
    ast = AST::make(at_Null);
    return false;
  }

  assert(ast->astType == at_uoc);

#if 0
  if (ast->child(0)->astType == at_interface && isSourceUoc) {
    errStream << ast->child(0)->loc.asString()
	      << ": Warning: interface units of compilation should "
	      << "no longer\n"
	      << "    be given on the command line.\n";
  }
#endif

  return true;
}

void
UocInfo::Compile(DepSet& deps, uint32_t compileVariant)
{
  for (size_t i = pn_none+1; (Pass)i <= pn_npass; i++) {
    if ((passInfo[i].variant & compileVariant) == 0)
      continue;
    
    if (Options::showPasses)
      std::cerr << uocName << " PASS " << passInfo[i].name << std::endl;

    //std::cout << "Now performing "
    //	      << passInfo[i].descrip
    //	      << " on " << path->asString()
    //	      << std::endl;

    if (! (this->*passInfo[i].fn)(std::cerr, deps, 0) ) {
      std::cerr << "Exiting due to errors during "
		<< passInfo[i].descrip
		<< std::endl;
      exit(1);
    }

    if(!ast->isValid()) {
      std::cerr << "PANIC: Invalid AST built for file \""
		<< origin
		<< "\". "
		<< "Please report this problem.\n"; 
      ast->PrettyPrint(std::cerr);
      std::cerr << std::endl;
      exit(1);
    }

    if (passInfo[i].printAfter) {
      std::cerr << "==== DUMPING "
		<< uocName
		<< " AFTER " << passInfo[i].name 
		<< " ====" << std::endl;
      this->PrettyPrint(std::cerr);
    }

    if (passInfo[i].stopAfter) {
      std::cerr << "Stopping (on request) after "
		<< passInfo[i].name
		<< std::endl;
      return;
    }

    lastCompletedPass = (Pass) i;
  }
}

void
UocInfo::PrettyPrint(std::ostream& out)
{
  ast->PrettyPrint(out);
  out << std::endl;
}
