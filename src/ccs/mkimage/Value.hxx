#ifndef VALUE_HXX
#define VALUE_HXX

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <inttypes.h>
#include <assert.h>

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <libsherpa/BigNum.hxx>
#include <libsherpa/UExcept.hxx>

#include <obstore/capability.h>
#include <libcoyimage/CoyImage.hxx>
#include "Environment.hxx"
#include "Interp.hxx"

struct AST;

/// @brief Common base class for all values.
///
/// The value notion for mkimage used to be simple. That, of course,
/// required repair.
///
/// The problem that needs to be solved is that some of our ``values''
/// are actually stored in fields of foreign structures. This means
/// that we need a proxy-value notion. Further, the introduction of
/// foreign fields means that we need something like a closure,
/// because the proxy value object needs to retain a pointer (or
/// equivalent) to the foreign object.
///
/// In handling foreign objects, we want to be able to create an
/// ``environment'' for each object type that describes its methods
/// and fields. Rather than generate the environment dynamically when
/// we do "object.field", we would like one statically allocated
/// environment per object type. The problem is that if the
/// environment is statically allocated it's field descriptors cannot
/// already contain the requisite object pointers.
///
/// The solution is to introduce a notion of "open" and "closed"
/// values. An "open" value is one that requires an object pointer but
/// doesn't yet have one. A "closed" value is one that either @em has
/// an object pointer or doesn't require one (e.g. scalars). Within
/// the interpreter, we will check whenever we process a.b. If the
/// value of a.b is an ``open'' value we will make a copy of it, close
/// the copy by inserting the value of a, and return that instead of
/// the open value.
///
/// We then introduce the idea that @em every value (open or closed)
/// has two methods get()->ClosedValue, and set(ClosedValue)->void.
///
/// For scalar values, the get() method simply returns 'this', the
/// set() method raises an exception.
///
/// For foreign fields, the get() method returns the value stored in
/// that field <em>expressed as a mkimage scalar value</em>. The set()
/// method updates the foreign field. Note it is important that the
/// foreign value type @em cache the rvalue, because the call to get()
/// may be deferred and may conceivably cross a set() boundary,
/// resulting in an incorrect answer!
///
/// The reason that we need the apply() method is that methods of
/// foreign objects need to be closed over their object pointer.
///
/// Now the @b only reason that this works is that we @b never allow
/// an OpenValue to escape the dot operation (the "a.b") that looked
/// it up. For example, in the expression "cap.l2v", the internal
/// binding of "lwv" is an OpenValue that accesses a foreign field,
/// but if we write:
///
///           def x = cap.l2v
///
/// the variable 'x' receives a closed value that is a *copy* of
/// the field value. The only place that an OpenValue can be
/// introduced is in the built-in procedure layer. This absolves us of
/// any concern that get() might need to be recursive.

struct Value : public boost::enable_shared_from_this<Value> {
  enum ValKind {
    vk_unit,
    vk_bool,
    vk_char,
    vk_int,
    vk_float,
    vk_string,
    vk_cap,			// capability
    vk_function,		// Top-level define, no closure, AST value
    vk_primfn,			// native-implemented functions
    vk_primMethod,		// native-implemented methods
    vk_primField,		// native-implemented fields
    vk_env,			// named scope -- value is an environment
    vk_boundValue,

  };

  const ValKind kind;
  bool isOpen;

  Value(ValKind k)
    :kind(k)
  {
    isOpen = false;
  }

  virtual boost::shared_ptr<Environment<Value> > getEnvironment()
  {
    return boost::shared_ptr<Environment<Value> >();
  }

  virtual boost::shared_ptr<Value> get()
  {
    // scalars have trivial getters:
    return shared_from_this();
  }

  virtual void set(const boost::shared_ptr<Value>&)
  {
    // scalars do not implement set()
    throw sherpa::excpt::NoAccess;
  }

  // Need something virtual so that RTTI works:
  virtual ~Value();

  static boost::shared_ptr<Value> Unit;
};

struct BoolValue : public Value {
  bool   b;			/* boolean Values */

  BoolValue(const std::string& _value)
    :Value(vk_bool)
  {
    if (_value == "true" || _value == "#t")
      b = true;
    else
      b = false;
  }

  BoolValue(bool _value)
    :Value(vk_bool)
  {
    b = _value;
  }

  static inline boost::shared_ptr<BoolValue> 
  make(const std::string& _value) {
    BoolValue *tmp = new BoolValue(_value);
    return boost::shared_ptr<BoolValue>(tmp);
  }
  static inline boost::shared_ptr<BoolValue> 
  make(bool _value) {
    BoolValue *tmp = new BoolValue(_value);
    return boost::shared_ptr<BoolValue>(tmp);
  }

  static boost::shared_ptr<BoolValue> True;
  static boost::shared_ptr<BoolValue> False;
} ;

struct CharValue : public Value {
private:
  static uint32_t DecodeRawCharacter(const char *s, const char **next);
  static uint32_t DecodeCharacter(const std::string&);

public:
  unsigned long c;		/* utf32 code points */

  CharValue(const std::string& _value)
    :Value(vk_char)
  {
    c = DecodeCharacter(_value);
  }

  CharValue(uint32_t _value)
    :Value(vk_char)
  {
    c = _value;
  }

  static inline boost::shared_ptr<CharValue> 
  make(const std::string& _value) {
    CharValue *tmp = new CharValue(_value);
    return boost::shared_ptr<CharValue>(tmp);
  }
  static inline boost::shared_ptr<CharValue> 
  make(const uint32_t _value) {
    CharValue *tmp = new CharValue(_value);
    return boost::shared_ptr<CharValue>(tmp);
  }
};

struct IntValue : public Value {
  sherpa::BigNum  bn;		// large precision integers

  unsigned long base;		// base as originally expressed

  IntValue(const std::string& _value);
  IntValue(const sherpa::BigNum& _value)
    :Value(vk_int)
  {
    bn = _value;
    base = 0;
  }

  static inline boost::shared_ptr<IntValue> 
  make(const std::string& _value) {
    IntValue *tmp = new IntValue(_value);
    return boost::shared_ptr<IntValue>(tmp);
  }
  static inline boost::shared_ptr<IntValue> 
  make(const sherpa::BigNum& _value) {
    IntValue *tmp = new IntValue(_value);
    return boost::shared_ptr<IntValue>(tmp);
  }
};

struct FloatValue : public Value {
  double d;

  unsigned long base;		// base as originally expressed

  FloatValue(const std::string& _value);
  FloatValue(double _value)
    :Value(vk_float)
  {
    d = _value;
    base = 0;
  }

  static inline boost::shared_ptr<FloatValue> 
  make(const std::string& _value) {
    FloatValue *tmp = new FloatValue(_value);
    return boost::shared_ptr<FloatValue>(tmp);
  }
  static inline boost::shared_ptr<FloatValue> 
  make(const double _value) {
    FloatValue *tmp = new FloatValue(_value);
    return boost::shared_ptr<FloatValue>(tmp);
  }
};

struct StringValue : public Value {
  // Helper for AST
  static uint32_t DecodeStringCharacter(const char *s, const char **next);

public:
  std::string s;  /* String Literals          */

  StringValue(const std::string& _value)
    :Value(vk_string)
  {
    s = _value;
  }

  static inline boost::shared_ptr<StringValue> 
  make(const std::string& _value) {
    StringValue *tmp = new StringValue(_value);
    return boost::shared_ptr<StringValue>(tmp);
  }
};

#if 0
struct RefValue : public Value {
  boost::shared_ptr<Value> v;			/* Indirect values          */

  RefValue(boost::shared_ptr<Value> _value)
    :Value(vk_ref)
  {
    v = _value;
  }
};
#endif

struct FnValue : public Value {
  std::string nm;
  boost::shared_ptr<Environment<Value> > closedEnv;
  boost::shared_ptr<AST> args;
  boost::shared_ptr<AST> body;

  FnValue(const std::string& id, 
	  const boost::shared_ptr<Environment<Value> >& _closedEnv,
	  const boost::shared_ptr<AST>& _args, 
	  const boost::shared_ptr<AST>& _body);

  static inline boost::shared_ptr<FnValue> 
  make(const std::string& id,
       const boost::shared_ptr<Environment<Value> >& _closedEnv,
       const boost::shared_ptr<AST>& _args, 
       const boost::shared_ptr<AST>& _body) {
    FnValue *tmp = new FnValue(id, _closedEnv, _args, _body);
    return boost::shared_ptr<FnValue>(tmp);
  }
};

struct PrimFnValue : public Value {
  std::string nm;		// purely for diagnostics
  const size_t minArgs;
  const size_t maxArgs;		/* 0 == any number */
  boost::shared_ptr<Value> (*fn)(PrimFnValue&,
				 InterpState&,
				 std::vector<boost::shared_ptr<Value> >&); // function to call

  PrimFnValue(const std::string& id, size_t _nArgs,
	      boost::shared_ptr<Value> 
	      (*_fn)(PrimFnValue&,
		     InterpState&,
		     std::vector<boost::shared_ptr<Value> >&))
    :Value(vk_primfn),
     minArgs(_nArgs), maxArgs(_nArgs)
  {
    nm = id;			// purely for diagnostics
    fn = _fn;
  }

  PrimFnValue(const std::string& id, size_t _minArgs, size_t _maxArgs,
	      boost::shared_ptr<Value> 
	      (*_fn)(PrimFnValue&,
		     InterpState&,
		     std::vector<boost::shared_ptr<Value> >&))
    :Value(vk_primfn),
     minArgs(_minArgs), maxArgs(_maxArgs)
  {
    nm = id;			// purely for diagnostics
    fn = _fn;
  }

  static inline boost::shared_ptr<PrimFnValue> 
  make(const std::string& id, size_t _nArgs,
       boost::shared_ptr<Value> 
       (*_fn)(PrimFnValue&,
	      InterpState&,
	      std::vector<boost::shared_ptr<Value> >&)) {
    PrimFnValue *tmp = new PrimFnValue(id, _nArgs, _fn);
    return boost::shared_ptr<PrimFnValue>(tmp);
  }

  static inline boost::shared_ptr<PrimFnValue> 
  make(const std::string& id, size_t _minArgs, size_t _maxArgs,
       boost::shared_ptr<Value> 
       (*_fn)(PrimFnValue&,
	      InterpState&,
	      std::vector<boost::shared_ptr<Value> >&)) {
    PrimFnValue *tmp = new PrimFnValue(id, _minArgs, _maxArgs,_fn);
    return boost::shared_ptr<PrimFnValue>(tmp);
  }
};

struct OpenValue : public Value {
  boost::shared_ptr<Value> thisValue;

  OpenValue(ValKind k)
    :Value(k)
  {
    isOpen = true;
  }

  virtual boost::shared_ptr<OpenValue> dup() = 0;

  virtual boost::shared_ptr<Value> 
  closeWith(boost::shared_ptr<Value> value)
  {
    boost::shared_ptr<OpenValue> vp = dup();
    vp->thisValue = value;
    vp->isOpen = false;

    return vp;
  }
};

struct PrimMethodValue : public OpenValue {
  std::string nm;		// purely for diagnostics
  const size_t minArgs;
  const size_t maxArgs;		/* 0 == any number */
  boost::shared_ptr<Value> (*fn)(PrimMethodValue&,
			     InterpState&,
			     std::vector<boost::shared_ptr<Value> >&); // function to call

  PrimMethodValue(const std::string& id, size_t _nArgs,
		  boost::shared_ptr<Value> 
		  (*_fn)(PrimMethodValue&,
			 InterpState&,
			 std::vector<boost::shared_ptr<Value> >&))
    :OpenValue(vk_primMethod),
     minArgs(_nArgs), maxArgs(_nArgs)
  {
    nm = id;			// purely for diagnostics
    fn = _fn;
  }

  PrimMethodValue(const std::string& id, size_t _minArgs, size_t _maxArgs,
		  boost::shared_ptr<Value> 
		  (*_fn)(PrimMethodValue&,
			 InterpState&,
			 std::vector<boost::shared_ptr<Value> >&))
    :OpenValue(vk_primMethod),
     minArgs(_minArgs), maxArgs(_maxArgs)
  {
    nm = id;			// purely for diagnostics
    fn = _fn;
  }

  PrimMethodValue(const PrimMethodValue& pmv)
    : OpenValue(pmv.kind),
      minArgs(pmv.minArgs), maxArgs(pmv.maxArgs)
  {
    nm = pmv.nm;
    fn = pmv.fn;
  }

  static inline boost::shared_ptr<PrimMethodValue> 
  make(const std::string& id, size_t _nArgs,
       boost::shared_ptr<Value> 
       (*_fn)(PrimMethodValue&,
	      InterpState&,
	      std::vector<boost::shared_ptr<Value> >&)) {
    PrimMethodValue *tmp = new PrimMethodValue(id, _nArgs, _fn);
    return boost::shared_ptr<PrimMethodValue>(tmp);
  }

  static inline boost::shared_ptr<PrimMethodValue> 
  make(const std::string& id, size_t _minArgs, size_t _maxArgs,
       boost::shared_ptr<Value> 
       (*_fn)(PrimMethodValue&,
	      InterpState&,
	      std::vector<boost::shared_ptr<Value> >&)) {
    PrimMethodValue *tmp = new PrimMethodValue(id, _minArgs, _maxArgs, _fn);
    return boost::shared_ptr<PrimMethodValue>(tmp);
  }

  static inline boost::shared_ptr<PrimMethodValue> 
  make(const PrimMethodValue& pmv) {
    PrimMethodValue *tmp = new PrimMethodValue(pmv);
    return boost::shared_ptr<PrimMethodValue>(tmp);
  }


  boost::shared_ptr<OpenValue> dup();

  boost::shared_ptr<Value> get()
  {
    return shared_from_this();
  }
};

/// @brief A (possibly settable) field of an externally defined
/// (foreign) object.
struct PrimFieldValue : public OpenValue {
  bool isVector;
  size_t ndx;
  bool isConstant;
  std::string ident;
  void (*doSet)(PrimFieldValue&,
		const boost::shared_ptr<Value> &);

  boost::shared_ptr<Value> (*doGet)(PrimFieldValue&);

  boost::shared_ptr<Value> rValue;

  void cacheTheValue()
  {
    rValue = doGet(*this);
  }

  PrimFieldValue(const std::string& _ident, bool _isConstant,
		 void (*_doSet)(PrimFieldValue&,
				const boost::shared_ptr<Value> &),
		 boost::shared_ptr<Value> (*_doGet)(PrimFieldValue&),
		 bool _isVector = false
		 )
    :OpenValue(vk_primField)
  {
    isConstant = _isConstant;
    ident = _ident;
    doSet = _doSet;
    doGet = _doGet;
    isVector = _isVector;
    ndx = ~0u;
  }

  PrimFieldValue(const PrimFieldValue& pmv)
    : OpenValue(pmv.kind)
  {
    isConstant = pmv.isConstant;
    ident = pmv.ident;
    doSet = pmv.doSet;
    doGet = pmv.doGet;
    isVector = pmv.isVector;
    ndx = ~0u;
  }

  static inline boost::shared_ptr<PrimFieldValue> 
  make(const std::string& _ident, bool _isConstant,
		 void (*_doSet)(PrimFieldValue&,
				const boost::shared_ptr<Value> &),
		 boost::shared_ptr<Value> (*_doGet)(PrimFieldValue&),
		 bool _isVector = false) {
    PrimFieldValue *tmp = 
      new PrimFieldValue(_ident, _isConstant, _doSet, _doGet, _isVector);
    return boost::shared_ptr<PrimFieldValue>(tmp);
  }

  static inline boost::shared_ptr<PrimFieldValue> 
  make(const PrimFieldValue& pmv) {
    PrimFieldValue *tmp = new PrimFieldValue(pmv);
    return boost::shared_ptr<PrimFieldValue>(tmp);
  }

  boost::shared_ptr<OpenValue> dup();

  boost::shared_ptr<Value> get()
  {
    return rValue;
  }

  virtual boost::shared_ptr<Value> closeWith(boost::shared_ptr<Value> value)
  {
    boost::shared_ptr<Value> v = OpenValue::closeWith(value);
    if (!isVector)
      boost::dynamic_pointer_cast<PrimFieldValue>(v)->cacheTheValue();
    
    return v;
  }

  void set(const boost::shared_ptr<Value>& v)
  {
    doSet(*this, v);
  }
};

class CapValue : public Value {
  boost::shared_ptr<Environment<Value> > getWrapperEnvironment();
  boost::shared_ptr<Environment<Value> > getEndpointEnvironment();
  boost::shared_ptr<Environment<Value> > getGptEnvironment();
  boost::shared_ptr<Environment<Value> > getProcessEnvironment();
public:
  capability              cap;
  boost::shared_ptr<CoyImage> ci;

  boost::shared_ptr<Environment<Value> > getEnvironment();

  // This will throw if the capability is not a CapPage capability.
  boost::shared_ptr<PrimFieldValue> derefAsVector(size_t ndx);

  CapValue(const boost::shared_ptr<CoyImage>& _ci, const capability& _c)
    :Value(vk_cap)
  {
    cap = _c;
    ci = _ci;
  }

  CapValue(const CapValue& _cv)
    :Value(vk_cap)
  {
    cap = _cv.cap;
    ci = _cv.ci;
  }

  static inline boost::shared_ptr<CapValue> 
  make(const boost::shared_ptr<CoyImage>& _ci, const capability& _c) {
    CapValue *tmp = new CapValue(_ci, _c);
    return boost::shared_ptr<CapValue>(tmp);
  }

  static inline boost::shared_ptr<CapValue> 
  make(const CapValue& _cv) {
    CapValue *tmp = new CapValue(_cv);
    return boost::shared_ptr<CapValue>(tmp);
  }
};

/// @brief A first-class environment
struct EnvValue : public Value {
  boost::shared_ptr<Environment<Value> > env;

  boost::shared_ptr<Environment<Value> > getEnvironment()
  {
    return env;
  }

  EnvValue(const boost::shared_ptr<Environment<Value> >& _env)
    :Value(vk_env)
  {
    env = _env;
  }

  static inline boost::shared_ptr<EnvValue> 
  make(const boost::shared_ptr<Environment<Value> >& _env) {
    EnvValue *tmp = new EnvValue(_env);
    return boost::shared_ptr<EnvValue>(tmp);
  }
};

/// @brief A ``wrapped'' value that encapsulates the binding of a
/// value within an environment.
///
/// This exists to support l-values. See the lengthy comment on the
/// Value class.
struct BoundValue : public Value {
  boost::shared_ptr<Binding<Value> > binding;
  boost::shared_ptr<Value > rvalue;

  BoundValue(const boost::shared_ptr<Binding<Value> >& _binding)
    :Value(vk_boundValue)
  {
    binding = _binding;
    rvalue = binding->val;	// must cache eagerly
  }

  static inline boost::shared_ptr<BoundValue> 
  make(const boost::shared_ptr<Binding<Value> >& _binding) {
    BoundValue *tmp = new BoundValue(_binding);
    return boost::shared_ptr<BoundValue>(tmp);
  }

  boost::shared_ptr<Value> get()
  {
    return rvalue;
  }

  void set(const boost::shared_ptr<Value>& v);
};

#endif /* VALUE_HXX */
