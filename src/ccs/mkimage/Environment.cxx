/*
 * Copyright (C) 2005, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdint.h>
#include <dirent.h>
#include <assert.h>

#include "Value.hxx"
#include "Environment.hxx"

using namespace std;
using namespace boost;

template<class T>
shared_ptr<Binding<T> >
Environment<T>::getLocalBinding(const std::string& nm) const
{
  for (size_t i = 0; i < bindings.size(); i++)
    if (bindings[i]->nm == nm)
      return bindings[i];

  return shared_ptr<Binding<T> >();
}

template<class T>
shared_ptr<Binding<T> > 
Environment<T>::getBinding(const std::string& nm) const
{
  shared_ptr<Binding<T> > binding = getLocalBinding(nm);
  
  if (binding)
    return binding;

  if (parent)
    return parent->getBinding(nm);

  return shared_ptr<Binding<T> >();
}

template<class T>
void
Environment<T>::removeBinding(const std::string& nm)
{
  typename BindingVec::iterator it;

  for (it = bindings.begin(); it != bindings.end(); it++) {
    if ((*it)->nm == nm) {
      bindings.erase(it);
      return;
    }
  }

  if (parent)
    parent->removeBinding(nm);
}

template<class T>
void
Environment<T>::addBinding(const std::string& name, 
			    shared_ptr<T> val, 
			    bool rebind)
{
  if (rebind) {
    shared_ptr<Binding<T> > binding = getBinding(name);
    if (binding) {
      binding->val = val;
      binding->flags = 0;
      return;
    }
  }

  bindings.insert(bindings.begin(), Binding<T>::make(name, val));
}

template<class T>
shared_ptr<Environment<T> > 
Environment<T>::newScope()
{
  shared_ptr<Environment<T> > nEnv = 
    Environment<T>::make(this->shared_from_this());

  return nEnv;
}

template<class T>
Environment<T>::~Environment()
{
}

// EXPLICIT INSTANTIATIONS:

template shared_ptr< Binding<Value> > 
Environment<Value>::getLocalBinding(const std::string& nm) const;

template shared_ptr< Binding<Value> > 
Environment<Value>::getBinding(const std::string& nm) const;

template void
Environment<Value>::addBinding(const std::string& nm, shared_ptr<Value> val,
			       bool rebind);

template void
Environment<Value>::removeBinding(const std::string& nm);

template shared_ptr<Environment<Value> > 
Environment<Value>::newScope();

template
Environment<Value>::~Environment();
 
