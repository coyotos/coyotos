/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <string>
#include <algorithm>

#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <libsherpa/utf8.hxx>
#include <libcoyimage/ExecImage.hxx>

#include "Environment.hxx"
#include "Value.hxx"
#include "AST.hxx"
#include "builtin.hxx"
#include "Options.hxx"

using namespace std;
using namespace boost;
using namespace sherpa;

static shared_ptr<Environment<Value> > builtins;

/** @bug This ought to be pulled from the constant defined in the
 * Range IDL file. */
static const oid_t physOidStart = 0xff00000000000000ull;

static inline coyaddr_t 
align_up(coyaddr_t x, coyaddr_t y)
{
  return (x + y - 1) & ~(y-1);
}

bool
isArgType(shared_ptr<Value> v, Value::ValKind vk)
{
  return (v->kind == vk);
}

void
requireArgType(InterpState& is, const string& nm,
	       vector<shared_ptr<Value> >& args, size_t ndx, Value::ValKind vk)
{
  shared_ptr<Value> v = args[ndx];

  if (!isArgType(v, vk)) {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate argument type for argument "
		 << ndx << " to " 
		 << nm << "()\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<BoolValue>
needBoolArg(InterpState& is, const string& nm,
	   vector<shared_ptr<Value> >& args, size_t ndx)
{
  shared_ptr<Value> v = args[ndx];

  try {
    return is.value_is_true_expr(v) ? BoolValue::True : BoolValue::False;
  }
  catch (...) {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate argument type for boolean-valued argument "
		 << ndx << " to " 
		 << nm << "()\n";
    is.backtrace();
    throw;
  }
}

shared_ptr<IntValue>
needIntArg(InterpState& is, const string& nm,
	   vector<shared_ptr<Value> >& args, size_t ndx)
{
  requireArgType(is, nm, args, ndx, Value::vk_int);
  return dynamic_pointer_cast<IntValue>(args[ndx]);
}

shared_ptr<StringValue>
needStringArg(InterpState& is, const string& nm,
	      vector<shared_ptr<Value> >& args, size_t ndx)
{
  requireArgType(is, nm, args, ndx, Value::vk_string);
  return dynamic_pointer_cast<StringValue>(args[ndx]);
}

shared_ptr<CapValue>
needCapArgType(InterpState& is, const string& nm,
	       vector<shared_ptr<Value> >& args, size_t ndx,
	       CapType ct1, CapType ct2)
{
  requireArgType(is, nm, args, ndx, Value::vk_cap);
  shared_ptr<CapValue> cv = dynamic_pointer_cast<CapValue>(args[ndx]);
  if ((cv->cap.type == ct1) || (cv->cap.type == ct2))
    return cv;

  is.errStream << is.curAST->loc << " "
	       << "Capability argument of type "
	       << cap_typeName(ct1);
  if (ct1 != ct2)
    is.errStream << " (or " << cap_typeName(ct2) << ")"
		 << " is required for argument "
		 << ndx << " to " 
		 << nm << "()\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<CapValue>
needAnyCapArg(InterpState& is, const string& nm,
	      vector<shared_ptr<Value> >& args, size_t ndx)
{
  requireArgType(is, nm, args, ndx, Value::vk_cap);
  return dynamic_pointer_cast<CapValue>(args[ndx]);
}

shared_ptr<CapValue>
needBankArg(InterpState& is, const string& nm,
	    vector<shared_ptr<Value> >& args, size_t ndx)
{
  needCapArgType(is, nm, args, ndx, ct_Entry);
  shared_ptr<CapValue> cv = dynamic_pointer_cast<CapValue>(args[ndx]);

  // Now this is truly a nuisance, but it is a useful error check to
  // validate that this entry capability might plausibly be a bank
  // capability:
  shared_ptr<CiEndpoint> ep = is.ci->GetEndpoint(cv->cap);

  oid_t bankOID = ep->v.endpointID - 1; /* unbias the endpoint ID */
  if (bankOID >= is.ci->vec.bank.size())
    goto bad;

  if (is.ci->vec.bank[bankOID].oid != cv->cap.u2.oid)
    goto bad;

  return cv;

 bad:
  is.errStream << is.curAST->loc << " "
	       << "Space bank capability is required for argument "
	       << ndx << " to "
	       << nm << "()\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<CapValue>
needSpaceArg(InterpState& is, const string& nm,
	    vector<shared_ptr<Value> >& args, size_t ndx)
{
  requireArgType(is, nm, args, ndx, Value::vk_cap);
  shared_ptr<CapValue> cv = dynamic_pointer_cast<CapValue>(args[ndx]);
  if (cap_isMemory(cv->cap) || cap_isType(cv->cap, ct_Null))
    return cv;

  is.errStream << is.curAST->loc << " "
	       << "Address space capability required for argument "
	       << ndx << " to " 
	       << nm << "()\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value> 
pf_print_one(InterpState& is,
	     shared_ptr<Value> v0)
{
  switch(v0->kind) {
  case Value::vk_int:
    {
      shared_ptr<IntValue> i = 
	dynamic_pointer_cast<IntValue>(v0);
      string s = i->bn.asString(10);
      std::cout << s;

      break;
    }
  case Value::vk_float:
    {
      shared_ptr<FloatValue> f = 
	dynamic_pointer_cast<FloatValue>(v0);
      std::cout << f->d;

      break;
    }
  case Value::vk_unit:
    {
      std::cout << "UNIT";
      break;
    }
  case Value::vk_cap:
    {
      shared_ptr<CapValue> cv =
	dynamic_pointer_cast<CapValue>(v0);
      std::cout << cv->cap;
      break;
    }
  case Value::vk_string:
    {
      shared_ptr<StringValue> sv =
	dynamic_pointer_cast<StringValue>(v0);
      const char *s = sv->s.c_str();
      const char *send = s + sv->s.size();

      while (s != send) {
	const char *snext;
	char utf8[7];
	uint32_t codePoint = sv->DecodeStringCharacter(s, &snext);
	unsigned len = utf8_encode(codePoint, utf8);

	for (unsigned pos = 0; pos < len; pos++) {
	  unsigned char c = utf8[pos];
	  std::cout << c;
	}

	s = snext;
      }

      break;
    }
  case Value::vk_env:
    {
      shared_ptr<EnvValue> ev =
	dynamic_pointer_cast<EnvValue>(v0);
      shared_ptr<Environment<Value> > env = ev->env;

      for (size_t i = 0; i < env->bindings.size(); i++) {
	std::cout << ((i == 0) ? "[ " : "\n  ")
		  << env->bindings[i]->nm << " = ";

	pf_print_one(is, env->bindings[i]->val);

	if (env->bindings[i]->flags)
	  std::cout << " (";

	string comma;

	if (env->bindings[i]->flags & BF_CONSTANT) {
	  std::cout << comma << "const";
	  comma = ", ";
	}
	if (env->bindings[i]->flags & BF_EXPORTED) {
	  std::cout << comma << "public";
	  comma = ", ";
	}
	if (env->bindings[i]->flags)
	  std::cout << ")";
      }
      std::cout << " ]";
      break;
    }
  case Value::vk_function:
    {
      shared_ptr<FnValue> fn =
	dynamic_pointer_cast<FnValue>(v0);
      cout << "<fn " << fn->nm << ", " << 
	fn->args->children.size() << " args>";
      break;
    }
  case Value::vk_primfn:
    {
      shared_ptr<PrimFnValue> fn =
	dynamic_pointer_cast<PrimFnValue>(v0);
      cout << "<primfn " << fn->nm << ", " << fn->minArgs;
      if (fn->maxArgs == 0)
	cout << "...";
      else if (fn->maxArgs != fn->minArgs)
	cout << "-" << fn->maxArgs;
      cout << " args>";
      break;
    }
  case Value::vk_primMethod:
    {
      shared_ptr<PrimMethodValue> fn =
	dynamic_pointer_cast<PrimMethodValue>(v0);
      cout << "<primMethod " << fn->nm << ", " << fn->minArgs;
      if (fn->maxArgs == 0)
	cout << "...";
      else if (fn->maxArgs != fn->minArgs)
	cout << "-" << fn->maxArgs;
      cout << " args>";
      break;
    }
  default:
    {
      is.errStream << is.curAST->loc << " "
		   << "Don't know how to print value type " 
		   << v0->kind << '\n';
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }

  return Value::Unit;
}

shared_ptr<Value> 
pf_doprint(PrimFnValue& pfv,
	   InterpState& is,
	   vector<shared_ptr<Value> >& args)
{
  for (size_t i = 0; i < args.size(); i++) {
    shared_ptr<Value> val = args[i];

    pf_print_one(is, val);
  }

  return Value::Unit;
}

shared_ptr<Value>
pf_doprint_star(InterpState& is,
		vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> v0 = args[0];

  if (v0->kind != Value::vk_cap) {
    // Be forgiving:
    pf_print_one(is, v0);
    std::cout << '\n';
    return Value::Unit;
  }

  shared_ptr<CapValue> cv =dynamic_pointer_cast<CapValue>(v0);

  switch(cv->cap.type) {
  case ct_Process:
    {
      shared_ptr<CiProcess> ob = is.ci->GetProcess(cv->cap);
      
      std::cout << *ob;
      break;
    }
  case ct_Endpoint:
    {
      shared_ptr<CiEndpoint> ob = is.ci->GetEndpoint(cv->cap);
      
      std::cout << *ob;
      break;
    }
  case ct_GPT:
    {
      shared_ptr<CiGPT> ob = is.ci->GetGPT(cv->cap);
      
      std::cout << *ob;
      break;
    }
  case ct_Page:
    {
      shared_ptr<CiPage> ob = is.ci->GetPage(cv->cap);

      std::cout << *ob;
      break;
    }
  case ct_CapPage:
    {
      shared_ptr<CiCapPage> ob = is.ci->GetCapPage(cv->cap);

      std::cout << *ob;
      break;
    }

  default:
    // Be forgiving:
    pf_print_one(is, v0);
    std::cout << '\n';
    break;
  }

  //  std::cout << '\n';
  return Value::Unit;
}

shared_ptr<Value>
pf_fatal(PrimFnValue& pfv,
        InterpState& is,
        vector<shared_ptr<Value> >& args)
{
  shared_ptr<StringValue> str = needStringArg(is, pfv.nm, args, 0);

  is.errStream << is.curAST->loc << " fatal: " << str->s << '\n';
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_dump_usage(PrimFnValue& pfv,
	      InterpState& is,
	      vector<shared_ptr<Value> >& args)
{
  std::cout << *is.ci << endl;

  //  std::cout << '\n';
  return Value::Unit;
}

shared_ptr<Value>
pf_add(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a =dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b =dynamic_pointer_cast<IntValue>(vr);

    return IntValue::make(a->bn + b->bn);
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    return FloatValue::make(a->d + b->d);
  }
  else if (vl->kind == Value::vk_string && vr->kind == Value::vk_string) {
    shared_ptr<StringValue> a =dynamic_pointer_cast<StringValue>(vl);
    shared_ptr<StringValue> b =dynamic_pointer_cast<StringValue>(vr);

    return StringValue::make(a->s + b->s);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_sub(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a =dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b =dynamic_pointer_cast<IntValue>(vr);

    return IntValue::make(a->bn - b->bn);
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    return FloatValue::make(a->d - b->d);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_mul(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a =dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b =dynamic_pointer_cast<IntValue>(vr);

    return IntValue::make(a->bn * b->bn);
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    return FloatValue::make(a->d * b->d);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_div(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b = dynamic_pointer_cast<IntValue>(vr);

    return IntValue::make(a->bn / b->bn);
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    return FloatValue::make(a->d / b->d);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_mod(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b = dynamic_pointer_cast<IntValue>(vr);

    return IntValue::make(a->bn % b->bn);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_negate(PrimFnValue& pfv,
	  InterpState& is,
	  vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];

  if (vl->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);

    return IntValue::make(- a->bn);
  }
  else if (vl->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);

    return FloatValue::make(- a->d);
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_cmp(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b = dynamic_pointer_cast<IntValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->bn < b->bn);
    case '>':
      return IntValue::make (a->bn > b->bn);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->d < b->d);
    case '>':
      return IntValue::make (a->d > b->d);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else if (vl->kind == Value::vk_string && vr->kind == Value::vk_string) {
    shared_ptr<StringValue> a = dynamic_pointer_cast<StringValue>(vl);
    shared_ptr<StringValue> b = dynamic_pointer_cast<StringValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->s.compare(b->s) < 0);
    case '>':
      return IntValue::make (a->s.compare(b->s) > 0);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_cmpe(PrimFnValue& pfv,
	InterpState& is,
	vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];
  shared_ptr<Value> vr = args[1];

  if (vl->kind == Value::vk_int && vr->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);
    shared_ptr<IntValue> b = dynamic_pointer_cast<IntValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->bn <= b->bn);
    case '>':
      return IntValue::make (a->bn >= b->bn);
    case '=':
      return IntValue::make (a->bn == b->bn);
    case '!':
      return IntValue::make (a->bn != b->bn);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else if (vl->kind == Value::vk_float && vr->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);
    shared_ptr<FloatValue> b = dynamic_pointer_cast<FloatValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->d <= b->d);
    case '>':
      return IntValue::make (a->d >= b->d);
    case '=':
      return IntValue::make (a->d == b->d);
    case '!':
      return IntValue::make (a->d != b->d);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else if (vl->kind == Value::vk_string && vr->kind == Value::vk_string) {
    shared_ptr<StringValue> a = dynamic_pointer_cast<StringValue>(vl);
    shared_ptr<StringValue> b = dynamic_pointer_cast<StringValue>(vr);

    switch(pfv.nm[0]) {
    case '<':
      return IntValue::make (a->s.compare(b->s) <= 0);
    case '>':
      return IntValue::make (a->s.compare(b->s) >= 0);
    case '=':
      return IntValue::make (a->s.compare(b->s) == 0);
    case '!':
      return IntValue::make (a->s.compare(b->s) != 0);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  } 
  else if (vl->kind == Value::vk_cap && vr->kind == Value::vk_cap &&
	(pfv.nm[0] == '=' || pfv.nm[0] == '!')) {
    /* capabilities can only be compared for equality */
    shared_ptr<CapValue> a = dynamic_pointer_cast<CapValue>(vl);
    shared_ptr<CapValue> b = dynamic_pointer_cast<CapValue>(vr);

    bool equal = (memcmp(&a->cap, &b->cap, sizeof (a->cap)) == 0);
    switch(pfv.nm[0]) {
    case '=':
      return IntValue::make (equal);
    case '!':
      return IntValue::make (!equal);
    default:
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_not(PrimFnValue& pfv,
       InterpState& is,
       vector<shared_ptr<Value> >& args)
{
  shared_ptr<Value> vl = args[0];

  if (vl->kind == Value::vk_int) {
    shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vl);

    return IntValue::make((a->bn == 0) ? 1 : 0);
  }
  else if (vl->kind == Value::vk_float) {
    shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vl);

    return IntValue::make((a->d == 0.0) ? 1 : 0);
  }
  else if (vl->kind == Value::vk_string) {
    shared_ptr<StringValue> a = dynamic_pointer_cast<StringValue>(vl);

    return IntValue::make(BigNum(0));
  }
  else {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate dynamic types to "
		 << pfv.nm << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_downgrade(PrimFnValue& pfv,
	     InterpState& is,
	     vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> v_cap = needAnyCapArg(is, pfv.nm, args, 0);
  shared_ptr<CapValue> out = CapValue::make(*v_cap);

  if (cap_isType(out->cap, ct_Null))
    return out;				/* Null caps are as weak as they get */

  if (!cap_isMemory(out->cap)) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() requires a memory capability" << '\n';
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  if (pfv.nm == "opaque" || pfv.nm == "nocall") {
    if (out->cap.type != ct_GPT) {
      is.errStream << is.curAST->loc << " "
		   << "Argument to "
		   << pfv.nm
		   << "() must be a GPT capability.\n";
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }

  if (pfv.nm == "nocache" || pfv.nm == "writethrough") {
    if ((out->cap.type != ct_Page) ||
	(out->cap.u2.oid < physOidStart)) {
      is.errStream << is.curAST->loc << " "
		   << "Argument to "
		   << pfv.nm
		   << "() must be a Physical page frame capability.\n";
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }
  }

  if (pfv.nm == "readonly") {
    out->cap.restr |= CAP_RESTR_RO;
    return out;
  }

  if (pfv.nm == "weaken") {
    out->cap.restr |= CAP_RESTR_WK | CAP_RESTR_RO;
    return out;
  }

  if (pfv.nm == "noexec") {
    out->cap.restr |= CAP_RESTR_NX;
    return out;
  }

  if (pfv.nm == "opaque") {
    out->cap.restr |= CAP_RESTR_OP;
    return out;
  }

  if (pfv.nm == "nocall") {
    out->cap.restr |= CAP_RESTR_NC;
    return out;
  }

  if (pfv.nm == "nocache") {
    out->cap.restr |= CAP_RESTR_CD;
    return out;
  }

#if 0
  if (pfv.nm == "writethrough") {
    out->cap.restr |= CAP_RESTR_WT;
    return out;
  }
#endif

  is.errStream << is.curAST->loc << " "
	       << "Buggered mkimage binding of \""
	       << pfv.nm << "\" in builtin.cxx.\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_guard_cap(PrimFnValue& pfv,
	     InterpState& is,
	     vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> v_cap = needAnyCapArg(is, pfv.nm, args, 0);
  shared_ptr<IntValue> v_offset = needIntArg(is, pfv.nm, args, 1);

  if (!cap_isMemory(v_cap->cap)) {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate capability type to "
		 << pfv.nm << "()\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  shared_ptr<CapValue> ocap = CapValue::make(*v_cap);

  // This function relies on the layout pun for window capabilities.
  coyaddr_t guard = v_offset->bn.as_uint64();
  uint32_t l2g = ocap->cap.u1.mem.l2g;

  if (args.size() > 2) {
    shared_ptr<IntValue> v_l2g = needIntArg(is, pfv.nm, args, 2);
    l2g = v_l2g->bn.as_uint32();
  }

  if (l2g > (8*sizeof(coyaddr_t)) ||
      l2g < is.ci->target.pagel2v) {
    is.errStream << is.curAST->loc << " "
		 << "l2g argument to "
		 << pfv.nm << "() must be in the range "
		 << is.ci->target.pagel2v
		 << "-"
		 << (8*sizeof(coyaddr_t))
		 << "\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
  if ((guard & (((1 << (l2g - 1)) << 1) - 1)) != 0) {
    is.errStream << is.curAST->loc << " "
		 << "guard value has bits below 2^(l2g) in call to "
		 << pfv.nm << "()\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
  
  ocap->cap.u1.mem.l2g = l2g;
  ocap->cap.u1.mem.match = ((guard >> (l2g - 1)) >> 1);
  
  coyaddr_t effGuard = (ocap->cap.u1.mem.match << (l2g - 1)) << 1;
  if (guard != effGuard) {
    is.errStream << is.curAST->loc << " "
	      << "guard value truncated in call to "
	      << pfv.nm << "()\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
  
  return ocap;
}

shared_ptr<Value>
pf_memcap_ops(PrimFnValue& pfv,
	      InterpState& is,
	      vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> v_cap = needAnyCapArg(is, pfv.nm, args, 0);

  if (! (cap_isMemory(v_cap->cap) && cap_isObjectCap(v_cap->cap)) ) {
    is.errStream << is.curAST->loc << " "
		 << "Inappropriate capability type to "
		 << pfv.nm << "()\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  // This function relies on the layout pun for window capabilities.
  uint32_t match = v_cap->cap.u1.mem.match;
  uint32_t l2g =   v_cap->cap.u1.mem.l2g;

  if (pfv.nm == "get_l2g")
    return IntValue::make(l2g);

  if (pfv.nm == "get_match")
    return IntValue::make(match);

  is.errStream << is.curAST->loc << " "
	       << "Buggered mkimage binding of \""
	       << pfv.nm << "\" in builtin.cxx.\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_mk_raw_obcap(PrimFnValue& pfv,
		InterpState& is,
		vector<shared_ptr<Value> >& args)
{
  shared_ptr<IntValue> v_oid = needIntArg(is, pfv.nm, args, 0);
  oid_t oid = v_oid->bn.as_uint64();

  if (pfv.nm == "PageCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Page, oid));

  if (pfv.nm == "PhysPageCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Page, oid + physOidStart));

  if (pfv.nm == "CapCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_CapPage, oid));

  if (pfv.nm == "GptCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_GPT, oid));

  if (pfv.nm == "ProcessCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Process, oid));

  if (pfv.nm == "EndpointCap")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Endpoint, oid));

  is.errStream << is.curAST->loc << " "
	       << "Buggered mkimage binding of \""
	       << pfv.nm << "\" in builtin.cxx.\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_guarded_space(PrimFnValue& pfv,
		 InterpState& is,
		 vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  shared_ptr<CapValue> spc = needSpaceArg(is, pfv.nm, args, 1);
  shared_ptr<IntValue> offset = needIntArg(is, pfv.nm, args, 2);
  uint32_t l2g = 0;
  if (args.size() > 3)
    l2g = needIntArg(is, pfv.nm, args, 3)->bn.as_uint32();

  return CapValue::make(is.ci, 
		      is.ci->MakeGuardedSubSpace(bank->cap,
						 spc->cap, 
						 offset->bn.as_uint64(),
						 l2g));
}

shared_ptr<Value>
pf_insert_subspace(PrimFnValue& pfv,
		   InterpState& is,
		   vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  shared_ptr<CapValue> spc = needSpaceArg(is, pfv.nm, args, 1);
  shared_ptr<CapValue> subspc = needSpaceArg(is, pfv.nm, args, 2);
  shared_ptr<IntValue> offset = needIntArg(is, pfv.nm, args, 3);
  uint32_t l2arg = 0;

  if (args.size() > 4)
    l2arg = needIntArg(is, pfv.nm, args, 4)->bn.as_uint32();

  return CapValue::make(is.ci, 
		      is.ci->AddSubSpaceToSpace(bank->cap, 
						spc->cap,
						offset->bn.as_uint64(),
						subspc->cap,
						l2arg));
}

shared_ptr<Value>
pf_lookup_addr(PrimFnValue& pfv,
		   InterpState& is,
		   vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> spc = needSpaceArg(is, pfv.nm, args, 0);
  shared_ptr<IntValue> offset = needIntArg(is, pfv.nm, args, 1);

  // bounds check?
  CoyImage::LeafInfo li = 
    is.ci->LeafAtAddr(spc->cap, offset->bn.as_uint64());

  shared_ptr<Environment<Value> > env = Environment<Value>::make();

  env->addConstant("cap", CapValue::make(is.ci, li.cap));
  env->addConstant("offset", IntValue::make(li.offset));
  env->addConstant("ro", IntValue::make(!!(li.restr & CAP_RESTR_RO)));
  env->addConstant("nx", IntValue::make(!!(li.restr & CAP_RESTR_NX)));
  env->addConstant("wk", IntValue::make(!!(li.restr & CAP_RESTR_WK)));
  env->addConstant("op", IntValue::make(!!(li.restr & CAP_RESTR_OP)));

  return EnvValue::make(env);
}

shared_ptr<Value>
pf_readfile(PrimFnValue& pfv,
	    InterpState& is,
	    vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  shared_ptr<StringValue> str = needStringArg(is, pfv.nm, args, 1);
  coyaddr_t offset = 0;
  if (args.size() > 2)
    offset = needIntArg(is, pfv.nm, args, 2)->bn.as_uint64();;

  filesystem::path file = Options::resolveLibraryPath(str->s);

  if (file.empty() || !filesystem::exists(file)) {
    is.errStream << is.curAST->loc << " "
		 << "file \"" << file << "\" not found.\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  if (!filesystem::is_regular(file)) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() filename argument must name a regular file.\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  if (offset & (is.ci->target.pageSize -1u)) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() offset must be a pagesize-multiple\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  ifstream ifs(file.string().c_str(), ifstream::in | ifstream::binary);
  if (ifs.fail()) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() unable to open \"" << file 
		 << "\"\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  const size_t pagesz = is.ci->target.pageSize;

  capability space = is.ci->CiCap();

  uintmax_t fileSize = filesystem::file_size(file);

  for (uintmax_t pos = 0; pos < fileSize; pos += pagesz) {
    capability pageCap = is.ci->AllocPage(bank->cap);
    shared_ptr<CiPage> page = is.ci->GetPage(pageCap);
    
    ifs.read((char *)page->data, pagesz);
    space = is.ci->AddSubSpaceToSpace(bank->cap,
				      space,
				      pos + offset,
				      pageCap);
  }

  if (!contains(is.deps, file))
    is.deps.insert(file);

  return CapValue::make(is.ci, space);
}

shared_ptr<Value>
pf_loadimage(PrimFnValue& pfv,
	     InterpState& is,
	     vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  shared_ptr<StringValue> str = needStringArg(is, pfv.nm, args, 1);

  filesystem::path file = Options::resolveLibraryPath(str->s);

  if (file.empty() || !filesystem::exists(file)) {
    is.errStream << is.curAST->loc << " "
		 << "file \"" << str->s << "\" not found.\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  if (!filesystem::is_regular(file)) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() filename argument must name a regular file.\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  // Our address space construction strategy is very brute force, and
  // it will fail (with notice) if the page offsets within the file
  // and the page offsets within the address space pages do not
  // align. 
  //
  // What we are trying to accomplish here is page sharing for pages
  // that are (a) multiply mapped, or (b) overlap a boundary from one
  // region to another. Case (a) can occur if text/data have widely
  // separated virtual addresses, but some of the data appears in the
  // same file page as the end of text. Case (b) occurs when _etext ==
  // _edata and some of the data appears in the same file page as the
  // end of text.
  //
  // Because of the sharing issue, we proceed as follows:
  //
  //  For each page of the file
  //    For each base address at which the page is loadable (if any)
  //      Scan all loadable regions to see if they overlap that page
  //        virtual address.
  //      Take the union (positive logic) of the permissions of these
  //        regions as the page permissions. Rationale: if any region
  //        requires write, and the page VMA is identical, then the
  //        mapping must permit write.
  //      Map the page at this address with these permisions.
  //
  // There is probably a simpler way to do this, but binaries
  // typically have no more than two or three loadable regions. In
  // light of this, an N^2 algorithm seems tolerable here, and this
  // algorithm will be visibly related to the one used for symbol
  // table loads later.

  // We are going to iterate through each page of the file,
  // checking if it overlaps with one or more regions.
  ExecImage ei(file);

  if (ei.elfMachine != is.ci->target.elfMachine) {
    is.errStream << is.curAST->loc << " "
		 << "file \"" << file
		 << "\" is not an executable for target "
		 << is.ci->target.name
		 << std::endl;
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  ei.sortByOffset();

  shared_ptr<Environment<Value> > imageEnv = Environment<Value>::make();
  imageEnv->addConstant("pc", IntValue::make(ei.entryPoint));

  capability addrSpace = is.ci->CiCap();

  uintmax_t fileSize = filesystem::file_size(file);

  for (uintmax_t offset = 0; offset < fileSize;
       offset += is.ci->target.pageSize) {

    uintmax_t bound = 
      min((uintmax_t)(offset + is.ci->target.pageSize), fileSize);
    
    bool overlaps = false;
    // Make an initial pass to see if this page of the file is
    // needed. This is purely to avoid allocating excess pages in the
    // image:
    for (size_t r = 0; r < ei.region.size(); r++) {
      ExecRegion &rgn = ei.region[r];

      if (rgn.fileOffsetOverlaps(offset, bound)) {
	overlaps = true;
	break;
      }
    }

    if (!overlaps)
      continue;

    uintmax_t minoffset = bound; // minimum page offset used.  Start at end-of-page
    uintmax_t maxoffset = offset;// maximum offset used.  Starts at start-of-page

    // Do a second pass to determine the range of data used.  We need to
    // zero anything outside of that.
    for (size_t r = 0; r < ei.region.size(); r++) {
      ExecRegion &rgn = ei.region[r];

      if (!rgn.fileOffsetOverlaps(offset, bound))
	continue;

      if ((uintmax_t)rgn.offset < minoffset)
	minoffset = rgn.offset;
      if ((uintmax_t)(rgn.offset + rgn.filesz) > maxoffset)
	maxoffset = rgn.offset + rgn.filesz;
    }

    // Load the page:
    capability pgCap = is.ci->AllocPage(bank->cap);
    shared_ptr<CiPage> pg = is.ci->GetPage(pgCap);
    memcpy(pg->data, ei.image.substr(offset, bound - offset).c_str(),
	   bound - offset);

    // If any part of the page is unused, zero it
    if (minoffset > offset)
      memset(pg->data, 0, minoffset - offset);
    if (maxoffset < bound)
      memset(pg->data + (maxoffset - offset), 0, (bound - maxoffset));

    // Insert the page with appropriate permissions at each region
    // where it appears:
    for (size_t r = 0; r < ei.region.size(); r++) {
      ExecRegion &rgn = ei.region[r];

      if (!rgn.fileOffsetOverlaps(offset, bound))
	continue;

      assert((rgn.offset % is.ci->target.pageSize) ==
	     (rgn.vaddr  % is.ci->target.pageSize));

      uint32_t perm = 0;

      // Page overlaps current region. Work out the page virtual
      // address of this mapping:
      uint64_t vaddr = rgn.vaddr;
      if ((uintmax_t)rgn.offset > offset)
	vaddr -= (rgn.offset - offset);
      else
	vaddr += (offset - rgn.offset);

      uint64_t vbound = vaddr + is.ci->target.pageSize;

      assert ((vaddr % is.ci->target.pageSize) == 0);

      // Make a second pass over the regions to collect permissions
      // based on virtual page address overlap:
      for (size_t rv = 0; rv < ei.region.size(); rv++) {
	ExecRegion &rgn = ei.region[rv];
	if (rgn.vAddrOverlaps(vaddr, vbound))
	  perm |= rgn.perm;
      }

      // This page needs to be inserted at /vaddr/ with permissions
      // /perm/:
      pgCap.restr =
        (((perm & ER_X) == 0) ? CAP_RESTR_NX : 0) |
        (((perm & ER_W) == 0) ? CAP_RESTR_RO : 0);

#ifdef DEBUG
      std::ios_base::fmtflags flgs = is.errStream.flags();

      is.errStream << "Calling addSubspace at " 
		   << std::hex
		   << std::showbase
		   << er.vaddr + pos
		   << std::dec
		   << std::endl;

      is.errStream.flags(flgs);

      is.errStream << "  Space    = " << addrSpace << endl;
      is.errStream << "  SubSpace = " << pgCap << endl;
#endif

      addrSpace = 
	is.ci->AddSubSpaceToSpace(bank->cap, addrSpace, vaddr, pgCap);
#ifdef DEBUG
      is.errStream << "  Result   = " << addrSpace << endl;
#endif
    }
  }

  // Now do a post-pass to deal with missing BSS pages. These
  // are not backed by anything in the file, and the pass above will
  // not have added them because it checks for overlap based on the
  // filesize field.
  ei.sortByAddress();
  coyaddr_t vaddr = 0;
  for (size_t r = 0; r < ei.region.size(); r++) {
    ExecRegion &rgn = ei.region[r];

    // Skip any part of this region that is backed by the file. Round
    // up because we chunked the file into pages when we were loading it:
    coyaddr_t rvaddr = rgn.vaddr + rgn.filesz;
    coyaddr_t rvbound = rgn.vaddr + rgn.memsz;
    rvaddr = align_up(rvaddr, is.ci->target.pageSize);
    rvbound = align_up(rvbound, is.ci->target.pageSize);

    vaddr = max(vaddr, rvaddr);

    while (vaddr < rvbound) {
      capability pgCap = is.ci->AllocPage(bank->cap);
      shared_ptr<CiPage> pg = is.ci->GetPage(pgCap);
      memset(pg->data, 0, is.ci->target.pageSize);
      
      coyaddr_t pgvbound = vaddr + is.ci->target.pageSize;

      uint32_t perm = 0;

      // Make a pass over the regions to collect permissions
      // based on virtual page address overlap:
      for (size_t rv = 0; rv < ei.region.size(); rv++) {
	ExecRegion &rgn = ei.region[rv];
	if (rgn.vAddrOverlaps(vaddr, pgvbound))
	  perm |= rgn.perm;
      }

      // This page needs to be inserted at /vaddr/ with permissions
      // /perm/:
      pgCap.restr =
        (((perm & ER_X) == 0) ? CAP_RESTR_NX : 0) |
        (((perm & ER_W) == 0) ? CAP_RESTR_RO : 0);

      addrSpace = 
	is.ci->AddSubSpaceToSpace(bank->cap, addrSpace, vaddr, pgCap);

      vaddr += is.ci->target.pageSize;
    }
  }

  imageEnv->addBinding("space", CapValue::make(is.ci, addrSpace));

  if(!contains(is.deps, file))
    is.deps.insert(file);

  return EnvValue::make(imageEnv);
}


shared_ptr<Value>
pf_mk_misccap(PrimFnValue& pfv,
		InterpState& is,
		vector<shared_ptr<Value> >& args)
{
  if (pfv.nm == "NullCap")
    return CapValue::make(is.ci, is.ci->CiCap());

  // Window, Background handled in pf_mk_window()
  if (pfv.nm == "CapBits")
    return CapValue::make(is.ci, is.ci->CiCap(ct_CapBits));

  if (pfv.nm == "Discrim")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Discrim));

  if (pfv.nm == "Range")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Range));

  if (pfv.nm == "Sleep")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Sleep));

  if (pfv.nm == "IrqCtl")
    return CapValue::make(is.ci, is.ci->CiCap(ct_IrqCtl));

  if (pfv.nm == "SchedCtl")
    return CapValue::make(is.ci, is.ci->CiCap(ct_SchedCtl));

  if (pfv.nm == "Checkpoint")
    return CapValue::make(is.ci, is.ci->CiCap(ct_Checkpoint));

  if (pfv.nm == "ObStore")
    return CapValue::make(is.ci, is.ci->CiCap(ct_ObStore));

  if (pfv.nm == "IoPerm")
    return CapValue::make(is.ci, is.ci->CiCap(ct_IOPriv));

  if (pfv.nm == "PinCtl")
    return CapValue::make(is.ci, is.ci->CiCap(ct_PinCtl));

  if (pfv.nm == "DevRangeCtl")
    return CapValue::make(is.ci, is.ci->CiCap(ct_DevRangeCtl));

  if (pfv.nm == "SysCtl")
    return CapValue::make(is.ci, is.ci->CiCap(ct_SysCtl));

  if (pfv.nm == "KernLog")
    return CapValue::make(is.ci, is.ci->CiCap(ct_KernLog));

  is.errStream << is.curAST->loc << " "
	       << "Buggered mkimage binding of \""
	       << pfv.nm << "\" in builtin.cxx.\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_mk_window(PrimFnValue& pfv,
	     InterpState& is,
	     vector<shared_ptr<Value> >& args)
{
  shared_ptr<IntValue> offset = needIntArg(is, pfv.nm, args, 1);

  if (pfv.nm == "Window") {
    shared_ptr<CapValue> cv = CapValue::make(is.ci, is.ci->CiCap(ct_Window));
    cv->cap.u2.offset = offset->bn.as_uint64();
    return cv;
  }

  if (pfv.nm == "LocalWindow") {
    shared_ptr<IntValue> slot = needIntArg(is, pfv.nm, args, 2);
    shared_ptr<CapValue> cv = CapValue::make(is.ci, is.ci->CiCap(ct_LocalWindow));
    cv->cap.u2.offset = offset->bn.as_uint64();
    cv->cap.allocCount = slot->bn.as_uint32();
    return cv;
  }

  is.errStream << is.curAST->loc << " "
	       << "Buggered mkimage binding of \""
	       << pfv.nm << "\" in builtin.cxx.\n";
  is.backtrace();
  THROW(excpt::BadValue, "Bad interpreter result");
}

shared_ptr<Value>
pf_mk_process(PrimFnValue& pfv,
	      InterpState& is,
	      vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocProcess(bank->cap));
}

shared_ptr<Value>
pf_mk_bank(PrimFnValue& pfv,
	      InterpState& is,
	      vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocBank(bank->cap));
}

shared_ptr<Value>
pf_mk_gpt(PrimFnValue& pfv,
	  InterpState& is,
	  vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocGPT(bank->cap));
}

shared_ptr<Value>
pf_mk_endpoint(PrimFnValue& pfv,
	       InterpState& is,
	       vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocEndpoint(bank->cap));
}

shared_ptr<Value>
pf_mk_page(PrimFnValue& pfv,
	   InterpState& is,
	   vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocPage(bank->cap));
}

shared_ptr<Value>
pf_mk_cappage(PrimFnValue& pfv,
	      InterpState& is,
	      vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  return CapValue::make(is.ci, is.ci->AllocCapPage(bank->cap));
}

shared_ptr<Value>
pf_mk_entry(PrimFnValue& pfv,
	    InterpState& is,
	    vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> cap = needCapArgType(is, pfv.nm, args, 0, ct_Endpoint, ct_Endpoint);
  shared_ptr<IntValue> pp = needIntArg(is, pfv.nm, args, 1);

  shared_ptr<CapValue> cv = CapValue::make(*cap);
  shared_ptr<CiEndpoint> ep = is.ci->GetEndpoint(cv->cap);

  cv->cap.type = ct_Entry;
  cv->cap.u1.protPayload = pp->bn.as_uint32();

  return cv;
}

shared_ptr<Value>
pf_mk_appint(PrimFnValue& pfv,
	    InterpState& is,
	    vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> cap = needCapArgType(is, pfv.nm, args, 0, ct_Endpoint, ct_Endpoint);
  shared_ptr<IntValue> maskBN = needIntArg(is, pfv.nm, args, 1);

  uint32_t mask = maskBN->bn.as_uint32();

  {
    shared_ptr<CapValue> cv = CapValue::make(*cap);
    shared_ptr<CiEndpoint> ep = is.ci->GetEndpoint(cv->cap);

    cv->cap.type = ct_AppNotice;
    cv->cap.u1.protPayload = mask;

    return cv;
  }
}

shared_ptr<Value>
pf_dup(PrimFnValue& pfv,
	InterpState& is,
	vector<shared_ptr<Value> >& args)
{
  shared_ptr<CapValue> bank = needBankArg(is, pfv.nm, args, 0);
  shared_ptr<CapValue> cap = needAnyCapArg(is, pfv.nm, args, 1);

  switch(cap->cap.type) {
  case ct_Endpoint:
    {
      shared_ptr<CiEndpoint> oldOb = is.ci->GetEndpoint(cap->cap);
      shared_ptr<CapValue> out = CapValue::make(*cap);
      capability tmp = is.ci->AllocEndpoint(bank->cap);
      shared_ptr<CiEndpoint> newOb = is.ci->GetEndpoint(tmp);

      memcpy(&newOb->v, &oldOb->v, sizeof(oldOb->v));
      out->cap.u2.oid = tmp.u2.oid;
      out->cap.allocCount = tmp.allocCount;
      return out;
    }
  case ct_Page:
    {
      shared_ptr<CiPage> oldOb = is.ci->GetPage(cap->cap);
      shared_ptr<CapValue> out = CapValue::make(*cap);
      capability tmp = is.ci->AllocPage(bank->cap);
      shared_ptr<CiPage> newOb = is.ci->GetPage(tmp);

      memcpy(newOb->data, oldOb->data, oldOb->pgSize);
      out->cap.u2.oid = tmp.u2.oid;
      out->cap.allocCount = tmp.allocCount;
      return out;
    }
  case ct_CapPage:
    {
      shared_ptr<CiCapPage> oldOb = is.ci->GetCapPage(cap->cap);
      shared_ptr<CapValue> out = CapValue::make(*cap);
      capability tmp = is.ci->AllocCapPage(bank->cap);
      shared_ptr<CiCapPage> newOb = is.ci->GetCapPage(tmp);

      memcpy(newOb->data, oldOb->data, oldOb->pgSize);
      out->cap.u2.oid = tmp.u2.oid;
      out->cap.allocCount = tmp.allocCount;
      return out;
    }
  case ct_GPT:
    {
      shared_ptr<CiGPT> oldOb = is.ci->GetGPT(cap->cap);
      shared_ptr<CapValue> out = CapValue::make(*cap);
      capability tmp = is.ci->AllocGPT(bank->cap);
      shared_ptr<CiGPT> newOb = is.ci->GetGPT(tmp);

      memcpy(&newOb->v, &oldOb->v, sizeof(oldOb->v));
      out->cap.u2.oid = tmp.u2.oid;
      out->cap.allocCount = tmp.allocCount;
      return out;
    }
  case ct_Process:
    {
      shared_ptr<CiProcess> oldOb = is.ci->GetProcess(cap->cap);
      shared_ptr<CapValue> out = CapValue::make(*cap);
      capability tmp = is.ci->AllocProcess(bank->cap);
      shared_ptr<CiProcess> newOb = is.ci->GetProcess(tmp);

      memcpy(&newOb->v, &oldOb->v, sizeof(oldOb->v));
      out->cap.u2.oid = tmp.u2.oid;
      out->cap.allocCount = tmp.allocCount;
      return out;
    }

  default:
    is.errStream << is.curAST->loc << " "
		 << pfv.nm
		 << "() requires a non-sender object capability as its "
		 << "argument.\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

shared_ptr<Value>
pf_fillpage(PrimFnValue& pfv,
	    InterpState& is,
	    vector<shared_ptr<Value> >& args)
{
  needCapArgType(is, pfv.nm, args, 0, ct_Page);
  shared_ptr<CapValue> pgCap = dynamic_pointer_cast<CapValue>(args[0]);
  uint32_t value =
    needIntArg(is, pfv.nm, args, 1)->bn.as_uint32();

  if (value > 255) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() value must be a legal byte value\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  shared_ptr<CiPage> pg = is.ci->GetPage(pgCap->cap);

  memset(pg->data, value, is.ci->target.pageSize);

  return Value::Unit;
}

shared_ptr<Value>
pf_set_page_uint64(PrimFnValue& pfv,
		   InterpState& is,
		   vector<shared_ptr<Value> >& args)
{
  needCapArgType(is, pfv.nm, args, 0, ct_Page);
  shared_ptr<CapValue> pgCap = dynamic_pointer_cast<CapValue>(args[0]);
  uint32_t addr =
    needIntArg(is, pfv.nm, args, 1)->bn.as_uint32();
  uint64_t value =
    needIntArg(is, pfv.nm, args, 2)->bn.as_uint64();

  size_t slot = addr / sizeof (value);

  if (slot >= (is.ci->target.pageSize / sizeof (value)) ||
      slot * sizeof (value) != addr) {
    is.errStream << is.curAST->loc << " "
		 << pfv.nm << "() addr must be an aligned page address value\n";
    is.backtrace();
    THROW(excpt::BadValue, "Bad interpreter result");
  }

  shared_ptr<CiPage> pg = is.ci->GetPage(pgCap->cap);

  if (is.ci->target.endian == LITTLE_ENDIAN) {
    size_t idx;
    for (idx = 0; idx < sizeof (value); idx++) {
      pg->data[addr + idx] = (value & (0xff));
      value >>= 8;
    }
  } else {
    size_t idx;
    for (idx = 0; idx < sizeof (value); idx++) {
      pg->data[addr + (sizeof (value) - 1) - idx] = (value & (0xff));
      value >>= 8;
    }
  }

  return Value::Unit;
}

shared_ptr<Value>
pf_set_user_console(PrimFnValue& pfv,
		    InterpState& is,
		    vector<shared_ptr<Value> >& args)
{
  shared_ptr<BoolValue> v_userConsole = needBoolArg(is, pfv.nm, args, 0);
  is.ci->haveUserConsole = v_userConsole->b;

  return Value::Unit;
}

shared_ptr<Environment<Value> > 
getBuiltinEnv(shared_ptr<CoyImage> ci)
{
  if (! builtins) {
    builtins = Environment<Value>::make();

    {
      // PRIME BANK INITIALIZATION:

      // Prime bank endpoint is well-known to be endpoint 0. See
      // CoyImage constructor and comment in obstore/CoyImgHdr.h
      shared_ptr<CapValue> pbep = CapValue::make(ci, ci->CiCap(ct_Endpoint, 0));
      shared_ptr<CapValue> pb = CapValue::make(*pbep);

      pb->cap.type = ct_Entry;
      pb->cap.u1.protPayload = CoyImage::FullStrengthBank;

      builtins->addConstant("PrimeBankEndpoint", pbep);
      builtins->addConstant("PrimeBank", pb);
    }

    // Manifest Constants:
    builtins->addConstant("true", BoolValue::True);
    builtins->addConstant("false", BoolValue::False);

    // CONSTRUCTORS FOR CAPABILITIES:

    // Object capability allocators:
    builtins->addConstant("PageCap",
			 PrimFnValue::make("PageCap", 1, 1, pf_mk_raw_obcap));

    builtins->addConstant("PhysPageCap",
			 PrimFnValue::make("PhysPageCap", 1, 1, pf_mk_raw_obcap));

    // Window capability fabrication:
    builtins->addConstant("Window", 
			 PrimFnValue::make("Window", 1, 1, pf_mk_window));

    builtins->addConstant("LocalWindow", 
			 PrimFnValue::make("LocalWindow", 2, 2, pf_mk_window));

#if 0
    // Bank fabrication
    builtins->addConstant("make_bank", 
			 PrimFnValue::make("make_bank", 1, 1, pf_mk_bank));

    // Cappage fabrication:
    builtins->addConstant("make_cappage", 
			 PrimFnValue::make("make_cappage", 1, 1, pf_mk_cappage));

    // Endpoint fabrication:
    builtins->addConstant("make_endpoint", 
			 PrimFnValue::make("make_endpoint", 1, 1, 
					 pf_mk_endpoint));

    // GPT fabrication:
    builtins->addConstant("make_gpt", 
			 PrimFnValue::make("make_gpt", 1, 1, pf_mk_gpt));

    // Page fabrication:
    builtins->addConstant("make_page", 
			 PrimFnValue::make("make_page", 1, 1, pf_mk_page));

    // Process fabrication:
    builtins->addConstant("make_process", 
			 PrimFnValue::make("make_process", 1, 1, pf_mk_process));
#endif

    // Sender fabrication:
    builtins->addConstant("enter",
			 PrimFnValue::make("enter", 2, 2, pf_mk_entry));

    // AppInt Sender fabrication:
    builtins->addConstant("AppInt",
			 PrimFnValue::make("AppInt", 2, 2, pf_mk_appint));

    // Misc capabilities
    builtins->addConstant("NullCap", 
			 PrimFnValue::make("NullCap", 0, 0, pf_mk_misccap));
    builtins->addConstant("CapBits", 
			 PrimFnValue::make("CapBits", 0, 0, pf_mk_misccap));
    builtins->addConstant("Discrim", 
			 PrimFnValue::make("Discrim", 0, 0, pf_mk_misccap));
    builtins->addConstant("Range", 
			 PrimFnValue::make("Range", 0, 0, pf_mk_misccap));
    builtins->addConstant("Sleep", 
			 PrimFnValue::make("Sleep", 0, 0, pf_mk_misccap));
    builtins->addConstant("IrqCtl", 
			 PrimFnValue::make("IrqCtl", 0, 0, pf_mk_misccap));
    builtins->addConstant("SchedCtl", 
			 PrimFnValue::make("SchedCtl", 0, 0, pf_mk_misccap));
    builtins->addConstant("Checkpoint", 
			 PrimFnValue::make("Checkpoint", 0, 0, pf_mk_misccap));
    builtins->addConstant("ObStore", 
			 PrimFnValue::make("ObStore", 0, 0, pf_mk_misccap));
    builtins->addConstant("IoPerm", 
			 PrimFnValue::make("IoPerm", 0, 0, pf_mk_misccap));
    builtins->addConstant("PinCtl", 
			 PrimFnValue::make("PinCtl", 0, 0, pf_mk_misccap));
    builtins->addConstant("SysCtl", 
			 PrimFnValue::make("SysCtl", 0, 0, pf_mk_misccap));
    builtins->addConstant("DevRangeCtl", 
			 PrimFnValue::make("DevRangeCtl", 0, 0, pf_mk_misccap));
    builtins->addConstant("KernLog", 
			 PrimFnValue::make("KernLog", 0, 0, pf_mk_misccap));

    // CAPABILITY TRANSFORMERS:

    builtins->addConstant("readonly", 
			 PrimFnValue::make("readonly", 1, 1, pf_downgrade));
    builtins->addConstant("weaken", 
			 PrimFnValue::make("weaken", 1, 1, pf_downgrade));
    builtins->addConstant("noexec", 
			 PrimFnValue::make("noexec", 1, 1, pf_downgrade));

    builtins->addConstant("opaque", 
			 PrimFnValue::make("opaque", 1, 1, pf_downgrade));

    builtins->addConstant("nocall", 
			 PrimFnValue::make("nocall", 1, 1, pf_downgrade));

    builtins->addConstant("nocache", 
			 PrimFnValue::make("nocache", 1, 1, pf_downgrade));

#if 0
    builtins->addConstant("writethrough", 
			 PrimFnValue::make("writethrough", 1, 1, pf_downgrade));
#endif

    builtins->addConstant("guard", 
			 PrimFnValue::make("guard", 2, 3, pf_guard_cap));

    // CAPABILITY ACCESSORS:

    builtins->addConstant("get_l2g", 
			 PrimFnValue::make("get_l2g", 1, 1, pf_memcap_ops));
    builtins->addConstant("get_match", 
			 PrimFnValue::make("get_match", 1, 1, pf_memcap_ops));

    // UTILITY FUNCTIONS

    builtins->addConstant("fatal", 
			 PrimFnValue::make("fatal", 1, 1, pf_fatal));

    builtins->addConstant("dump_usage", 
			 PrimFnValue::make("dump_usage", 0, 0, pf_dump_usage));

    builtins->addConstant("doprint", 
			 PrimFnValue::make("doprint", 1, 0, pf_doprint));

    builtins->addConstant("print_space", 
			 PrimFnValue::make("print_space", 1, 1, pf_doprint_space));

    builtins->addConstant("print_tree", 
			 PrimFnValue::make("print_tree", 1, 1, pf_doprint_tree));

    builtins->addConstant("guarded_space", 
			 PrimFnValue::make("guarded_space", 3, 4, pf_guarded_space));

    builtins->addConstant("insert_subspace", 
			 PrimFnValue::make("insert_subspace", 4, 5, pf_insert_subspace));

    builtins->addConstant("lookup_addr",
			  PrimFnValue::make("lookup_addr", 2, 2, pf_lookup_addr));
    builtins->addConstant("readfile", 
			 PrimFnValue::make("readfile", 2, 3, pf_readfile));

    builtins->addConstant("loadimage", 
			 PrimFnValue::make("loadimage", 2, 2, pf_loadimage));

    builtins->addConstant("dup", 
			 PrimFnValue::make("dup", 2, 2, pf_dup));

    // Page content manipulation
    builtins->addConstant("fillpage",
			 PrimFnValue::make("fillpage", 2, 2, pf_fillpage));

    builtins->addConstant("set_page_uint64",
			 PrimFnValue::make("set_page_uint64", 3, 3, 
					 pf_set_page_uint64));

    // Advise kernel that there is a user-mode console driver:
    builtins->addConstant("set_user_console", 
			 PrimFnValue::make("set_user_console", 1, 1, pf_set_user_console));
    {
      // Environment for storage object constructor capabilities:
      shared_ptr<Environment<Value> > ctors = Environment<Value>::make();

      // Bank fabrication
      ctors->addConstant("Bank", 
			 PrimFnValue::make("make_bank", 1, 1, pf_mk_bank));

      // Cappage fabrication:
      ctors->addConstant("CapPage", 
			 PrimFnValue::make("make_cappage", 1, 1, pf_mk_cappage));

      // GPT fabrication:
      ctors->addConstant("GPT", 
			 PrimFnValue::make("make_gpt", 1, 1, pf_mk_gpt));

      // Endpoint fabrication:
      ctors->addConstant("Endpoint", 
			 PrimFnValue::make("make_endpoint", 1, 1, 
					 pf_mk_endpoint));

      // Page fabrication:
      ctors->addConstant("Page", 
			 PrimFnValue::make("make_page", 1, 1, pf_mk_page));

      // Process fabrication:
      ctors->addConstant("Process", 
			 PrimFnValue::make("make_process", 1, 1, pf_mk_process));



      builtins->addConstant("#new", EnvValue::make(ctors));
    }

    {
      // Environment for passed-through command line options
      shared_ptr<Environment<Value> > opts = Environment<Value>::make();

      // --diag
      opts->addConstant("diag", 
			BoolValue::make(Options::showDiag));

      builtins->addConstant("opts", EnvValue::make(opts));
    }


#if 0
    {
      // Environment for miscellaneous capabilities:
      shared_ptr<Environment<Value> > caps = Environment<Value>::make;

      caps->addConstant("null", CapValue::make(ci, ci->CiCap()));
      caps->addConstant("capbits", CapValue::make(ci, ci->CiCap(ct_CapBits)));
      caps->addConstant("discrim", CapValue::make(ci, ci->CiCap(ct_Discrim)));
      caps->addConstant("range", CapValue::make(ci, ci->CiCap(ct_Range)));
      caps->addConstant("sleep", CapValue::make(ci, ci->CiCap(ct_Sleep)));
      caps->addConstant("irqctl", CapValue::make(ci, ci->CiCap(ct_IRQCtl)));
      caps->addConstant("schedctl", CapValue::make(ci, ci->CiCap(ct_SchedCtl)));
      caps->addConstant("checkpoint", CapValue::make(ci, ci->CiCap(ct_Checkpoint)));
      caps->addConstant("obstore", CapValue::make(ci, ci->CiCap(ct_ObStore)));
      caps->addConstant("ioperm", CapValue::make(ci, ci->CiCap(ct_IoPerm)));
      caps->addConstant("pinctl", CapValue::make(ci, ci->CiCap(ct_PinCtl)));
      caps->addConstant("devrangectl", CapValue::make(ci, ci->CiCap(ct_DevRangeCtl)));

      builtins->addConstant("coyotos", EnvValue::make(caps));
    }
#endif

    // Arithmetic and comparison operators:

    builtins->addPureConstant("+", 
			      PrimFnValue::make("+", 1, 0, pf_add));
    builtins->addPureConstant("-", 
			      PrimFnValue::make("-", 1, 0, pf_add));
    builtins->addPureConstant("*", 
			      PrimFnValue::make("*", 1, 0, pf_mul));
    builtins->addPureConstant("/", 
			      PrimFnValue::make("/", 1, 0, pf_div));
    builtins->addPureConstant("%", 
			      PrimFnValue::make("%", 1, 0, pf_mod));
    builtins->addPureConstant("unary-", 
			      PrimFnValue::make("-", 1, 0, pf_negate));

    builtins->addPureConstant("<", 
			      PrimFnValue::make("<", 1, 0, pf_cmp));
    builtins->addPureConstant(">", 
			      PrimFnValue::make(">", 1, 0, pf_cmp));

    builtins->addPureConstant("<=", 
			      PrimFnValue::make("<=", 1, 0, pf_cmpe));
    builtins->addPureConstant(">=", 
			      PrimFnValue::make(">=", 1, 0, pf_cmpe));
    builtins->addPureConstant("==", 
			      PrimFnValue::make("==", 1, 0, pf_cmpe));
    builtins->addPureConstant("!=", 
			      PrimFnValue::make("!=", 1, 0, pf_cmpe));
    builtins->addPureConstant("!", 
			      PrimFnValue::make("!", 1, 0, pf_not));

  }

  return builtins;
}
