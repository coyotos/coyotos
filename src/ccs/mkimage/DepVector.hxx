#ifndef DEPVECTOR_HXX
#define DEPVECTOR_HXX

/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <set>
#include <boost/filesystem/path.hpp>

// This might be more efficient if it were a std::map onto bool
typedef std::set< boost::filesystem::path > DepSet;
void emit_deps_to(DepSet& deps, std::ostream& s);

static inline bool
contains(DepSet& v, const boost::filesystem::path& p)
{
  if(v.find(p) == DepSet::iterator())
    return false;
  return true;
}


#endif /* DEPVECTOR_HXX */
