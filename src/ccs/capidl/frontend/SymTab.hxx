#ifndef SYMTAB_HXX
#define SYMTAB_HXX

/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <libsherpa/BigNum.hxx>
#include <libsherpa/LToken.hxx>
#include "DocComment.hxx"
#include "ArchInfo.hxx"

using namespace sherpa;

/*
 * Token type structure. Using a structure for this is a quite
 * amazingly bad idea, but using a union makes the C++ constructor
 * logic unhappy....
 */

enum LitType {			/* literal type */
  lt_void,
  lt_integer,
  lt_unsigned,
  lt_float,
  lt_char,
  lt_bool,
  lt_string,
};

struct LitValue {
  sherpa::BigNum  bn;		/* large precision integers */
  double          d;		/* doubles, floats */
  LitType	  lty;
  /* no special field for lt_string, as the name is the literal */
};

/* There is a design issue hiding here: how symbolic should the output
 * of the IDL compiler be? I think the correct answer is "very", in
 * which case we may need to do some tail chasing for constants. Thus,
 * I consider a computed constant value to be a symbol.
 */

enum SymClass {
#define SYMCLASS(pn, x,n) sc_##x,
#include "symclass.def"
#undef  SYMCLASS
};

#define SYMDEBUG

#define SF_NOSTUB     0x1u
#define SF_NO_OPCODE  0x2u

struct Symbol;

typedef std::vector<boost::shared_ptr<Symbol> > SymVec;

static inline bool
contains(SymVec sv, const boost::shared_ptr<Symbol>& s) {
  for (SymVec::iterator it = sv.begin();
       it != sv.end(); it++) {
    if (*it == s)
      return true;
  }
  return false;
}

struct Symbol : public boost::enable_shared_from_this<Symbol> {
private:
  static const char *sc_names[];
  static const char *sc_printnames[];
  static unsigned sc_isScope[];

public:
  static boost::shared_ptr<Symbol> CurScope;
  static boost::shared_ptr<Symbol> VoidType;
  static boost::shared_ptr<Symbol> UniversalScope;
  static boost::shared_ptr<Symbol> KeywordScope;


  sherpa::LexLoc loc;
  std::string    name;
#ifdef SYMDEBUG
  std::string    qualifiedName;
#endif
  boost::shared_ptr<DocComment>  docComment;

  bool           mark;		/* used for circularity detection */
  bool           isActiveUOC;	/* created in an active unit of compilation */

  boost::shared_ptr<Symbol>  nameSpace;	/* containing namespace */
  SymVec         children;	/* members of the scope */
  SymVec         raises;	/* exceptions raised by this method/interface */
  SymClass       cls;

  boost::shared_ptr<Symbol>  type;		/* type of an identifier */
  boost::shared_ptr<Symbol>  baseType;	/* base type, if extension */
  boost::shared_ptr<Symbol>  value;		/* value of a constant */
				  
  bool           complete;	/* used for top symbols only. */
  unsigned       ifDepth;	/* depth of inheritance tree */

  unsigned       flags;		/* flags that govern code generation */

  LitValue       v;		/* only for sc_value and sc_builtin */

  /* Using a left-recursive grammar is always really messy, because
   * you have to do the scope push/pop by hand, which is at best
   * messy. Bother.
   */

private:
  Symbol();

  static boost::shared_ptr<Symbol> Construct(const sherpa::LToken& tok, bool isActiveUOC, SymClass);

  inline void
  AddChild(boost::shared_ptr<Symbol> sym)
  {
    children.push_back(sym);
  }


public:
  /* Creates a new symbol of the specified type in the currently
   * active scope. This is not a constructor because it must be able
   * to return failure in the event of a symbol name collision. */
  static boost::shared_ptr<Symbol> 
  Create_inScope(const sherpa::LToken& nm, 
		 bool isActiveUOC, SymClass, 
		 boost::shared_ptr<Symbol> inScope);

  static boost::shared_ptr<Symbol> 
  Create(const sherpa::LToken& nm, bool isActiveUOC, SymClass);

  static boost::shared_ptr<Symbol> 
  CreatePackage(const sherpa::LToken& nm, boost::shared_ptr<Symbol> inPkg);

  static boost::shared_ptr<Symbol> 
  CreateRef(const sherpa::LToken& nm, bool isActiveUOC);

  static boost::shared_ptr<Symbol> 
  CreateRef_inScope(const sherpa::LToken& nm, 
		    bool isActiveUOC, 
		    boost::shared_ptr<Symbol>  inScope);

  static boost::shared_ptr<Symbol> 
  CreateRaisesRef(const sherpa::LToken& nm, bool isActiveUOC);

  static boost::shared_ptr<Symbol> 
  CreateRaisesRef_inScope(const sherpa::LToken& nm, 
			  bool isActiveUOC, 
			  boost::shared_ptr<Symbol> inScope);

  static boost::shared_ptr<Symbol> 
  GenSym(SymClass, bool isActiveUOC);

  static boost::shared_ptr<Symbol> 
  GenSym_inScope(SymClass, bool isActiveUOC, boost::shared_ptr<Symbol> inScope);
  
  static boost::shared_ptr<Symbol> 
  GenSym_withoutScope(SymClass sc, bool isActiveUOC) {
    return GenSym_inScope(sc, isActiveUOC, boost::shared_ptr<Symbol>());
  }
  
  static boost::shared_ptr<Symbol> 
  MakeKeyword(const std::string& nm, SymClass sc, LitType lt, unsigned value);

  static boost::shared_ptr<Symbol> MakeIntLitFromBigNum(const sherpa::LexLoc& loc, 
				      const sherpa::BigNum&);
  static boost::shared_ptr<Symbol> MakeIntLit(const sherpa::LToken&);
  static boost::shared_ptr<Symbol> MakeStringLit(const sherpa::LToken&);
  static boost::shared_ptr<Symbol> MakeCharLit(const sherpa::LToken&);
  static boost::shared_ptr<Symbol> MakeFloatLit(const sherpa::LToken&);
  static boost::shared_ptr<Symbol> MakeExprNode(const sherpa::LToken& op, 
			      boost::shared_ptr<Symbol> left,
			      boost::shared_ptr<Symbol> right);

  /* Following, when applied to scalar types (currently only integer
   * types) will produce min and max values. */
  static boost::shared_ptr<Symbol> MakeMaxLit(const sherpa::LexLoc& loc, boost::shared_ptr<Symbol> );
  static boost::shared_ptr<Symbol> MakeMinLit(const sherpa::LexLoc& loc, boost::shared_ptr<Symbol> );

  static void PushScope(boost::shared_ptr<Symbol> newScope);
  static void PopScope();

  static void InitSymtab();
  static boost::shared_ptr<Symbol> FindPackageScope();

  void ClearAllMarks();

  unsigned CountChildren(bool (Symbol::*predicate)());
  boost::shared_ptr<Symbol> LookupChild(const std::string& name, boost::shared_ptr<Symbol> bound);

  inline boost::shared_ptr<Symbol> LookupChild(const std::string& name) {
    return LookupChild(name, boost::shared_ptr<Symbol>());
  }

  boost::shared_ptr<Symbol> LexicalLookup(const std::string& name, boost::shared_ptr<Symbol> bound);
  inline
  boost::shared_ptr<Symbol> LexicalLookup(const std::string& name) {
    return LexicalLookup(name, boost::shared_ptr<Symbol>());
  }
  boost::shared_ptr<Symbol> LookupChild(const sherpa::LToken& tok, boost::shared_ptr<Symbol> bound)
  {
    return LookupChild(tok.str, bound);
  }
  boost::shared_ptr<Symbol> LookupChild(const sherpa::LToken& tok)
  {
    return LookupChild(tok.str);
  }
  boost::shared_ptr<Symbol> LexicalLookup(const sherpa::LToken& tok, boost::shared_ptr<Symbol> bound)
  {
    return LexicalLookup(tok.str, bound);
  }

  bool ResolveSymbolReference();
  bool ResolveInheritedReference();

#ifdef SYMDEBUG
  void QualifyNames();
#else
  inline void QualifyNames()
{
}
#endif
  bool ResolveReferences();
  void ResolveIfDepth();
  bool TypeCheck();

  bool IsLinearizable();
  bool IsFixedSerializable();
  bool IsDirectSerializable();

  void DoComputeDependencies(SymVec&);
  void ComputeDependencies(SymVec&);
#if 0
  void ComputeTransDependencies(SymVec&);
#endif

  unsigned alignof(const ArchInfo&);
  unsigned directSize(const ArchInfo&);
  unsigned indirectSize(const ArchInfo&);

  boost::shared_ptr<Symbol> UnitOfCompilation();

  /* Return TRUE iff the passed symbol is itself (directly) a type
     symbol. Note that if you update this list you should also update it
     in the switch in symbol_ResolveType() */
  inline bool IsBasicType()
  {
    if (this == NULL)
      return 0;
  
    return (cls == sc_primtype ||
	    cls == sc_enum ||
	    cls == sc_struct ||
	    cls == sc_union ||
	    cls == sc_interface ||
	    cls == sc_absinterface ||
	    cls == sc_seqType ||
	    cls == sc_bufType ||
	    cls == sc_arrayType);
  }

  /* Chase down symrefs to find the actual defining reference of a symbol */
  inline boost::shared_ptr<Symbol> ResolveRef()
  {
    boost::shared_ptr<Symbol> sym = shared_from_this();

    while (sym && sym->cls == sc_symRef)
      sym = sym->value;

    return sym;
  }

  boost::shared_ptr<Symbol> ResolvePackage();

  /* Assuming that the passed symbol is a type symbol, chase through
     symrefs and typedefs to find the actual type. */
  boost::shared_ptr<Symbol> ResolveType();

  /* Return TRUE iff the symbol is a type symbol */
  inline bool IsTypeSymbol()
  {
    boost::shared_ptr<Symbol> sym = ResolveRef();
  
    return sym->IsBasicType() || (sym->cls == sc_typedef);
  }

  /* Return TRUE iff the symbol is of the provided type */
  inline bool IsType(boost::shared_ptr<Symbol> sym, SymClass sc)
  {
    boost::shared_ptr<Symbol> typeSym = ResolveType();
    if (!typeSym)
      return false;

    return (typeSym->cls == sc);
  }

  inline bool IsConstantSymbol()
  {
    return (cls == sc_const);
  }

  inline bool IsEnumSymbol()
  {
    return (cls == sc_enum);
  }

  /* Return TRUE iff the symbol, after resolving name references, is a
     typedef. Certain parameter types are legal, but only if they appear
     in typedefed form. */
  inline bool IsTypedef()
  {
    boost::shared_ptr<Symbol> sym = ResolveRef();
    if (!sym)
      return false;
    return (sym->cls == sc_typedef);
  }

#if 0
  /* Return TRUE iff the type of this symbol is some sort of aggregate
     type. */
  inline bool IsAggregateType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;

    return (sym->cls == sc_struct ||
	    sym->cls == sc_union ||
	    sym->cls == sc_bufType ||
	    sym->cls == sc_seqType ||
	    sym->cls == sc_arrayType);
  }
#endif

  inline bool IsValidParamType()
  {
    boost::shared_ptr<Symbol> typeSym = ResolveType();
    
    if (!typeSym) return false;

    /* Sequences, Buffers, are invalid, but typedefs of these are okay. */
    if (typeSym->cls == sc_seqType || typeSym->cls == sc_bufType)
      return IsTypedef();

    return typeSym->IsBasicType();
  }

  inline bool IsValidMemberType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();

    if (!sym)
      return false;

    /* Buffers and typdefs of buffers are disallowed. */
    if (sym->cls == sc_bufType)
      return false;

    return sym->IsBasicType();
  }

  inline bool IsValidSequenceBaseType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();

    if (!sym) return false;

    /* Variable number of client preallocated buffers makes no sense. */
    if (sym->cls == sc_bufType)
      return false;

    /* Temporary restriction: we do not allow sequences of
       sequences. This is an implementation restriction, not a
       fundamental problem. */
    if (sym->cls == sc_arrayType || sym->cls == sc_seqType)
      return false;
  
    return sym->IsBasicType();
  }

  inline bool IsValidBufferBaseType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();

    if (!sym) return false;

    /* Temporary restriction: we do not allow sequences of
       sequences. This is an implementation restriction, not a
       fundamental problem. */
    if (sym->cls == sc_arrayType || sym->cls == sc_seqType ||
	sym->cls == sc_bufType)
      return false;

    return sym->IsBasicType();
  }

  inline bool IsSequenceType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
  
    if (!sym) return false;

    return (sym->cls == sc_seqType);
  }

  inline bool IsBufferType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
  
    if (!sym) return false;

    return (sym->cls == sc_bufType);
  }

  inline bool IsFixSequenceType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;
  
    return sym->cls == sc_arrayType;
  }

  inline bool IsConstantValue()
  {
    boost::shared_ptr<Symbol> sym = ResolveRef();
    if (!sym) return false;
  
    return (sym->cls == sc_const ||
	    sym->cls == sc_value ||
	    sym->cls == sc_arithop);
  }

  inline bool IsInterface()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;

    return (sym->cls == sc_interface ||
	    sym->cls == sc_absinterface);
  }

  inline bool IsOperation()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;

    return (sym->cls == sc_operation);
  }

  inline bool IsException()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;

    return (sym->cls == sc_exception);
  }

  /* Return TRUE if the symbol is a type that is passed by reference 
     rather than by copy. */
  inline bool IsReferenceType()
  {
    return IsInterface();
  }

  inline bool IsVoidType()
  {
    boost::shared_ptr<Symbol> sym = ResolveType();
    if (!sym) return false;
     
    if (sym->cls != sc_primtype)
      return false;

    return (sym->v.lty == lt_void);
  }

  inline bool IsAnonymous()
  {
    return (name[0] == '#');
  }

  std::string QualifiedName(char sep);

  unsigned long long CodedName();

  inline std::string ClassName()
  {
    return sc_names[cls];
  }
  inline std::string ClassPrintName()
  {
    return sc_printnames[cls];
  }
  inline bool IsScope() const
  {
    return sc_isScope[cls] ? true : false;
  }

  /* sorting helpers */
  static int
  CompareByName(const boost::shared_ptr<Symbol>& s1, 
		const boost::shared_ptr<Symbol>& s2);

  static int
  CompareByQualifiedName(const boost::shared_ptr<Symbol>& s1, 
			 const boost::shared_ptr<Symbol>& s2);
};

std::string symname_join(const std::string& n1, const std::string& n2, char sep);

#endif /* SYMTAB_HXX */
