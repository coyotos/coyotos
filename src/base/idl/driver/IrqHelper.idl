package coyotos.driver;

/**
 * @brief Interrupt demultiplexing helper application.
 *
 * The interrupt helper is a small process that waits for an interrupt
 * and posts a notice to an interested client program. It exists
 * primarily because the kernel has no means to perform a general
 * up-call.
 *
 * The helper executes in one of two states. In ``loop'' state, it
 * waits for an interrupt and calls the callback capability. This
 * state is exited when the callback invocation result is an
 * exception. It also ends after the callback returns if the wait loop
 * has been directed to run only once via waitOnce().
 *
 * In ``control'' state, it receives on the IrqHelper interface and
 * can be instructed to use a different callback capability or
 * re-start the loop state.
 *
 * A flaw in this design is that there is no straightforward way for
 * an outside party to determine which state the IrqHelper is in. One
 * way to @em force the IrqHelper to enter the control state is to
 * nullify the process capability in its callback endpoint.
 *
 * A second flaw in this design is that a callback failure may result
 * in a lost interrupt.
 */
interface IrqHelper extends coyotos.Cap {

  /** @brief Provide the helper with a notification callback
   * capability that should be called from the wait-and-notify loop.
   */
  void setCallback(IrqCallback callbackCap);

  /** @brief Return true iff an interrupt callback is pending.
   *
   * This will return true if an interrupt wait has completed and the
   * associated callback generated an exception. If runWaitLoop() or
   * waitOnce() are called while a delivery is pending, they will
   * re-try the callback.
   */
  boolean isDeliveryPending();

  /** @brief If an interrupt delivery is pending, cancel it, returning
   * true iff cancelation occurred.
   *
   * It is the responsibility of the caller not to lose track of
   * pending interrupts.
   */
  boolean cancelPendingDelivery();

  /** @brief Start the wait and notify loop, continuing until
   * otherwise instructed by the notification response.
   *
   * The IrqHelper returns immediately from this request, and then
   * enters a loop in which it waits for its assigned interrupt, calls
   * the callback capability, and repeats. If the callback generates
   * an exception, the wait loop is terminated and the helper
   * re-enters the control state.
   *
   * Raises cap.RequestError exception if interrupt is not initialized
   * or callback capability is not set.
   */
  void runWaitLoop();

  /** @brief Execute the wait and notify loop exactly once. */
  void waitOnce();
};
