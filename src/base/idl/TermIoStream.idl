package coyotos;

/**
 * @brief Bi-directional I/O stream interface for serial streams.
 *
 * The intent of this interface is to follow the TERMIOS specification
 * of the Single UNIX Specification, Version 3, which can be found
 * <link href="http://www.opengroup.org/onlinepubs/009695399/basedefs/xbd_chap11.html">here</link>.
 * (free to read, registration required).
 */
interface TermIoStream extends IoStream {

  typedef unsigned long tcflag_t;
  typedef unsigned long speed_t;

  const unsigned long NCCS = 32;

  struct termios {
    tcflag_t c_iflag;
    tcflag_t c_oflag;
    tcflag_t c_cflag;
    tcflag_t c_lflag;
    unsigned short c_line;
    array<unsigned short,NCCS> c_cc;
    speed_t c_ispeed;
    speed_t c_ospeed;
  };

  unsigned long bitset iflag {
    IGNBRK  = 1,
    BRKINT  = 2,
    IGNPAR  = 4,
    PARMRK  = 8,
    INPCK   = 16,
    ISTRIP  = 32,
    INLCR   = 64,
    IGNCR   = 128,
    ICRNL   = 256,
    IUCLC   = 512,
    IXON    = 1024,
    IXANY   = 2048,
    IXOFF   = 4096,
    IMAXBEL = 8192
  };

  unsigned long bitset oflag {
    OPOST  =        1,
    OLCUC  =        2,
    ONLCR  =        4,
    OCRNL  =        8,
    ONOCR  =       16,
    ONLRET =       32,
    OFILL  =       64,
    OFDEF  =      128,
    NLDLY  =      256,
    NL0    =      512,
    NL1    =     1024,
    CRDLY  =     2048,
    CR0    =     4096,
    CR1    =     8192,
    CR2    =    16384,
    CR3    =    32768,
    TABDLY =    65536,
    TAB0   =   131072,
    TAB1   =   262144,
    TAB2   =   524288,
    TAB3   =  1048576,
    BSDLY  =  2097152,
    BS0    =  4194304,
    BS1    =  8388608,
    FFDLY  = 16777216,
    FF0    = 33554432,
    FF1    = 67108864
  };

  unsigned long bitset cflag {
    B0       =            0,
    B50      =            1,
    B75      =            2,
    B110     =            3,
    B134     =            4,
    B150     =            5,
    B200     =            6,
    B300     =            7,
    B600     =            8,
    B1200    =            9,
    B1800    =           10,
    B2400    =           11,
    B4800    =           12,
    B9600    =           13,
    B19200   =           14,
    B38400   =           15,
    B57600   =           16,
    B115200  =           17,
    B230400  =           18,
    B460800  =           19,
    B500000  =           20,
    B576000  =           21,
    B921600  =           22,
    B1000000 =           23,
    B1152000 =           24,
    B1500000 =           25,
    B2000000 =           26,
    B2500000 =           27,
    B3000000 =           28,
    B3500000 =           29,
    B4000000 =           30,
    MAX_BAUD = B4000000,
    CSIZE  =      64,
    CS5    =       0,
    CS6    =      32,
    CS7    =      40,
    CS8    =      48,
    CSTOPB =     128,
    CREAD  =     256,
    PARENB =     512,
    PARODD =    1024,
    HUPCL  =    2048,
    CLOCAL =    4096
  };

  unsigned long bitset lflag {
    ISIG    =        1,
    ICANON  =        2,
    ECHO    =        4,
    ECHOE   =        8,
    ECHONL  =       16,
    NOFLSH  =       32,
    TOSTOP  =       64,
    ECHOCTL =      128,
    ECHOPRT =      256,
    ECHOKE  =      512,
    FLUSHO  =     1024,
    PENDIN  =     2048,
    ECHOK   =     4096
  };

  /* c_cc indices */
  unsigned short enum cc {
    VINTR    =  0,
    VQUIT    =  1,
    VERASE   =  2,
    VKILL    =  3,
    VEOF     =  4,
    VTIME    =  5,
    VSTART   =  8,
    VSTOP    =  9,
    VEOL     = 11,
    VREPRINT = 12,
    VWERASE  = 14,
    VEOL2    = 16
  };

  void makeraw();
  void makecooked();
  void setattr(termios t);
  termios getattr();
};
