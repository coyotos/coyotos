/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <stdlib.h>
#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/CapBits.h>
#include <coyotos/runtime.h>
#include <coyotos/kprintf.h>

#define CR_PRIMEBANK    CR_APP(0)
#define CR_LOG          CR_APP(1)
#define CR_CAPBITS      CR_APP(2)
#define CR_SUBBANK      CR_APP(3)
#define CR_TMP(x)       CR_APP(4 + (x))

#define unless(x) if (!(x))

#define FAIL (void)(*(uint32_t *)0 = __LINE__)

#define CHECK(expr)                                                     \
  do {                                                                  \
    if (!(expr)) {                                                        \
      kprintf(CR_LOG, "%s:%d: FAIL %s, except 0x%x%x\n",                \
        __FILE__, __LINE__, #expr,                                      \
        (int)(IDL_exceptCode >> 32), (int)IDL_exceptCode);              \
      for(;;); \
    } \
  } while (0)


static unsigned my_rand(void)
{
     static unsigned seed = 1;
     return rand_r(&seed);
}

static coyotos_Range_obType
randomType(void)
{
     return my_rand() % coyotos_Range_obType_otNUM_TYPES;
}

int
main(int argc, char *argv[])
{
  int loops = 20;
  int idx;
  for (idx = 0; idx < loops; idx++) {
    
    kprintf(CR_LOG, "INFO: PASS %d: Allocating child bank", idx);

    CHECK(coyotos_SpaceBank_createChild(CR_PRIMEBANK, CR_SUBBANK));
    coyotos_Range_obType t1 = randomType();
    coyotos_Range_obType t2 = randomType();
    coyotos_Range_obType t3 = randomType();
    
    kprintf(CR_LOG, "INFO: PASS %d: allocating %d, %d, and %d\n", 
	    idx, t1, t2, t3);

    CHECK(coyotos_SpaceBank_alloc(CR_SUBBANK,
				  t1, t2, t3,
				  CR_TMP(0), CR_TMP(1), CR_TMP(2)));
    
#if 0
    /* Useful for bring-up debugging */
    {
      size_t cndx;
      for (cndx = 0; cndx <= 2; cndx++) {
	coyotos_CapBits_info bits = { {0, 0, 0, 0} };
	CHECK(coyotos_CapBits_get(CR_CAPBITS,
				  CR_TMP(cndx), &bits));
	kprintf(CR_LOG, "tmp%d: 0x%08x 0x%0x 0x%x\n",
		cndx,
		bits.w[0], bits.w[1], bits.w[2], bits.w[3]);
      }
    }
#endif

    // free 1, 2, or 3 of the objects
    int count = (my_rand() % 3) + 1;
    
    kprintf(CR_LOG, "INFO: PASS %d: freeing %d objects, types %d %d %d\n",
	    idx, count, t1, t2, t3);
    CHECK(coyotos_SpaceBank_free(CR_SUBBANK,
				 count,
				 CR_TMP(0), CR_TMP(1), CR_TMP(2)));

    kprintf(CR_LOG, "INFO: PASS %d: Destroying bank", idx);
    // destroy the bank and all outstanding objects
    CHECK(coyotos_Cap_destroy(CR_SUBBANK));
  }

  kprintf(CR_LOG, "PASS: Completed %d passes, shooting self in head\n", idx);

  CHECK(coyotos_SpaceBank_destroyBankAndReturn(CR_SPACEBANK,
                                               CR_NULL,
                                               RC_coyotos_Cap_OK));
  for(;;) ;
  return 0;
}

