/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <stdlib.h>
#include <coyotos/runtime.h>
#include <coyotos/kprintf.h>

#define CR_LOG          CR_APP(0)

char *sbrk(size_t);

int
main(int argc, char *argv[])
{
  size_t bytes = 0;
  while (bytes < 4096 * 1024 && malloc(4096) != 0) {
    bytes += 4096;
    if ((bytes % (1024*1024)) == 0) {
      uint32_t mbytes = bytes / (1024*1024);
      kprintf(CR_LOG, "INFO: allocated %d mbytes\n", mbytes);
    }
  }
  sbrk(0);

  kprintf(CR_LOG, "PASS: Completed heap growth and sbrk(0)\n");

  for(;;) ;
  return 0;
}

