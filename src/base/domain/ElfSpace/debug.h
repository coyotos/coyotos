/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef DEBUG_H__
#define DEBUG_H__

#define dbg_all ~0u

#define dbg_init        0x1u
#define dbg_fault       0x2u
#define dbg_fail        0x4u
#define dbg_l2calc      0x8u

/** Change the setting of dbg_flags to determine what debugging
 * diagnostics actually emerge:
 */
#define dbg_flags (0u)

#define DEBUG_CND(x) ((dbg_flags) & (dbg_ ## x))
#define DEBUG(x) if (DEBUG_CND(x))

#endif /* DEBUG_H__ */
