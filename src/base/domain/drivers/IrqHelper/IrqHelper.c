/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Implements the following interfaces:
    coyotos.driver.IrqHelper
    coyotos.driver.IrqHelperCtl
 */

#include <coyotos/capidl.h>
#include <coyotos/syscall.h>
#include <coyotos/runtime.h>
#include <coyotos/reply_create.h>

#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/Endpoint.h>
#include <idl/coyotos/AddressSpace.h>
#include <idl/coyotos/IrqCtl.h>
#include <idl/coyotos/driver/IrqCallback.h>

/* Utility quasi-syntax */
#define unless(x) if (!(x))

#include "mki/coyotos.driver.IrqHelper.h"

#include <idl/coyotos/driver/IrqHelper.server.h>
#include <idl/coyotos/driver/IrqHelperCtl.server.h>

typedef union {
  _IDL_IFUNION_coyotos_driver_IrqHelper
      coyotos_driver_IrqHelper;
  _IDL_IFUNION_coyotos_driver_IrqHelperCtl
      coyotos_driver_IrqHelperCtl;
  InvParameterBlock_t pb;
  InvExceptionParameterBlock_t except;
  uintptr_t icw;
} _IDL_GRAND_SERVER_UNION;


#define CR_IRQCTL	 coyotos_driver_IrqHelper_APP_IRQCTL
#define CR_LOG		 coyotos_driver_IrqHelper_APP_LOG
#define CR_Helper	 coyotos_driver_IrqHelper_APP_Helper
#define CR_CallBack	 coyotos_driver_IrqHelper_APP_CallBack


coyotos_IrqCtl_irq_t myIrq = -1;
bool have_callback = false;
bool deliveryPending = false;

void
RunWaitLoop(bool once)
{
  do {
    if (!deliveryPending)
      coyotos_IrqCtl_wait(CR_IRQCTL, myIrq);

    deliveryPending = true;

    if (!coyotos_driver_IrqCallback_onInterrupt(CR_CallBack, myIrq))
      break;

    deliveryPending = false;
  } while (!once);
}


/* You should supply a function that selects an interface
 * type based on the incoming endpoint ID and protected
 * payload */
static inline uint64_t
choose_if(uint64_t epID, uint32_t pp)
{
  if (pp == coyotos_driver_IrqHelper_PP_Ctl)
    return IKT_coyotos_driver_IrqHelperCtl;
  else
    return IKT_coyotos_driver_IrqHelper;
}

/* The IDL_SERVER_Environment structure type is something
 * that you should extend to hold any "extra" information
 * you need to carry around in your handlers. CapIDL code
 * will pass this pointer along, but knows absolutely
 * nothing about the contents of the structure.
 *
 * The following structure definition is provided as a
 * starting point. It is generally a good idea to pass the
 * received protected payload and endpoint ID to handler
 * functions so that they know what object was invoked and
 * with what permissions.
 */
typedef struct IDL_SERVER_Environment {
  uint32_t pp;
  uint64_t epID;
} IDL_SERVER_Environment;

/* Handlers for coyotos.driver.IrqHelperCtl */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelperCtl_setIRQ(
  coyotos_IrqCtl_irq_t irq,
  struct IDL_SERVER_Environment *_env)
{
  myIrq = irq;

  if (!coyotos_IrqCtl_bindIrq(CR_IRQCTL, irq))
    return IDL_exceptCode;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelperCtl_getHelper(
  caploc_t _retVal,
  struct IDL_SERVER_Environment *_env)
{
  cap_copy(_retVal, CR_Helper);

  return RC_coyotos_Cap_OK;
}

/* Handlers for coyotos.driver.IrqHelper */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelper_setCallback(
  caploc_t callbackCap,
  struct IDL_SERVER_Environment *_env)
{
  cap_copy(CR_CallBack, callbackCap);
  have_callback = true;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelper_isDeliveryPending(
  bool *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  *_retVal = deliveryPending;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelper_cancelPendingDelivery(
  bool *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  *_retVal = deliveryPending;
  deliveryPending = false;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelper_runWaitLoop(
  struct IDL_SERVER_Environment *_env)
{
  if (myIrq == -1 || !have_callback)
    return RC_coyotos_Cap_RequestError;

  RunWaitLoop(false);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqHelper_waitOnce(
  struct IDL_SERVER_Environment *_env)
{
  if (myIrq == -1 || !have_callback)
    return RC_coyotos_Cap_RequestError;

  RunWaitLoop(true);

  return RC_coyotos_Cap_OK;
}

/* Handlers for coyotos.Cap */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_Cap_destroy(
  struct IDL_SERVER_Environment *_env)
{
  /*
   * ** UNCOMMENT and update if destroy can be refused **
   * if (destroy not allowed)
   *   return RC_coyotos_Cap_NoAccess;
   */
  unless (coyotos_SpaceBank_destroyBankAndReturn(CR_SPACEBANK,
                                                 CR_RETURN,
                                                 RC_coyotos_Cap_OK)) {
    return IDL_exceptCode;
  }
  /* Not reached */
  return RC_coyotos_Cap_RequestError;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_Cap_getType(
  uint64_t *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  *_retVal = choose_if(_env->epID, _env->pp);
  return RC_coyotos_Cap_OK;
}

/* ProcessRequests implements the main Event Loop */
void
ProcessRequests(struct IDL_SERVER_Environment *_env)
{
  _IDL_GRAND_SERVER_UNION gsu;
  
  gsu.icw = 0;
  gsu.pb.sndPtr = 0;
  gsu.pb.sndLen = 0;
  
  for(;;) {
    gsu.icw &= (IPW0_LDW_MASK|IPW0_LSC_MASK
        |IPW0_SG|IPW0_SP|IPW0_SC|IPW0_EX);
    gsu.icw |= IPW0_MAKE_NR(sc_InvokeCap)|IPW0_RP|IPW0_AC
        |IPW0_MAKE_LRC(3)|IPW0_NB|IPW0_CO;
    
    gsu.pb.u.invCap = CR_RETURN;
    gsu.pb.rcvCap[0] = CR_RETURN;
    gsu.pb.rcvCap[1] = CR_ARG0;
    gsu.pb.rcvCap[2] = CR_ARG1;
    gsu.pb.rcvCap[3] = CR_ARG2;
    gsu.pb.rcvBound = (sizeof(gsu) - sizeof(gsu.pb));
    gsu.pb.rcvPtr = ((char *)(&gsu)) + sizeof(gsu.pb);
    
    invoke_capability(&gsu.pb);
    
    _env->pp = gsu.pb.u.pp;
    _env->epID = gsu.pb.epID;
    
    /* Re-establish defaults. Note we rely on the handler proc
     * to decide how MANY of these caps will be sent by setting ICW.SC
     * and ICW.lsc fields properly.
     */
    gsu.pb.sndCap[0] = CR_REPLY0;
    gsu.pb.sndCap[1] = CR_REPLY1;
    gsu.pb.sndCap[2] = CR_REPLY2;
    gsu.pb.sndCap[3] = CR_REPLY3;
    
    /* We rely on the (de)marshaling procedures to set sndLen to zero
     * if no string is to be sent. We cannot zero it preemptively here
     * because sndLen is an IN parameter telling how many bytes we got.
     * Set sndPtr to zero so that we will fault if this is mishandled.
     */
    gsu.pb.sndPtr = 0;
    
    if ((gsu.icw & IPW0_SC) == 0) {
      /* Protocol violation -- reply slot unpopulated. */
      gsu.icw = 0;
      gsu.pb.sndLen = 0;
      continue;
    }
    
    switch(choose_if(gsu.pb.epID, gsu.pb.u.pp)) {
    case IKT_coyotos_driver_IrqHelper:
      _IDL_IFDISPATCH_coyotos_driver_IrqHelper(&gsu.coyotos_driver_IrqHelper, _env);
      break;
    case IKT_coyotos_driver_IrqHelperCtl:
      _IDL_IFDISPATCH_coyotos_driver_IrqHelperCtl(&gsu.coyotos_driver_IrqHelperCtl, _env);
      break;
    default:
      {
        gsu.except.icw =
          IPW0_MAKE_LDW((sizeof(gsu.except)/sizeof(uintptr_t))-1)
          |IPW0_EX|IPW0_SP;
        gsu.except.exceptionCode = RC_coyotos_Cap_UnknownRequest;
        gsu.pb.sndLen = 0;
        break;
      }
    }
  }
}

static inline bool
exit_gracelessly(errcode_t errCode)
{
  return
    coyotos_SpaceBank_destroyBankAndReturn(CR_SPACEBANK,
                                           CR_RETURN,
                                           errCode);
}

bool
initialize()
{
  unless(
	 coyotos_AddressSpace_getSlot(CR_TOOLS, 
				      coyotos_driver_IrqHelper_TOOL_IRQCTL,
				      CR_IRQCTL) &&

	 coyotos_AddressSpace_getSlot(CR_TOOLS, 
				      coyotos_driver_IrqHelper_TOOL_LOG,
				      CR_LOG) &&

         /* Set up helper entry capability */
         coyotos_Endpoint_makeEntryCap(CR_INITEPT,
                                       coyotos_driver_IrqHelper_PP_Helper,
                                       CR_Helper) &&

         /* Set up ctl entry capability */
         coyotos_Endpoint_makeEntryCap(CR_INITEPT,
                                       coyotos_driver_IrqHelper_PP_Ctl,
                                       CR_REPLY0)
         )
    exit_gracelessly(IDL_exceptCode);
  
  /* Send our entry cap to our caller */
  REPLY_create(CR_RETURN, CR_REPLY0);
  
  return true;
}

int
main(int argc, char *argv[])
{
  struct IDL_SERVER_Environment ise;
  
  if (!initialize())
    return 0;
  
  ProcessRequests(&ise);
  
  return 0;
}
