/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdint.h>
#include <string.h>
#include "termios.h"

#define I(nm) coyotos_TermIoStream_iflag_##nm
#define O(nm) coyotos_TermIoStream_oflag_##nm
#define C(nm) coyotos_TermIoStream_cflag_##nm
#define L(nm) coyotos_TermIoStream_lflag_##nm
#define CC(nm) coyotos_TermIoStream_cc_##nm

coyotos_TermIoStream_termios tstate;

static void 
termios_clear_chars()
{
  memset(&tstate.c_cc, 0, sizeof(tstate.c_cc));
}

void 
termios_make_cooked()
{
  termios_clear_chars();

  tstate.c_iflag = I(BRKINT)|I(ICRNL)|I(IXON);
  tstate.c_oflag = (O(OPOST)|O(ONLCR));
  tstate.c_lflag = (L(ECHO)|L(ECHONL)|L(ECHOE)|L(ECHOCTL)|
		    L(ECHOKE)|L(ICANON)|L(ISIG));
  tstate.c_cflag = (C(CREAD)|C(B9600));

  tstate.c_cc[CC(VEOL)]     = '\n';
  tstate.c_cc[CC(VEOL2)]     = '\r';
  tstate.c_cc[CC(VERASE)]   = 0x08;
  tstate.c_cc[CC(VKILL)]    = 0x15;
  tstate.c_cc[CC(VWERASE)]  = 0x17;
  tstate.c_cc[CC(VREPRINT)] = 0x12;
}

void 
termios_make_raw()
{
  termios_clear_chars();

  tstate.c_iflag &= ~(I(IGNBRK)|I(BRKINT)|I(PARMRK)|I(ISTRIP)|
		     I(INLCR)|I(IGNCR)|I(ICRNL)|I(IXON));
  tstate.c_oflag &= ~O(OPOST);
  tstate.c_lflag &= ~(L(ECHO)|L(ECHONL)|L(ICANON)|L(ISIG));
  tstate.c_cflag &= ~(C(CSIZE)|C(PARENB));
  tstate.c_cflag |= C(CS8);
}

void
termios_getattr(coyotos_TermIoStream_termios *tios)
{
  memcpy(tios, &tstate, sizeof(tstate));
}

void
termios_setattr(coyotos_TermIoStream_termios * const tios)
{
  memcpy(&tstate, tios, sizeof(tstate));
}

