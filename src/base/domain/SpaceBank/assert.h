/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef ASSERT_H__
#define ASSERT_H__

#include <inttypes.h>

extern void __assert3_fail(const char *file, int lineno, const char *desc, uint64_t val1, uint64_t val2)
  __attribute__ ((__noreturn__));
extern void __assert_fail(const char *file, int lineno, const char *desc)
  __attribute__ ((__noreturn__));

#ifdef NDEBUG
#define assert(x)         ((void) 0)
#define assert3(a, x, b)  ((void) 0)
#define assert3p(a, x, b) ((void) 0)

#else

#undef assert
#define assert(x) \
  ((x) ? ((void) 0) : ((void) __assert_fail(__FILE__, __LINE__, #x)))

#define assert3(a, x, b)						\
  ({									\
    const typeof(a) __a = (a);						\
    const typeof(b) __b = (b);						\
    ((__a x __b) ? ((void) 0)						\
     : (void) __assert3_fail(__FILE__, __LINE__, #a " " #x " " #b,	\
			     __a, __b));				\
  })

#define assert3p(a, x, b)						\
  ({									\
    const typeof(a) __a = (a);						\
    const typeof(b) __b = (b);						\
    ((__a x __b) ? ((void) 0)						\
     : (void) __assert3_fail(__FILE__, __LINE__, #a " " #x " " #b,	\
			     (uintptr_t)__a, (uintptr_t)__b));		\
  })

#endif

#endif /* ASSERT_H__ */
