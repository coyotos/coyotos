/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Heap support data structures
 */

/** @brief initialize the alloc subsystem and address space.
 *
 * After this completes, @p image_header is valid and available for use
 */
void heap_init(void);

/** @brief Mark heap as frozen, so no further allocations may occur.
 */
void heap_freeze();

/** @brief allocates @p bytes of zero-filled storage aligned appropriately */
void *calloc(size_t nmemb, size_t size);

/** @brief map Page oids from @p start to @p end at sequential page
 * addresses.*/
void *
do_mmap_pages(oid_t start, oid_t end, bool readOnly);

static inline const void *
mmap_constant_pages(oid_t start, oid_t end)
{
  return do_mmap_pages(start, end, true);
}

static inline void *
mmap_pages(oid_t start, oid_t end)
{
  return do_mmap_pages(start, end, false);
}

