/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief SpaceBank implementation
 */

#include <coyotos/capidl.h>
#include <coyotos/syscall.h>
#include <coyotos/runtime.h>
#include <coyotos/reply_create.h>
#include <coyotos/kprintf.h>

#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/Endpoint.h>
#include <idl/coyotos/AddressSpace.h>
#include <idl/coyotos/CapBits.h>

#include "SpaceBank.h"
#include "debug.h"

/* Utility quasi-syntax */
#define unless(x) if (!(x))

/* all of our handler procedures are static */
#define IDL_SERVER_HANDLER_PREDECL static

#include <idl/coyotos/SpaceBank.server.h>

typedef union {
  _IDL_IFUNION_coyotos_SpaceBank
      coyotos_SpaceBank;
  InvParameterBlock_t pb;
  InvExceptionParameterBlock_t except;
  uintptr_t icw;
} _IDL_GRAND_SERVER_UNION;

/** @brief Server environment for SpaceBank handlers */
typedef struct IDL_SERVER_Environment {
  uint32_t	restr;		/**< @brief Restrictions (from PP) */
  Bank 		*bank;		/**< @brief Invoked bank (from epID) */
  caploc_t	returnTo;	/**< @brief Capability to send response to */
  char		nullSndCap[4];  /**< @brief Output capabilities to CR_NULLify */
} IDL_SERVER_Environment;

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_Cap_destroy(
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noDestroy)
    return RC_coyotos_Cap_NoAccess;

  // special case the Prime Bank, since no-one can destroy it.
  if (ise->bank->parent == 0)
    return RC_coyotos_Cap_NoAccess;

  bank_destroy(ise->bank, 1);  // destroy the bank's contents as well

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_Cap_getType(
  uint64_t *out, 
  struct IDL_SERVER_Environment *ise)
{
  *out = IKT_coyotos_SpaceBank;
  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_alloc(
  coyotos_Range_obType ty1,
  coyotos_Range_obType ty2,
  coyotos_Range_obType ty3,
  caploc_t c1,
  caploc_t c2,
  caploc_t c3,
  struct IDL_SERVER_Environment *ise)
{
  Bank *bank = ise->bank;

  if (ise->restr & coyotos_SpaceBank_restrictions_noAlloc)
    return RC_coyotos_Cap_NoAccess;

  Object *obj1 = 0;
  Object *obj2 = 0;
  Object *obj3 = 0;

  if (!(check_obType(ty1) || check_obType(ty2) || check_obType(ty3))) {
    DEBUG(reqerr)
      kprintf(CR_KERNLOG, "SpaceBank: bad object types to alloc()\n");
    return RC_coyotos_Cap_RequestError;
  }

  if (ty1 == coyotos_Range_obType_otInvalid)
    ise->nullSndCap[0] = 1;
  else if ((obj1 = bank_alloc(bank, ty1, c1)) == 0)
    goto fail_alloc;

  if (ty2 == coyotos_Range_obType_otInvalid)
    ise->nullSndCap[1] = 1;
  else if ((obj2 = bank_alloc(bank, ty2, c2)) == 0)
    goto fail_alloc;

  if (ty3 == coyotos_Range_obType_otInvalid)
    ise->nullSndCap[2] = 1;
  else if ((obj3 = bank_alloc(bank, ty3, c3)) == 0)
    goto fail_alloc;

  return RC_coyotos_Cap_OK;

 fail_alloc:
  if (obj3 != 0)
    object_unalloc(obj3);
  if (obj2 != 0)
    object_unalloc(obj2);
  if (obj1 != 0)
    object_unalloc(obj1);

  return RC_coyotos_SpaceBank_LimitReached;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_allocProcess(
  caploc_t brand,
  caploc_t _retVal,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noAlloc)
    return RC_coyotos_Cap_NoAccess;

  Object *obj = bank_alloc_proc(ise->bank, brand, _retVal);
  if (obj == 0)
    return RC_coyotos_SpaceBank_LimitReached;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_createChild(
  caploc_t _retVal,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noAlloc)
    return RC_coyotos_Cap_NoAccess;

  if (!bank_create(ise->bank, _retVal))
    return RC_coyotos_SpaceBank_LimitReached;

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_destroyBankAndReturn(
  caploc_t rcvr,
  coyotos_Cap_exception_t resultCode,
  struct IDL_SERVER_Environment *ise)
{
  // acts like a destroy, which if successful, returns elsewhere.
  uint64_t result = HANDLE_coyotos_Cap_destroy(ise);

  if (result != RC_coyotos_Cap_OK)
    return result;

  // Send the reply to rcvr, and use the requested result code
  ise->returnTo = rcvr;
  return resultCode;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_free(
  uint32_t count,
  caploc_t c1,
  caploc_t c2,
  caploc_t c3,
  struct IDL_SERVER_Environment *ise)
{
  Bank *bank = ise->bank;

  if (ise->restr & coyotos_SpaceBank_restrictions_noFree)
    return RC_coyotos_Cap_NoAccess;

  Object *obj1 = NULL;
  Object *obj2 = NULL;
  Object *obj3 = NULL;

  switch(count) {
  case 3:
    obj3 = object_identify(c3);
    if (!obj3) {
      DEBUG(reqerr)
	kprintf(CR_KERNLOG, "SpaceBank: object 3 to free() does_not_exist\n");
      return RC_coyotos_Cap_RequestError;
    }
    if (obj3->bank != bank) {
      DEBUG(reqerr) {
	oid_t oid = object_getOid(obj3);
	coyotos_Range_obType ty = object_getType(obj3);
	coyotos_CapBits_info bits = { {0, 0, 0, 0} };
	coyotos_CapBits_get(CR_CAPBITS, c3, &bits);
	kprintf(CR_KERNLOG, "c3 is 0x%08x 0x%0x 0x%x\n",
		bits.w[0], bits.w[1], bits.w[2], bits.w[3]);
	kprintf(CR_KERNLOG, 
		"SpaceBank: object 3 0x%p ty %d oid 0x%llx to free() has wrong "
		"bank (0x%p vs 0x%p)\n", 
		obj3,
		ty, oid, obj3->bank, bank);
      }
      return RC_coyotos_Cap_RequestError;
    }
    /*FALLTHROUGH*/
  case 2:
    obj2 = object_identify(c2);
    if (!obj2) {
      DEBUG(reqerr)
	kprintf(CR_KERNLOG, "SpaceBank: object 2 to free() does_not_exist\n");
      return RC_coyotos_Cap_RequestError;
    }
    if (obj2->bank != bank) {
      DEBUG(reqerr) {
	oid_t oid = object_getOid(obj2);
	coyotos_Range_obType ty = object_getType(obj2);
	kprintf(CR_KERNLOG, 
		"SpaceBank: object 2 0x%p ty %d oid 0x%llx to free() has wrong "
		"bank (0x%p vs 0x%p)\n", 
		obj2,
		ty, oid, obj2->bank, bank);
      }
      return RC_coyotos_Cap_RequestError;
    }
    /*FALLTHROUGH*/
  case 1:
    obj1 = object_identify(c1);
    if (!obj1) {
      DEBUG(reqerr)
	kprintf(CR_KERNLOG, "SpaceBank: object 1 to free() does_not_exist (0x%p vs 0x%p)\n");
      return RC_coyotos_Cap_RequestError;
    }
    if (obj1->bank != bank) {
      DEBUG(reqerr) {
	oid_t oid = object_getOid(obj1);
	coyotos_Range_obType ty = object_getType(obj1);
	kprintf(CR_KERNLOG, 
		"SpaceBank: object 1 0x%p ty %d oid 0x%llx to free() has wrong "
		"bank (0x%p vs 0x%p)\n", 
		obj1,
		ty, oid, obj1->bank, bank);
      }
      return RC_coyotos_Cap_RequestError;
    }
    break;

  default:
    DEBUG(reqerr)
      kprintf(CR_KERNLOG, "SpaceBank: bad count %d to free()\n", count);
    return RC_coyotos_Cap_RequestError;
  }
    
  if (obj1 != 0) object_rescindAndFree(obj1);
  if (obj2 != 0) object_rescindAndFree(obj2);
  if (obj3 != 0) object_rescindAndFree(obj3);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_SpaceBank_getEffectiveLimits(
  coyotos_SpaceBank_limits *_retVal,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noQueryLimits)
    return RC_coyotos_Cap_NoAccess;
  
  bank_getEffLimits(ise->bank, _retVal);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_getLimits(
  coyotos_SpaceBank_limits *_retVal,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noQueryLimits)
    return RC_coyotos_Cap_NoAccess;
  
  bank_getLimits(ise->bank, _retVal);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_getUsage(
  coyotos_SpaceBank_limits *_retVal,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noQueryLimits)
    return RC_coyotos_Cap_NoAccess;
  
  bank_getUsage(ise->bank, _retVal);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_reduce(
  coyotos_SpaceBank_restrictions mask,
  caploc_t _retVal,
  struct IDL_SERVER_Environment *ise)
{
  // make sure we don't include non-sensical restrictions
  // MUST be highest allowed restriction
  if (mask > coyotos_SpaceBank_restrictions_noRemove) {
    DEBUG(reqerr)
      kprintf(CR_KERNLOG, "SpaceBank: bad mask to reduce()\n");
    return RC_coyotos_Cap_RequestError;
  }

  bank_getEntry(ise->bank, ise->restr | mask, _retVal);
  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_remove(
  struct IDL_SERVER_Environment *ise)
{
  if ((ise->restr & coyotos_SpaceBank_restrictions_noDestroy) ||
      (ise->restr & coyotos_SpaceBank_restrictions_noRemove))
    return RC_coyotos_Cap_NoAccess;

  // special case the Prime Bank, since no-one can destroy it.
  if (ise->bank->parent == 0)
    return RC_coyotos_Cap_NoAccess;

  bank_destroy(ise->bank, 0);  // just the bank

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_setLimits(
  coyotos_SpaceBank_limits limits,
  struct IDL_SERVER_Environment *ise)
{
  if (ise->restr & coyotos_SpaceBank_restrictions_noChangeLimits)
    return RC_coyotos_Cap_NoAccess;

  if (!bank_setLimits(ise->bank, &limits)) {
    DEBUG(reqerr)
      kprintf(CR_KERNLOG, "SpaceBank: could not setLimits()\n");
    return RC_coyotos_Cap_RequestError;
  }

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t 
HANDLE_coyotos_SpaceBank_verifyBank(
  caploc_t bank,
  bool *_retVal,
  struct IDL_SERVER_Environment *ise)
{
  bool success = false;
  uint64_t epID = 0;
  coyotos_Cap_payload_t payload = 0;
  bool isMe = false;

  MUST_SUCCEED(coyotos_Process_identifyEntry(CR_SELF, 
					     bank,
					     &payload, 
					     &epID,
					     &isMe,
					     &success));
  *_retVal = (success && isMe);
  return (RC_coyotos_Cap_OK);
}

void
ProcessRequests(struct IDL_SERVER_Environment *ise)
{
  _IDL_GRAND_SERVER_UNION gsu;
  
  gsu.icw = 0;
  gsu.pb.sndPtr = 0;
  gsu.pb.sndLen = 0;

  ise->returnTo = CR_RETURN;

  for(;;) {
    int idx;

    gsu.icw &= (IPW0_LDW_MASK|IPW0_LSC_MASK
        |IPW0_SG|IPW0_SP|IPW0_SC|IPW0_EX);
    gsu.icw |= IPW0_MAKE_NR(sc_InvokeCap)|IPW0_RP|IPW0_AC
        |IPW0_MAKE_LRC(3)|IPW0_NB|IPW0_CO;
    
    gsu.pb.u.invCap = ise->returnTo;

    gsu.pb.rcvCap[0] = CR_RETURN;
    gsu.pb.rcvCap[1] = CR_ARG0;
    gsu.pb.rcvCap[2] = CR_ARG1;
    gsu.pb.rcvCap[3] = CR_ARG2;
    gsu.pb.rcvBound = (sizeof(gsu) - sizeof(gsu.pb));
    gsu.pb.rcvPtr = ((char *)(&gsu)) + sizeof(gsu.pb);
    
    invoke_capability(&gsu.pb);

    /* Set up the server environment. */
    ise->restr = gsu.pb.u.pp;
    ise->bank = bank_fromEPID(gsu.pb.epID);

    ise->returnTo = CR_RETURN;		/* default to CR_RETURN */
    for (idx = 0; idx < 4; idx++)
      ise->nullSndCap[idx] = 0;		/* default to no clearing */

    assert(ise->bank != 0);
    
    /* Re-establish defaults. Note we rely on the handler proc
     * to decide how MANY of these caps will be sent by setting ICW.SC
     * and ICW.lsc fields properly.
     */
    gsu.pb.sndCap[0] = CR_REPLY0;
    gsu.pb.sndCap[1] = CR_REPLY1;
    gsu.pb.sndCap[2] = CR_REPLY2;
    gsu.pb.sndCap[3] = CR_REPLY3;
    
    /* We rely on the (de)marshaling procedures to set sndLen to zero
     * if no string is to be sent. We cannot zero it preemptively here
     * because sndLen is an IN parameter telling how many bytes we got.
     * Set sndPtr to zero so that we will fault if this is mishandled.
     */
    gsu.pb.sndPtr = 0;
    
    if ((gsu.icw & IPW0_SC) == 0) {
      /* Protocol violation -- reply slot unpopulated. */
      gsu.icw = 0;
      gsu.pb.sndLen = 0;
      continue;
    }
    
    /* normally, we'd switch on the implied type, but we only have one */
    _IDL_IFDISPATCH_coyotos_SpaceBank(&gsu.coyotos_SpaceBank, ise);

    /* send NULL caps as requested;  this way, the handlers don't have
     * to worry about zeroing the uninteresting reply registers.
     */
    for (idx = 0; idx < 4; idx++)
      if (ise->nullSndCap[idx] != 0)
	gsu.pb.sndCap[idx] = CR_NULL;
  }
}

int
main(int argc, char *argv[])
{
  struct IDL_SERVER_Environment ise;
  
  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: start\n");

  initialize();
  
  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: initialized\n");

  ProcessRequests(&ise);
  
  return 0;
}
