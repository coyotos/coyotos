/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief SpaceBank heap implementation.
 *
 * This file implements the SpaceBank heap. Storage allocated by the
 * space bank is never returned, so the heap manager does not provide
 * any implementation of free().
 *
 * In the embedded space bank case, it is possible to do a one-time
 * allocation of all required storage. The implementation of the
 * embedded space bank assumes that all of the associated management
 * data structures will fit in memory. If that isn't true't on your
 * embedded system, this isn't the implementation for you.
 *
 * The embedded space bank is linked as a small-model program. This
 * means that it has a relatively low start address. The
 * linker-generated image ought to fit within a single GPT in
 * practice, but will certainly fit within slot zero of a
 * newly-introduced top-level GPT of any plausible size. The
 * implementation strategy for the heap is to construct a new GPT
 * having l2v=addrspace_l2v, stick the original address space GPT in
 * slot 0, and then proceed to construct the heap starting at whatever
 * virtual address corresponds to slot 1 of that new, top-level GPT
 * (that is, at 1<<addrspace_l2v).
 *
 * The heap is also used to map various data structures from the
 * CoyImage (see mmap_constant_pages).
 */

#include "SpaceBank.h"
#include "debug.h"
#include "heap.h"
#include <coyotos/kprintf.h>

static uintptr_t mybrk = 0;  /* allocation base */
static bool frozen = false;

/** @brief l2v of the new top-level GPT that we will introduce.
 *
 * @bug The strategy below leads to a sub-optimal leaf structure on
 * systems whose address size in bits is not 4(k)+log2(pgsize) for
 * some k, notably coldfire. On such systems, we might do better to
 * adjust the l2v of the top-level GPT up or down somewhat.
 */
static const coyotos_Memory_l2value_t addrspace_l2v = 
  (COYOTOS_HW_ADDRESS_BITS - coyotos_GPT_l2slots);

/** @brief Initialize the space bank heap, returning virtual address
 * of the CoyImgHdr page.
 *
 * Inserts a new top-level GPT in preparation for later heap growth as
 * calls to calloc() are made. Maps the CoyImgHdr page so that we can
 * later reference it.
 */
void
heap_init(void)
{
  coyotos_Memory_l2value_t oldl2v;

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: alloc_init\n");

  // Our mkimage has an initial GPT which we use as the top of our 
  // address space.
  MUST_SUCCEED(coyotos_Memory_setGuard(CR_INITGPT, 
				       make_guard(0, COYOTOS_HW_ADDRESS_BITS),
				       CR_ADDRSPACE));
  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: guard is set\n");

  MUST_SUCCEED(coyotos_GPT_setl2v(CR_ADDRSPACE, 
				  addrspace_l2v, 
				  &oldl2v));
  
  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: l2v is set\n");

  // Put the existing address space in slot 0 of the initial GPT
  MUST_SUCCEED(coyotos_Process_getSlot(CR_SELF, 
				       coyotos_Process_cslot_addrSpace,
				       CR_TMP1));

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: got space slot\n");

  MUST_SUCCEED(coyotos_AddressSpace_setSlot(CR_ADDRSPACE, 
					    0, 
					    CR_TMP1));

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: grew space\n");

  // And install the new address space
  MUST_SUCCEED(coyotos_Process_setSlot(CR_SELF, 
				       coyotos_Process_cslot_addrSpace,
				       CR_ADDRSPACE));

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: set space slot\n");

  /* Set up a read-only Page zero mapping */
  get_pagecap(CR_TMP1, 0);
  MUST_SUCCEED(coyotos_Memory_reduce(CR_TMP1, 
				     coyotos_Memory_restrictions_readOnly,
				     CR_TMP1));

  /* And install it as the first page in the heap */
  MUST_SUCCEED(coyotos_AddressSpace_setSlot(CR_ADDRSPACE, 
					    1,
					    CR_TMP1));

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: CoyImgHdr page is mapped\n");

  /* Now, set up our break state to match. */
  mybrk = ((uintptr_t)1 << addrspace_l2v);

  DEBUG(init) kprintf(CR_KERNLOG, "heap_init complete.\n");
}

/** @brief Safe version of (addr >> shift) */
static inline uintptr_t 
highbits_shifted(uintptr_t addr, uint8_t shift)
{
  if (shift >= 32)
    return 0;
  return (addr >> shift);
}

/** @brief Safe version of (addr & ((1UL << shift) - 1)) */
static inline uintptr_t 
lowbits(uintptr_t addr, uint8_t shift)
{
  if (shift >= 32)
    return addr;
  return addr & ((1UL << shift) - 1);
}

/** @brief Given a page capability @p pageCap and an address @p addr,
 * install that page at the specified address.
 */
static void
install_Page(caploc_t pageCap, uintptr_t addr)
{
  /*
   * We will loop as we go down;  the capability registers used looks like:
   *      cap        next     spare    next_spare
   *  1.  ADDRSPACE  TMP1     TMP2     TMP3
   *  2.  TMP1       TMP2     TMP3     TMP1
   *  3.  TMP2       TMP3     TMP1     TMP2
   *  4.  TMP3       TMP1     TMP2     TMP3
   *  5.  TMP1       TMP2     TMP3     TMP1
   *  ... etc. ...
   */
  assert(pageCap.raw != CR_TMP1.raw && pageCap.raw != CR_TMP2.raw
	 && pageCap.raw != CR_TMP3.raw);

  caploc_t cap = CR_ADDRSPACE;
  caploc_t next = CR_TMP1;
  caploc_t spare = CR_TMP2;
  caploc_t next_spare = CR_TMP3;

  {
    guard_t theGuard = 0;
    
    MUST_SUCCEED(coyotos_Memory_getGuard(pageCap, &theGuard));
    assert3(guard_match(theGuard), ==, 0);
    assert3(guard_l2g(theGuard), ==, COYOTOS_PAGE_ADDR_BITS);
  }

  coyotos_Memory_l2value_t l2v = 0;
  coyotos_Memory_l2value_t lastl2v = COYOTOS_SOFTADDR_BITS;

  // the top-level GPT always has a guard of zero and an l2g of the HW
  // address limit
  for (;;) {
    DEBUG(heap) kprintf(CR_KERNLOG, "processing addr=0x%x\n", addr);

    MUST_SUCCEED(coyotos_GPT_getl2v(cap, &l2v));

    uintptr_t slot = highbits_shifted(addr, l2v);
    uintptr_t remaddr = lowbits(addr, l2v);
    
    DEBUG(heap) 
      kprintf(CR_KERNLOG, "slot=%d, remaddr=0x%x\n", slot, remaddr);

    if (l2v == COYOTOS_PAGE_ADDR_BITS) {
      MUST_SUCCEED(coyotos_AddressSpace_setSlot(cap, slot, pageCap));
      return;
    }

#if DEBUG_CND(heap)
    uint64_t capType = 0;
    coyotos_Cap_getType(cap, &capType);
    DEBUG(heap) 
      kprintf(CR_KERNLOG, "capType to aspace_getSlot is 0x%llx\n", capType);
#endif /* DEBUG_CND(heap) */

    MUST_SUCCEED(coyotos_AddressSpace_getSlot(cap, slot, next));

#if DEBUG_CND(heap)
    coyotos_Cap_getType(next, &capType);
    DEBUG(heap) 
      kprintf(CR_KERNLOG, "next capType is 0x%llx\n", capType);
#endif /* DEBUG_CND(heap) */

    /**
     * The following check is doing something tricky. It relies on the
     * fact that all caps in the heap portion of the memory tree are
     * either GPT, Page, or Null caps. GPT and Page caps respond to
     * coyotos_Memory_getGuard, while Null caps raise an
     * UnknownRequest exception. The following call therefore does
     * double duty to fetch the guard if @p next holds a memory cap,
     * or detect that @p next is a null cap on the basis of the error.
     *
     * @bug Should there be an assert here against the error code in
     * __IDL_env?
     */
    guard_t theGuard;
    if (!coyotos_Memory_getGuard(next, &theGuard)) {
      assert3(remaddr, ==, 0);
      DEBUG(heap) 
	kprintf(CR_KERNLOG, "getGuard fails (null cap reached)\n");
      MUST_SUCCEED(coyotos_AddressSpace_setSlot(cap, slot, pageCap));
      return;
    }

    uint32_t match = guard_match(theGuard);
    coyotos_Memory_l2value_t l2g = guard_l2g(theGuard);

    DEBUG(heap) 
      kprintf(CR_KERNLOG, "theGuard = 0x%x match == %d l2g == %d\n", 
	      theGuard, match, l2g);

    assert3(match, ==, 0);
    assert3(l2g, <=, 32);
    assert3(l2g, <=, l2v);
    
    uintptr_t matchbits = highbits_shifted(remaddr, l2g);
    if (matchbits != 0) {
      DEBUG(heap) 
	kprintf(CR_KERNLOG, "  matchbits non-zero\n");

      // we need to add a GPT
      require_GPT(spare);
      MUST_SUCCEED(coyotos_GPT_setl2v(spare, l2g, &l2v));
      MUST_SUCCEED(coyotos_Memory_setGuard(spare, 
					   make_guard(0, 
						      l2g + 
						      coyotos_GPT_l2slots),
					   spare));
      MUST_SUCCEED(coyotos_Memory_setGuard(next, 
					   make_guard(0, l2g),
					   next));
      MUST_SUCCEED(coyotos_AddressSpace_setSlot(spare, 0, next));
      MUST_SUCCEED(coyotos_AddressSpace_setSlot(cap, slot, spare));
      continue;  // re-execute loop with newly inserted GPT
    }
    if (l2g == COYOTOS_PAGE_ADDR_BITS) {
      DEBUG(heap) 
	kprintf(CR_KERNLOG, "  l2g == PAGE_ADDR_BITS\n");
      // replacing a page.  Just overwrite it.
      MUST_SUCCEED(coyotos_AddressSpace_setSlot(cap, slot, pageCap));
      return;
    }

    assert3(l2v, <, lastl2v);
    lastl2v = l2v;

    // we're traversing *into* a GPT.  Since the guard is zero, and
    // we match it, we just need to update the address and swap around
    // our caps.
    addr = remaddr;

    cap = next;
    next = spare;
    spare = next_spare;
    next_spare = cap;
  }
}

/** @brief align a size up to the minimum alignment boundry.
 */  

static inline uintptr_t
align_up(uintptr_t x, uintptr_t amount)
{
  return x + ((-x) & (amount - 1));
}

/** @brief Allocate @p nmemb zeroed elements of @p elem_size.
 *
 * This extends the space bank heap as needed.
 */
void *
calloc(size_t elem_size, size_t nmemb)
{
  assert3(mybrk, !=, 0);
  assert (!frozen);

  size_t bytes_arg = elem_size * nmemb;

  size_t bytes = align_up(bytes_arg, sizeof(uintptr_t));
  const uintptr_t a_start = mybrk;
  const uintptr_t a_end = mybrk + bytes;

  uintptr_t pos;

  assert3(a_end, >=, mybrk);

  for (pos = align_up(mybrk, COYOTOS_PAGE_SIZE);
       pos < a_end;
       pos += COYOTOS_PAGE_SIZE) {
    assert3(pos, >=, mybrk);

    require_Page(CR_TMPPAGE);
    install_Page(CR_TMPPAGE, pos);
  }

  assert3(a_end, >=, mybrk);
  mybrk = a_end;
  return (void *)a_start;
}

/** @brief Mark that no further allocations should be permitted.
 *
 * The space bank must account for all internally allocated pages and
 * deduct them from the pages that are avaialable for object
 * allocation.
 */
void
heap_freeze()
{
  frozen = true;
}

/** @brief Map a sequential range of page OIDs starting at an
 * arbitrarily selected page boundary.
 */
void *
do_mmap_pages(oid_t base, oid_t bound, bool readOnly)
{
  assert3(mybrk, !=, 0);
  assert(base <= bound);

  size_t nPages = bound - base;

  const uintptr_t start = align_up(mybrk, COYOTOS_PAGE_SIZE);
  const uintptr_t end = start + COYOTOS_PAGE_SIZE * nPages;

  size_t idx;

  for (idx = 0; idx < nPages; idx++) {
    get_pagecap(CR_TMPPAGE, base + idx);
    if (readOnly)
      MUST_SUCCEED(coyotos_Memory_reduce(CR_TMPPAGE, 
					 coyotos_Memory_restrictions_readOnly,
					 CR_TMPPAGE));
    install_Page(CR_TMPPAGE, start + idx * COYOTOS_PAGE_SIZE);
  }
  mybrk = end;

  DEBUG(heap)
    kprintf(CR_KERNLOG, "do_mmap_pages(0x%llx, 0x%llx, %c) -> 0x%p\n",
	    base, bound, (readOnly ? 't' : 'f'), start);

  return (void *)start;
}

