/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <coyotos/syscall.h>

	.file "coyotos_yield.S"
	.text
	.align 2
	.globl	yield
	.type	yield, @function
yield:
	/* D0 is caller-save */
	link.w	%fp,#0
	move.l	#(IPW0_MAKE_NR(sc_Yield)),%d0	/* Could be mov3q */
	trap	#0	

	unlk	%fp
	rts
	.size	yield, .-yield
