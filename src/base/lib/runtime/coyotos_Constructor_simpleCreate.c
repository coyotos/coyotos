/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <coyotos/captemp.h>
#include <coyotos/runtime.h>
#include <idl/coyotos/Process.h>
#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/Constructor.h>

#define unless(x)  if (!(x))

bool
IDL_ENV_coyotos_Constructor_simpleCreate(IDL_Environment *_env,
					 caploc_t _invCap, 
					 caploc_t runtime,
					 caploc_t newBank,
					 caploc_t _retVal)
{
  bool result = false;

  caploc_t tmp_sched = captemp_alloc();
  caploc_t tmp_fullbank = captemp_alloc();
  caploc_t tmp_r_bank = captemp_alloc();

  unless (IDL_ENV_coyotos_Process_getSlot(_env, 
					  CR_SELF,
					  coyotos_Process_cslot_schedule,
					  tmp_sched) &&
	  IDL_ENV_coyotos_SpaceBank_createChild(_env,
						CR_SPACEBANK,
						tmp_fullbank))
    goto release;

  // from now on, we need to clean up tmp_fullbank on failure
  if (IDL_ENV_coyotos_SpaceBank_reduce(_env,
				       tmp_fullbank,
				       coyotos_SpaceBank_restrictions_noRemove,
				       tmp_r_bank) &&
      IDL_ENV_coyotos_Constructor_create(_env,
					 _invCap,
					 tmp_r_bank,
					 tmp_sched,
					 runtime,
					 _retVal)) {
    cap_copy(newBank, tmp_fullbank);
    result = true;

  } else {
    // restore the error code if destroy fails
    errcode_t err = _env->errCode;
    unless (IDL_ENV_coyotos_Cap_destroy(_env, tmp_fullbank))
      _env->errCode = err;
  }

 release:
  captemp_release(tmp_r_bank);
  captemp_release(tmp_fullbank);
  captemp_release(tmp_sched);

  return result;
}

