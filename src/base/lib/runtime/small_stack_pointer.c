#include <stdint.h>
#include "coyotos.TargetInfo.h"

/* Default stack pointer at top of single GPT. */
uintptr_t __rt_stack_pointer __attribute__((weak, section(".data")))
  = coyotos_TargetInfo_small_stack_pointer;
