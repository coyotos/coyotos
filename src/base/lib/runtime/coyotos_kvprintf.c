/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <coyotos/kprintf.h>
#include <coyotos/ascii.h>
#include <idl/coyotos/KernLog.h>

bool
IDL_ENV_kvprintf(IDL_Environment *_env, caploc_t cap, const char* fmt, 
		 va_list ap)
{
  char buf[128];

  size_t len = kvsnprintf(buf, sizeof(buf), fmt, ap);

  coyotos_KernLog_logString ls  = { sizeof(buf), len, buf };

  return IDL_ENV_coyotos_KernLog_log(_env, cap, ls);
}


bool
kvprintf(caploc_t cap, const char* fmt, va_list listp)
{
  return IDL_ENV_kvprintf(__IDL_Env, cap, fmt, listp);
}
