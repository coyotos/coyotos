<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Hand-Building a Cross Environment</title>
  </head>
  <body>
    <h1 class="title">Hand-Building a Cross Environment</h1>
    <p>
      If you are not running on some version of Fedora, you will need
      to build the cross environment for yourself. Here are
      instructions for doing this.
    </p>
    <p>
      These instructions have been tested on a Fedora machine with the
      RPM-based tool chain deinstalled. The tree builds, but it is
      likely that there are many dependencies that are not addressed
      here. As you find them, please let us know what they are and we
      will try to update these instructions. Some of the issues we
      know about appear at the <a href="#issues">bottom</a> of this
      page.
    </p>
    <p>
      Before checking out the cross environment tree, you should begin
      by checking out the base coyotos tree as described <a
      href="index.html#checkout">here</a>. Once that is checked out,
      you should clone the <tt>ccs-xenv</tt> package by:
    </p>
    <pre class="literallayout">
cd coyotos/src <em># if necessary</em>
hg clone http://dev.eros-os.com/hg/ccs-xenv/trunk ccs-xenv</pre>
    <div class="note">
      <p>
	<b>Note:</b> Technically the <tt>ccs-xenv</tt> tree does not
	need to sit within the coyotos tree at all. We recommend this
	configuration mainly because it is the one that we use and
	test.
      </p>
    </div>
    <p>
      Before building the cross tools, choose a directory where they
      should be installed. If you install them into <tt>/coyotos</tt>
      or <tt>/usr/local/coyotos</tt>, the makefile fragments in the
      main Coyotos tree will find them automatically. Otherwise you
      will need to set the environment variable <tt>COYOTOS_XENV</tt>
      before building the main tree. Do <em>not</em> install the cross
      tools within your working source tree.
    </p>
    <p>
      In the following example, we will install into
      <tt>/home/shap/coyotos-xenv</tt>. Build the cross tools for your
      desired target(s) by typing:
    </p>
    <pre class="literallayout">
cd coyotos/src/ccs-xenv <em># if necessary</em>
make -f Makefile.xenv TARGETS="coyotos-i386" CROSSROOT=/home/shap/coyotos-xenv</pre>
    <p>
      If you do not provide a value for CROSSROOT, the default
      directory is assumed to be <tt>/coyotos</tt>. This is the
      preferred location if you are able to write it, because it will
      match the configuration used by the RPM-based cross tools. This
      is good, mainly because that is the configuration that we test
      against.
    </p>
    <p>
      Once the cross environment is built, you should be able to compile the
      main tree by following the <a href="index.html#build">instructions</a>.
    </p>
    <h1>Additional Requirements to Build Documentation</h1>
    <p>
      Note that the default build process does not recompile the
      documentation tree. If you plan to recompile that, you will also
      need to build and install the OSDoc tools. OSDoc was developed
      independent of Coyotos, and the source tree does not expect to
      live within the Coyotos tree.
    </p>
    <p>
      Fetch the OSDoc distribution by:
    </p>
    <pre class="literallayout">
hg clone http://dev.eros-os.com/hg/osdoc/trunk osdoc</pre>
    <p>
      Then compile and install it into your previously built cross
      tools tree by:
    </p>
    <pre class="literallayout">
cd osdoc
./configure --prefix=/home/shap/coyotos/host
make install</pre>
    <p>
      Where <tt>/home/shap/coyotos</tt> should be replaced by the name
      of the directory where you installed the cross tools. Once this
      is installed, you should be able to build the documents by
      following the <a href="index.html#docs">instructions</a>.
    </p>
    <h1 id="issues">Common Build Issues</h1>
    <ol>
      <li>
	<p>
	  <b>Missing <tt>libicu-devel</tt></b>
	</p>
	<p>
	  The most common source of cross tool build failure is a
	  complaint that the <tt>unicode.h</tt> header file is
	  missing. This is supplied by either the <tt>libicu</tt> or
	  the <tt>libicu-devel</tt> package, depending on your Linux
	  system. The cross build takes a while, so do yourself a
	  favor and make sure that is installed before you compile it.
	</p>
      </li>
      <li>
	<p>
	  <b>Use <tt>CROSSROOT</tt>, not <tt>COYOTOS_XENV</tt></b>
	</p>
	<p>
	  If your cross environment does not live in /coyotos or
	  /usr/local/coyotos, you need to set the
	  <tt>COYOTOS_XENV</tt> environment variable to point at it in
	  order to build the Coyotos tree.
	</p>
	<p>
	  When you build the cross tools, the argument to
	  <tt>make</tt> is <tt>CROSSROOT</tt>, not
	  <tt>COYOTOS_XENV</tt>. The reason is that this helps avoid
	  clobbering an existing cross environment by mistake. In
	  particular, it helps us avoid accidently overwriting the
	  RPM-based cross environment when we test the cross build.
	</p>
      </li>
    </ol>
  </body>
</html>
